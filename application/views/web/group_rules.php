<div class="page-title" style="background-image: url(<?= base_url();  ?>assets/pic/1920-1280-img-2.jpg)">
    <div class="grid-row">
        <h1>Terms & Condition</h1>
        <nav class="bread-crumb">
            <a href="<?=   base_url();  ?>">Home</a>
            <i class="fa fa-long-arrow-right"></i>
            <a href="<?=   base_url('About/rules');  ?>">Terms & Condition</a>
           
        </nav>
    </div>
</div>
<div class="page-content grid-row">
    <main>
        <section class="clear-fix">
            <h2>Alumni Terms & Condition</h2>
            <p class="fs-18"><strong>Donec mollis magna quis urna convallis, quis faucibus sem vestibulum. Sed vehicula suscipit lacus</strong></p>
            
            <p>Maecenas accumsan, massa nec vulputate congue, dolor erat ullamcorper dolor, ac aliquam eros sem in dui. In eu sagittis metus. Proin consectetur suscipit dui sed euismod. Nam non metus in est vehicula vestibulum et vel neque. Mauris scelerisque lectus at diam pretium, eget fringilla erat sollicitudin.<br><br>Pellentesque quis pharetra tellus. Donec interdum nisi at sem ornare, ut congue felis eleifend. Nam elementum nec leo eget pharetra. Quisque consectetur porta erat sit amet fermentum.</p>
            <p>Maecenas accumsan, massa nec vulputate congue, dolor erat ullamcorper dolor, ac aliquam eros sem in dui. In eu sagittis metus. Proin consectetur suscipit dui sed euismod. Nam non metus in est vehicula vestibulum et vel neque. Mauris scelerisque lectus at diam pretium, eget fringilla erat sollicitudin.<br><br>Pellentesque quis pharetra tellus. Donec interdum nisi at sem ornare, ut congue felis eleifend. Nam elementum nec leo eget pharetra. Quisque consectetur porta erat sit amet fermentum.</p>
            <div class="block-overflow">
                <div class="columns-row">
                    <div class="columns-col columns-col-6">
                        <ul class="check-list">
                            <li>Aliquam justo lorem, commodo eget tristique</li>
                            <li>Curabitur vehicula leo accumsan, varius tellus </li>
                            <li>Pellentesque imperdiet, leo ut pulvinar facilisis</li>
                            <li>Сonvallis lectus, vitae condimentum nulla odio</li>
                        </ul>
                    </div>
                    <div class="columns-col columns-col-6">
                        <ul class="check-list">
                            <li>Aliquam justo lorem, commodo eget tristique</li>
                            <li>Curabitur vehicula leo accumsan, varius tellus </li>
                            <li>Pellentesque imperdiet, leo ut pulvinar facilisis</li>
                            <li>Сonvallis lectus, vitae condimentum nulla odio</li>
                        </ul>
                    </div>
                </div>
            </div>
        </section>
    </main>
</div>