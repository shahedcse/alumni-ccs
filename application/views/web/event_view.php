<div class="page-title" style="background-image: url(<?= base_url(); ?>assets/pic/1920-1280-img-2.jpg)">
    <div class="grid-row">
        <h1>Event Details</h1>
        <nav class="bread-crumb">
            <a href="<?= base_url(); ?>">Home</a>
            <i class="fa fa-long-arrow-right"></i>
            <a href="#">Event Details</a>

        </nav>
    </div>
</div>
<div class="page-content grid-row">
    <main>
        <div class="blog-post">
            <article>

                <h2><?= $event_details->event_tilte; ?></h2>
                <p>
                <h3>Place:<?= $event_details->place; ?></h3>
                <?= $event_details->details; ?></p>
                <img src="<?= base_url('"assets/img/blog/' . $event_details->file); ?>">
            </article>
        </div>
    </main>
</div>