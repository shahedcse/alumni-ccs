<div class="page-title" style="background-image: url(<?= base_url(); ?>assets/pic/1920-1280-img-2.jpg)">
    <div class="grid-row">
        <h1>Donation</h1>
        <nav class="bread-crumb">
            <a href="<?= base_url(); ?>">Home</a>
            <i class="fa fa-long-arrow-right"></i>
            <a href="<?= base_url('About/Donation'); ?>">Donation</a>

        </nav>
    </div>
</div>
<div class="page-content grid-row">
    <main>
        <section class="clear-fix">
            <div class="picture">
                <div class="hover-effect"></div>
                <div class="link-cont">
                    <a href="<?= base_url(); ?>assets/pic/istockphoto-836931558-612x612.jpg" class="fancy fa fa-search"></a>
                </div>
                <img src="<?= base_url(); ?>assets/pic/istockphoto-836931558-612x612.jpg" data-at2x="pic/870x460-img-1@2x.jpg" alt="">
            </div>
        </section>
    </main>
</div>




