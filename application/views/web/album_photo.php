<div class="page-title" style="background-image: url(<?= base_url(); ?>assets/pic/1920-1280-img-2.jpg)">
    <div class="grid-row">
        <h1>Photo Gallery</h1>
        <nav class="bread-crumb">
            <a href="<?= base_url(); ?>">Home</a>
            <i class="fa fa-long-arrow-right"></i>
            <a href="#">Photo Gallery</a>
        </nav>
    </div>
</div>
<div class="page-content grid-row">
    <main>
        <div class="isotope-container">
            <div class="isotope-header clear-fix">
                <h2 class="margin-none">Album Photos</h2>&nbsp;&nbsp;
                <a href="<?= base_url('Media/image_gallery'); ?>">
                    <button type="button" class="cws-button border-radius">Back to Album</button>
                </a>
            </div>
            <div class="grid-col-row">
                <div class="isotope" style="position: relative; overflow: hidden; height: 1011px;">
                    <?php
                    foreach ($photoall as $value):
                        ?>
                        <div class="item  isotope-item" >
                            <div class="picture">
                                <div class="hover-effect"></div>
                                <div class="link-cont">
                                    <a href="<?= base_url('assets/img/gallery/' . $value->image_path); ?>" class="fancy fa fa-search"></a>
                                </div>
                                <img src="<?= base_url('assets/img/gallery/' . $value->image_path); ?>"  alt="">
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
    </main>
</div>
