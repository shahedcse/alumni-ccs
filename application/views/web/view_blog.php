<div class="grid-row">
    <div class="page-content grid-col-row clear-fix">
        <div class="grid-col grid-col-9">
            <!-- main content -->
            <main>
                <!-- blog post -->
                <div class="blog-post">
                    <article>
                        <div class="post-info">
                            <div class="date-post"><div class="day"><?= date("d", strtotime($details->created_date)) ?></div><div class="month"><?= date("M", strtotime($details->created_date)) ?></div></div>
                            <div class="post-info-main">
                                <div class="author-post"><?= $details->name; ?></div>
                            </div>
                            <div class="comments-post"><i class="fa fa-comment"></i> <?= $comment_count; ?></div>
                        </div>
                        <div class="blog-media picture2">
                            <div class="hover-effect"></div>

                            <img src="<?= base_url(); ?>assets/img/blog/<?= $details->fetured_image ?>" alt="">
                        </div>
                        <h3><?= $details->blog_tilte ?></h3>
                        <p><?= $details->details ?></p>

                    </article></div>
                <!-- blog post -->
                <hr class="divider-color">
                <!-- comments for post -->
                <div class="comments">
                    <div id="comments">

                        <?php if (empty($comment_count)): ?>
                            <div class="comment-title">No Comments Found</span></div>
                        <?php else: ?>
                            <div class="comment-title">Comments <span>(<?= $comment_count; ?>)</span></div>
                            <ol class="commentlist">
                                <?php
                                if (sizeof($comments)):
                                    foreach ($comments as $value):
                                        $userQr = $this->db->query("SELECT * FROM users where id='$value->created_by'")->row();
                                        ?>
                                        <li class="comment">
                                            <div class="comment_container clear">
                                                <img style="border-radius: 50%" width="60px;" height="60px;"src="<?= $base_url ?>assets/img/profile/<?= $userQr->image_path; ?>"class="avatar" alt="">
                                                <div class="comment-text">
                                                    <p class="meta">
                                                        <strong><?= $userQr->name; ?></strong>
                                                        <time datetime="#"><?= $value->created_date; ?></time>
                                                    </p>
                                                    <div class="description">
                                                        <p><?= $value->comment; ?></p>
                                                    </div>

                                                </div>
                                            </div>
                                        </li>
                                        <?php
                                    endforeach;
                                endif;
                                ?>

                            </ol>
                        <?php endif; ?>
                    </div>
                </div>

                <hr class="divider-color">
                <div class="leave-reply">
                    <?php if (in_array($this->session->userdata('user_role'), array(1, 2))) : ?>
                        <div class="title">Leave a Reply</div>
                        <form class="message-form clear-fix" action="<?= base_url('Blog/insert_comment'); ?>" method="post">
                            <input type="hidden" id="blog_id" name="blog_id" value="<?= $details->id; ?>">
                            <p class="message-form-message">
                                <textarea id="comment" name="comment" required="" cols="45" rows="8" aria-required="true" placeholder="Your Comment"></textarea>
                            </p>
                            <p class="form-submit rectangle-button green medium">
                                <input class="cws-button border-radius alt"  name="submit" type="submit" id="submit" value="Submit">
                            </p>
                        </form>
                    <?php else: ?>
                        <div class="buttons-set">
                            <a href="<?= base_url('Auth'); ?>">
                                <button type="button" class=" btn-group-lg btn-outline-success">Log In Now for make comment </button>
                            </a>
                        </div>
                    <?php endif; ?>
                </div>
            </main>
            <!-- / main content -->
        </div>
        <div class="grid-col grid-col-3 sidebar">
            <?php $this->load->view('web/blog_sidebar'); ?>
        </div>
    </div>
</div>
<script async src="https://platform-api.sharethis.com/js/sharethis.js#property=5d8b1f057b26a80012429758&product=sticky-share-buttons"></script>