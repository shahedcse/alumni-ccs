<div class="page-title" style="background-image: url(<?= base_url(); ?>assets/pic/1920-1280-img-2.jpg)">
    <div class="grid-row">
        <h1>Event Lis</h1>
        <nav class="bread-crumb">
            <a href="<?= base_url(); ?>">Home</a>
            <i class="fa fa-long-arrow-right"></i>
            <a href="#">All Events</a>

        </nav>
    </div>
</div>
<div class="page-content grid-row">
    <main>
        <section class="clear-fix">
            <aside class="widget-event" style="padding: 15px;border: 1px solid grey;">
                <h2>Our Events</h2>

                <?php if (!empty($allevents)): ?>
                    <?php foreach ($allevents AS $value): ?>

                        <article class="clear-fix">
                            <a href="<?= base_url('About/event_details?id=' . $value->id); ?>">
                                <div class="date"><div class="day"><?= date("d", strtotime($value->date)) ?></div><div class="month"><?= date("M", strtotime($value->date)) ?></div></div>
                                <div class="event-description"><span><?= $value->place ?></span><p style="font-size: 16px;"><?= $value->event_tilte ?> </p></div>
                            </a>
                        </article>

                    <?php endforeach; ?>
                <?php else: ?>

                    <article class="clear-fix">
                        <div class="event-description"><p>No Event Available Now !!</p></div>
                    </article>

                <?php endif; ?>


            </aside>

        </section>
    </main>
</div>