<!-- footer -->
<footer>
    <div class="grid-row">
        <div class="grid-col-row clear-fix">
            <section class="grid-col grid-col-4 footer-about">
                <h2 class="corner-radius">About Us</h2>
                <div>
                    <h3>Sed aliquet dui auctor blandit ipsum tincidunt</h3>
                    <p>Quis rhoncus lorem dolor eu sem. Aenean enim risus, convallis id ultrices eget.</p>
                </div>
                <address>
                    <p></p>
                    <a href="#" class="phone-number">123-123456789</a>
                    <br />
                    <a href="#" class="email">info@chattogramcollegiatealumni.com</a>
                </address>
                <div class="footer-social">
                    <a href="#" class="fa fa-twitter"></a>
                    <a href="#" class="fa fa-skype"></a>
                    <a href="#" class="fa fa-google-plus"></a>
                    <a href="#" class="fa fa-rss"></a>
                    <a href="#" class="fa fa-youtube"></a>
                </div>
            </section>
            <section class="grid-col grid-col-4 footer-about">
                <h2 class="corner-radius">Payment Partner</h2>
                <aside class="widget-categories">
                    <center>
                        <ul style="color:#fff;" >
                            <li class="cat-item cat-item-1 current-cat"><img src="<?= base_url(); ?>assets/img/payment/p1.png"></li>
                            <li class="cat-item cat-item-1 current-cat"><img src="<?= base_url(); ?>assets/img/payment/p2.png"></li>
                            <li class="cat-item cat-item-1 current-cat"><img src="<?= base_url(); ?>assets/img/payment/p3.png"></li>
                            <li class="cat-item cat-item-1 current-cat"><img src="<?= base_url(); ?>assets/img/payment/p4.png"></li>
                            <li class="cat-item cat-item-1 current-cat"><img src="<?= base_url(); ?>assets/img/payment/p5.png"></li>
                        </ul>
                    </center>
                </aside>
            </section>

            <div id="fb-root"></div>
            <script async defer crossorigin="anonymous" src="https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v4.0&appId=393410661320474&autoLogAppEvents=1"></script>
            <section class="grid-col grid-col-4 footer-latest">
                <div class="fb-page" data-href="https://www.facebook.com/ChattagramCollegiates/" data-tabs="" data-width="" data-height="" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/ChattagramCollegiates/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/ChattagramCollegiates/">Chattagram Collegiates</a></blockquote></div>
            </section>

        </div>
    </div>
    <div class="footer-bottom"style="background-color:#AA0000; ">
        <div class="grid-row clear-fix">
            <div class="copyright">alumni CCS<span></span> <?= date('Y'); ?>. All Rights Reserved</div>
            <nav class="footer-nav">
                <p style="color:black;">Design & Developed by  <a  target="_blank"href="http://logicandthoughts.com/">Logic & Thoughts</a>.</p>

            </nav>
        </div>
    </div>
</footer>

<div class="modal fade" id="myModal"tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
    <div class="modal-dialog">
        <button type="button" class="btn btn-danger pull-right" data-dismiss="modal">X</button>
        <center>
            <div class="grid-col grid-col-6">
                <div class="video-player">
                    <iframe src="https://www.youtube.com/embed/HVHE-g9PgsY"></iframe>
                </div>
            </div>
        </center>
    </div>
</div>


<script type="text/javascript" src="<?= base_url(); ?>assets/js/jquery.easing.min.js"></script>
<script type="text/javascript" src="<?= base_url(); ?>assets/js/jquery.easy-ticker.js"></script>


<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
<script type='text/javascript' src='<?= base_url(); ?>assets/js/jquery.validate.min.js'></script>
<script src="<?= base_url(); ?>assets/js/jquery.form.min.js"></script>
<script src="<?= base_url(); ?>assets/js/TweenMax.min.js"></script>
<script src="<?= base_url(); ?>assets/js/main.js"></script>
<!-- jQuery REVOLUTION Slider  -->
<script type="text/javascript" src="<?= base_url(); ?>assets/rs-plugin/js/jquery.themepunch.tools.min.js"></script>
<script type="text/javascript" src="<?= base_url(); ?>assets/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>
<script src="<?= base_url(); ?>assets/js/jquery.isotope.min.js"></script>
<script src="<?= base_url(); ?>assets/js/owl.carousel.min.js"></script>
<script src="<?= base_url(); ?>assets/js/jquery-ui.min.js"></script>
<script src="<?= base_url(); ?>assets/js/jflickrfeed.min.js"></script>
<script src="<?= base_url(); ?>assets/js/jquery.tweet.js"></script>
<script type='text/javascript' src='<?= base_url(); ?>assets/tuner/js/colorpicker.js'></script>
<script type='text/javascript' src='<?= base_url(); ?>assets/tuner/js/scripts.js'></script>
<script src="<?= base_url(); ?>assets/js/jquery.fancybox.pack.js"></script>
<script src="<?= base_url(); ?>assets/js/jquery.fancybox-media.js"></script>
<script src="<?= base_url(); ?>assets/js/retina.min.js"></script>
<script type="text/javascript">
    $("#myModal").on('hidden.bs.modal', function(e) {
        $("#myModal iframe").attr("src", $("#myModal iframe").attr("src"));
    });


    $(document).ready(function() {

        var dd = $('.vticker').easyTicker({
            direction: 'up',
            easing: 'easeInOutBack',
            speed: 'slow',
            interval: 1200,
            height: 'auto',
            visible: 0,
            mousePause: 1

        }).data('easyTicker');

        cc = 1;
        $('.aa').click(function() {
            $('.vticker ul').append('<li>' + cc + ' Triangles can be made easily using CSS also without any images. This trick requires only div tags and some</li>');
            cc++;
        });

        $('.vis').click(function() {
            dd.options['visible'] = 3;

        });

        $('.visall').click(function() {
            dd.stop();
            dd.options['visible'] = 0;
            dd.start();
        });

    });
    (function() {
        var options = {
            facebook: "157089424390855", // Facebook page ID
            call_to_action: "Message us", // Call to action
            position: "left" // Position may be 'right' or 'left'
        };
        var proto = document.location.protocol, host = "whatshelp.io", url = proto + "//static." + host;
        var s = document.createElement('script');
        s.type = 'text/javascript';
        s.async = true;
        s.src = url + '/widget-send-button/js/init.js';
        s.onload = function() {
            WhWidgetSendButton.init(host, proto, options);
        };
        var x = document.getElementsByTagName('script')[0];
        x.parentNode.insertBefore(s, x);
    })();
</script>
<script src="<?= base_url(); ?>assets/slider/js/jssor.slider-27.5.0.min.js" type="text/javascript"></script>
<script type="text/javascript">
    jQuery(document).ready(function($) {

        var jssor_1_SlideoTransitions = [
            [{b: -1, d: 1, o: -0.7}],
            [{b: 900, d: 2000, x: -379, e: {x: 7}}],
            [{b: 900, d: 2000, x: -379, e: {x: 7}}],
            [{b: -1, d: 1, o: -1, sX: 2, sY: 2}, {b: 0, d: 900, x: -171, y: -341, o: 1, sX: -2, sY: -2, e: {x: 3, y: 3, sX: 3, sY: 3}}, {b: 900, d: 1600, x: -283, o: -1, e: {x: 16}}]
        ];

        var jssor_1_options = {
            $AutoPlay: 1,
            $SlideDuration: 900,
            $SlideEasing: $Jease$.$OutQuint,
            $CaptionSliderOptions: {
                $Class: $JssorCaptionSlideo$,
                $Transitions: jssor_1_SlideoTransitions
            },
            $ArrowNavigatorOptions: {
                $Class: $JssorArrowNavigator$
            },
            $BulletNavigatorOptions: {
                $Class: $JssorBulletNavigator$
            }
        };

        var jssor_1_slider = new $JssorSlider$("jssor_1", jssor_1_options);

        /*#region responsive code begin*/

        var MAX_WIDTH = 3000;

        function ScaleSlider() {
            var containerElement = jssor_1_slider.$Elmt.parentNode;
            var containerWidth = containerElement.clientWidth;

            if (containerWidth) {

                var expectedWidth = Math.min(MAX_WIDTH || containerWidth, containerWidth);

                jssor_1_slider.$ScaleWidth(expectedWidth);
            }
            else {
                window.setTimeout(ScaleSlider, 40);
            }
        }

        ScaleSlider();

        $(window).bind("load", ScaleSlider);
        $(window).bind("resize", ScaleSlider);
        $(window).bind("orientationchange", ScaleSlider);
        /*#endregion responsive code end*/
    });
</script>