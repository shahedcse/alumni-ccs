<div class="page-title" style="background-image: url(<?= base_url(); ?>assets/pic/1920-1280-img-2.jpg)">
    <div class="grid-row">
        <h1>Contact</h1>
        <nav class="bread-crumb">
            <a href="<?= base_url(); ?>">Home</a>
            <i class="fa fa-long-arrow-right"></i>
            <a href="#">Contact</a>

        </nav>
    </div>
</div>
<div class="page-content grid-row">
    <main>
        <div class="grid-row clear-fix">
            <div class="grid-col-row">
                <div class="grid-col grid-col-8">
                    <section>
                        <h2>Contact us</h2>
                        <div class="widget-contact-form">
                            <div  id="success" style="display:none">
                                <div class="alert alert-success"> <strong>Success!</strong> message send Successfully. </div>
                            </div>

                            <div  id="failed" style="display:none">
                                <div class="alert alert-danger"> <strong>Alert!</strong> Please fill Up all fields. </div>
                            </div>
                            <div class="email_server_responce"></div>
                            <form action="#" class="contact-form" method="post" >
                                <p><span class="your-name"><input type="text" name="name" id="name" required="" value="" size="40" placeholder="Name" aria-invalid="false" required></span>
                                </p>
                                <p><span class="your-email"><input type="text" name="phone"onkeypress='return isNumberKey(event)' maxlength="11"id="phone" required="" value="" size="40" placeholder="Phone" aria-invalid="false" required></span> </p>
                                <p><span class="your-message"><textarea name="message" id="message" required="" placeholder="Comments" cols="40" rows="5" aria-invalid="false" required></textarea></span> </p>
                                <button onclick="send_message();" type="button" class="cws-button bt-color-3 border-radius alt icon-right">Submit <i class="fa fa-angle-right"></i></button>
                            </form>
                        </div>
                    </section>
                </div>
                <div class="grid-col grid-col-4 widget-address">
                    <section>
                        <h2>Our Address</h2>
                        <address>
                            <p>Chittagong Collegiate School,3 rd floor,School building</p>

                            <p><strong class="fs-18">Phone number:</strong><br>
                                <a href="#">+8801700000000</a><br>
                                <a href="#">+8801700000000</a>
                            </p>
                            <p><strong class="fs-18">E-mail:</strong><br>
                                <a href="#">info@alumniccs.com</a><br>
                            </p>
                        </address>
                    </section>
                </div>
            </div>
        </div>
    </main>
</div>
<script>
    function isNumberKey(evt) {
        var charCode = (evt.which) ? evt.which : evt.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;
        return true;
    }

    function send_message() {
        var name = $("#name").val();
        var phone = $("#phone").val();
        var message = $("#message").val();

        if (name == '' || phone == '' || message == '') {
            $("#failed").show();
            $("#success").hide();
        }
        else {
            $.ajax({
                type: "GET",
                url: "<?= base_url('About/send_message'); ?>",
                data: {
                    name: name,
                    phone: phone,
                    message: message
                }
            });
            $("#name").val("");
            $("#phone").val("");
            $("#message").val("");
            $("#failed").hide();
            $("#success").show();
        }
    }
</script>


