<main>
    <section class="fullwidth-background bg-2">
        <div class="grid-row">
            <div class="login-block">
                <div class="logo">
                    <img src="<?= base_url(); ?>assets/img/logo.png" data-at2x="img/logo@2x.png" alt="">
                    <h2>Aliumni CCS</h2>
                </div>
                <?php
                if ($this->session->userdata('add')):
                    echo '<div class="info-boxes confirmation-message"><div class="info-box-icon"><i class="fa fa-check"></i></div><strong>Congrats !</strong><br>' . $this->session->userdata('add') . '<div class="close-button"></div></div>';
                    $this->session->unset_userdata('add');
                elseif ($this->session->userdata('login_error')):
                    echo '<div class="info-boxes error-message"><div class="info-box-icon"><i class="fa fa-times"></i></div><strong>Sorry !!</strong><br>' . $this->session->userdata('login_error') . '<div class="close-button"></div> </div>';
                    $this->session->unset_userdata('login_error');
                endif;
                ?>
                <div class="clear-both"></div>
                <div class="login-or">
                    <hr class="hr-or">
                    <span class="span-or">Login</span>
                </div>
                <form class="login-form" action="<?= base_url('Auth/login');   ?>" method="POST">
                    <div class="form-group">
                        <input type="text" name="lm_no" class="login-input" required="" placeholder="LM No.">
                        <span class="input-icon">
                            <i class="fa fa-user"></i>
                        </span>
                    </div>
                    <div class="form-group">
                        <input type="password" name="password" class="login-input" required="" placeholder="Password">
                        <span class="input-icon">
                            <i class="fa fa-lock"></i>
                        </span>
                    </div>
                    <button type="submit" class="button-fullwidth cws-button bt-color-3 border-radius">LOG IN</button>
                    <p>Have no Account? <a href="<?= base_url('Auth/registration'); ?>">Click Here to registration</a></p>

                </form>
            </div>
        </div>
    </section>
</main>