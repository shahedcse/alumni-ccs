<div class="page-title" style="background-image: url(<?= base_url(); ?>assets/pic/1920-1280-img-2.jpg)">
    <div class="grid-row">
        <h1>Blog & News</h1>
        <nav class="bread-crumb">
            <a href="<?= base_url(); ?>">Home</a>
            <i class="fa fa-long-arrow-right"></i>
            <a href="#">Blog & News</a>

        </nav>
    </div>
</div>
<div class="page-content grid-row">
    <main>
        <div class="page-content grid-col-row clear-fix">
            <div class="grid-col grid-col-9">
                <main>
                    <div class="blog-post">
                        <?php foreach ($all_blog as $value): ?>
                            <article>
                                <div class="post-info">
                                    <div class="date-post"><div class="day"><?= date("d", strtotime($value->created_date)) ?></div><div class="month"><?= date("M", strtotime($value->created_date)) ?></div></div>
                                    <div class="post-info-main">
                                        <div class="author-post"><?= $value->name; ?></div>
                                    </div>
                                    <div class="comments-post"><i class="fa fa-comment"></i>
                                        <?php
                                        $comments = $this->db->query("SELECT COUNT(id) as totl FROM blog_comment where blog_id='$value->id'")->row()->totl;
                                        if (!empty($comments)):
                                            echo $comments;
                                        else:
                                            echo '';
                                        endif;
                                        ?>
                                    </div>
                                </div>
                                <div class="blog-media picture2">
                                    <div class="hover-effect"></div>
                                    <img src="<?= base_url(); ?>assets/img/blog/<?= $value->fetured_image ?>" data-at2x="<?= base_url(); ?>assets/img/blog/<?= $value->fetured_image ?>" class="columns-col-8" alt="">
                                </div>
                                <h3><?= $value->blog_tilte; ?></h3>
                                <p>
                                    <?php
                                    $big = "$value->details";
                                    $string = substr($big, 0, 1200) . '...';
                                    echo $string;
                                    ?>
                                    <a href="<?= base_url('Blog/blog_view?id=' . $value->id); ?>" class="cws-button border-radius alt icon-right">Read More <i class="fa fa-angle-right"></i></a>
                                </p>

                            </article>
                        <?php endforeach; ?>
                    </div>
                    <!-- / post item -->
                    <hr class="divider-color">

                    <div class="page-pagination clear-fix">
                        <a href="#"><i class="fa fa-angle-double-left"></i></a><!--
                        --><a href="#">1</a><!-- 
                        --><a href="#">2</a><!-- 
                        --><a href="#" class="active">3</a><!-- 
                        --><a href="#"><i class="fa fa-angle-double-right"></i></a>
                    </div>
                    <!-- / pagination -->
                </main>
            </div>
            <div class="grid-col grid-col-3 sidebar">
                <?php $this->load->view('web/blog_sidebar'); ?>
            </div>
        </div>
    </main>
</div>

