<div class="content-wrapper" style="margin-top: 50px;">
    <div class="container">
        <div class="alumni-directory">
            <?php
            if ($this->session->userdata('add')):
                echo '<div class="alert alert-success fade in"><button data-dismiss="alert" class="close close-sm" type="button"><i class="fa fa-times"></i></button><strong>Success Message !!! </strong> ' . $this->session->userdata('add') . '</div>' . '<br>' . '<br>';
                $this->session->unset_userdata('add');
            elseif ($this->session->userdata('notadd')):
                echo '<div class="alert alert-block alert-danger fade in"><button data-dismiss="alert" class="close close-sm" type="button"><i class="fa fa-times"></i></button><strong>Failed Meaasge !!! </strong> ' . $this->session->userdata('notadd') . '</div>';
                $this->session->unset_userdata('notadd');
            endif;
            ?>
            <div class="top-section">
                <div class="row">
                    <div class="title-page text-left col-md-6 col-sm-12 col-xs-12">
                        <h4 class="text-regular">Profession Wise member List.</h4>
                    </div>

                </div>
            </div>
            <div class="panel-body" style="padding: 25px; background-color:  #a6c3f9 ">
                <div class="form-group">
                    <form action="<?= base_url('Member/professional_list'); ?>" method="POST">
                        <label class="col-md-3 control-label">Profession Name:</label>
                        <div class="col-md-6 inputGroupContainer">
                            <div class="input-group">
                                <span class="input-group-addon" style="max-width: 100%;"><i class="glyphicon glyphicon-list"></i></span>
                                <select class="selectpicker form-control" required="true"id="Profession" name="Profession">
                                    <option value="">--Select--</option>
                                    <?php
                                    foreach ($profession as $value):
                                        if ($pro_name == $value->p_name):
                                            ?>
                                            <option value="<?= $value->p_name; ?>"selected=""><?= $value->p_name; ?></option>
                                        <?php else: ?>
                                            <option value="<?= $value->p_name; ?>"><?= $value->p_name; ?></option>
                                        <?php
                                        endif;
                                    endforeach;
                                    ?>
                                </select>
                            </div>
                        </div><br>
                        <button type="submit"class="btn btn-success btn-lg">Search</button>
                    </form>
                </div>
            </div>
            <div class="alumni-directory-content">

                <table id="example" class="table table-striped table-bordered">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Photo</th>
                            <th>present Location</th>
                            <th>Profession</th>
                            <th>Field</th>
                            <th>Specialization</th>
                            <th>Contact</th>
                        </tr>
                    </thead>
                    <?php if (!empty($allmember)): ?>
                        <tbody>
                            <?php
                            if (sizeof($allmember)):
                                foreach ($allmember as $value):
                                    ?>
                                    <tr>
                                        <td><?= $value->name; ?></td>
                                        <td> <img class="zoom"  src="<?= $base_url ?>assets/images/webimg/<?= $value->image_path; ?>" alt=""> </td>
                                        <td><?= $value->present_loc; ?></td>
                                        <td><?= $value->Profession; ?> </td>
                                        <td><?= $value->profession_field; ?></td>
                                        <td><?= $value->specialization; ?> </td>
                                        <td>
                                            <?php if (!empty($value->fblink)): ?>
                                                <a  target="_blank" href="<?= $value->fblink; ?>">
                                                    <img src="<?= base_url(); ?>assets/images/fbprofile.png" style="width: 50px; height: 50px;">
                                                </a>
                                            <?php else: ?>
                                                N/A
                                            <?php endif; ?>
                                        </td>
                                    </tr>
                                    <?php
                                endforeach;
                            endif;
                            ?>
                        </tbody>
                    <?php else: ?>
                        <tbody>
                            <tr>
                                <td   style="text-align: center; color: red"colspan="7">No data available.</td>
                            </tr>
                        </tbody>
                    <?php endif; ?>
                </table>

            </div>

        </div>
    </div>
</div>

