<aside class="widget-post">
    <h2>Recent Posts</h2>
    <div class="carousel-nav">
        <div class="carousel-button">
            <div class="prev"><i class="fa fa-angle-double-left"></i></div><!-- 
            --><div class="next"><i class="fa fa-angle-double-right"></i></div>
        </div>
    </div>
    <hr class="divider-big">
    <div class="owl-carousel widget-carousel owl-theme" style="opacity: 1; display: block;">
        <div class="owl-wrapper-outer">
            <?php
            $all_blog = $this->db->query("SELECT blog.*,users.name FROM blog JOIN users ON users.id=blog.created_by order by id desc LIMIT 3")->result();
            ?>
            <div class="owl-wrapper" style="width: 1080px; left: 0px; display: block;"><div class="owl-item" style="width: 270px;">
                    <div>
                        <?php foreach ($all_blog AS $value): ?>
                            <article class="clear-fix">
                                <img  height="40px;" width="50px;"src="<?= base_url(); ?>assets/img/blog/<?= $value->fetured_image; ?>" data-at2x="<?= base_url(); ?>assets/pic/60x60-img-3@2x.jpg" alt="">
                                <h4><a href="<?= base_url('Blog/blog_view?id=' . $value->id); ?>"><?= $value->blog_tilte; ?></a></h4>

                            </article>
                        <?php endforeach; ?>
                    </div>
                </div>

            </div>
        </div>

    </div>
</aside>
<aside class="widget-categories">
    <h2>Categories</h2>
    <hr class="divider-big">
    <?php
    $all_blogcat = $this->db->query("SELECT * FROM blog_category")->result();
    ?>
    <ul>
        <?php foreach ($all_blogcat AS $value): ?>
            <li class="cat-item cat-item-1 current-cat"><a href="#"><?= $value->category_name; ?></a></li>
        <?php endforeach; ?>

    </ul>
</aside>


<aside class="widget-calendar">
    <h2>Calendar</h2>
    <hr class="divider-big">
    <div id="calendar" class="hasDatepicker"><div class="ui-datepicker-inline ui-datepicker ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" style="display: block;"><div class="ui-datepicker-header ui-widget-header ui-helper-clearfix ui-corner-all"><a class="ui-datepicker-prev ui-corner-all" data-handler="prev" data-event="click" title="<i class=&quot;fa fa-angle-double-left&quot;></i>"><span class="ui-icon ui-icon-circle-triangle-w"><i class="fa fa-angle-double-left"></i></span></a><a class="ui-datepicker-next ui-corner-all" data-handler="next" data-event="click" title="<i class=&quot;fa fa-angle-double-right&quot;></i>"><span class="ui-icon ui-icon-circle-triangle-e"><i class="fa fa-angle-double-right"></i></span></a><div class="ui-datepicker-title"><span class="ui-datepicker-month">September</span>&nbsp;<span class="ui-datepicker-year">2019</span></div></div><table class="ui-datepicker-calendar"><thead><tr><th><span title="Monday">M</span></th><th><span title="Tuesday">T</span></th><th><span title="Wednesday">W</span></th><th><span title="Thursday">T</span></th><th><span title="Friday">F</span></th><th class="ui-datepicker-week-end"><span title="Saturday">S</span></th><th class="ui-datepicker-week-end"><span title="Sunday">S</span></th></tr></thead><tbody><tr><td class=" ui-datepicker-other-month ui-datepicker-unselectable ui-state-disabled">&nbsp;</td><td class=" ui-datepicker-other-month ui-datepicker-unselectable ui-state-disabled">&nbsp;</td><td class=" ui-datepicker-other-month ui-datepicker-unselectable ui-state-disabled">&nbsp;</td><td class=" ui-datepicker-other-month ui-datepicker-unselectable ui-state-disabled">&nbsp;</td><td class=" ui-datepicker-other-month ui-datepicker-unselectable ui-state-disabled">&nbsp;</td><td class=" ui-datepicker-week-end ui-datepicker-other-month ui-datepicker-unselectable ui-state-disabled">&nbsp;</td><td class=" ui-datepicker-week-end " data-handler="selectDay" data-event="click" data-month="8" data-year="2019"><a class="ui-state-default" href="#">1</a></td></tr><tr><td class=" " data-handler="selectDay" data-event="click" data-month="8" data-year="2019"><a class="ui-state-default" href="#">2</a></td><td class=" ui-datepicker-days-cell-over  ui-datepicker-current-day ui-datepicker-today" data-handler="selectDay" data-event="click" data-month="8" data-year="2019"><a class="ui-state-default ui-state-highlight ui-state-active" href="#">3</a></td><td class=" " data-handler="selectDay" data-event="click" data-month="8" data-year="2019"><a class="ui-state-default" href="#">4</a></td><td class=" " data-handler="selectDay" data-event="click" data-month="8" data-year="2019"><a class="ui-state-default" href="#">5</a></td><td class=" " data-handler="selectDay" data-event="click" data-month="8" data-year="2019"><a class="ui-state-default" href="#">6</a></td><td class=" ui-datepicker-week-end " data-handler="selectDay" data-event="click" data-month="8" data-year="2019"><a class="ui-state-default" href="#">7</a></td><td class=" ui-datepicker-week-end " data-handler="selectDay" data-event="click" data-month="8" data-year="2019"><a class="ui-state-default" href="#">8</a></td></tr><tr><td class=" " data-handler="selectDay" data-event="click" data-month="8" data-year="2019"><a class="ui-state-default" href="#">9</a></td><td class=" " data-handler="selectDay" data-event="click" data-month="8" data-year="2019"><a class="ui-state-default" href="#">10</a></td><td class=" " data-handler="selectDay" data-event="click" data-month="8" data-year="2019"><a class="ui-state-default" href="#">11</a></td><td class=" " data-handler="selectDay" data-event="click" data-month="8" data-year="2019"><a class="ui-state-default" href="#">12</a></td><td class=" " data-handler="selectDay" data-event="click" data-month="8" data-year="2019"><a class="ui-state-default" href="#">13</a></td><td class=" ui-datepicker-week-end " data-handler="selectDay" data-event="click" data-month="8" data-year="2019"><a class="ui-state-default" href="#">14</a></td><td class=" ui-datepicker-week-end " data-handler="selectDay" data-event="click" data-month="8" data-year="2019"><a class="ui-state-default" href="#">15</a></td></tr><tr><td class=" " data-handler="selectDay" data-event="click" data-month="8" data-year="2019"><a class="ui-state-default" href="#">16</a></td><td class=" " data-handler="selectDay" data-event="click" data-month="8" data-year="2019"><a class="ui-state-default" href="#">17</a></td><td class=" " data-handler="selectDay" data-event="click" data-month="8" data-year="2019"><a class="ui-state-default" href="#">18</a></td><td class=" " data-handler="selectDay" data-event="click" data-month="8" data-year="2019"><a class="ui-state-default" href="#">19</a></td><td class=" " data-handler="selectDay" data-event="click" data-month="8" data-year="2019"><a class="ui-state-default" href="#">20</a></td><td class=" ui-datepicker-week-end " data-handler="selectDay" data-event="click" data-month="8" data-year="2019"><a class="ui-state-default" href="#">21</a></td><td class=" ui-datepicker-week-end " data-handler="selectDay" data-event="click" data-month="8" data-year="2019"><a class="ui-state-default" href="#">22</a></td></tr><tr><td class=" " data-handler="selectDay" data-event="click" data-month="8" data-year="2019"><a class="ui-state-default" href="#">23</a></td><td class=" " data-handler="selectDay" data-event="click" data-month="8" data-year="2019"><a class="ui-state-default" href="#">24</a></td><td class=" " data-handler="selectDay" data-event="click" data-month="8" data-year="2019"><a class="ui-state-default" href="#">25</a></td><td class=" " data-handler="selectDay" data-event="click" data-month="8" data-year="2019"><a class="ui-state-default" href="#">26</a></td><td class=" " data-handler="selectDay" data-event="click" data-month="8" data-year="2019"><a class="ui-state-default" href="#">27</a></td><td class=" ui-datepicker-week-end " data-handler="selectDay" data-event="click" data-month="8" data-year="2019"><a class="ui-state-default" href="#">28</a></td><td class=" ui-datepicker-week-end " data-handler="selectDay" data-event="click" data-month="8" data-year="2019"><a class="ui-state-default" href="#">29</a></td></tr><tr><td class=" " data-handler="selectDay" data-event="click" data-month="8" data-year="2019"><a class="ui-state-default" href="#">30</a></td><td class=" ui-datepicker-other-month ui-datepicker-unselectable ui-state-disabled">&nbsp;</td><td class=" ui-datepicker-other-month ui-datepicker-unselectable ui-state-disabled">&nbsp;</td><td class=" ui-datepicker-other-month ui-datepicker-unselectable ui-state-disabled">&nbsp;</td><td class=" ui-datepicker-other-month ui-datepicker-unselectable ui-state-disabled">&nbsp;</td><td class=" ui-datepicker-week-end ui-datepicker-other-month ui-datepicker-unselectable ui-state-disabled">&nbsp;</td><td class=" ui-datepicker-week-end ui-datepicker-other-month ui-datepicker-unselectable ui-state-disabled">&nbsp;</td></tr></tbody></table></div></div>
    <hr class="margin-top">
</aside>



