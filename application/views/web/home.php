<!DOCTYPE HTML>
<style>
    .style2:hover {
        //  z-index: 1;
        -webkit-transition: all 100ms ease-in;
        -webkit-transform: scale(1.1);
        -ms-transition: all .4s ease-in;
        -ms-transform: scale(1.1);
        -moz-transition: all .4s ease-in;
        -moz-transform: scale(1.1);
        transition: all .4s ease-in;
        transform: scale(1.1);
    }
</style>
<html>
    <body>
        <div id="jssor_1" style="position:relative;margin:0 auto;top:0px;left:0px;width:1300px;height:430px;overflow:hidden;visibility:hidden;">
            <!-- Loading Screen -->
            <div data-u="loading" class="jssorl-009-spin" style="position:absolute;top:0px;left:0px;width:100%;height:100%;text-align:center;background-color:rgba(0,0,0,0.7);">
                <img style="margin-top:-19px;position:relative;top:50%;width:38px;height:38px;" src="<?= base_url(); ?>assets/slider/img/spin.svg" />
            </div>
            <div data-u="slides" style="cursor:default;position:relative;top:0px;left:0px;width:1300px;height:500px;overflow:hidden;">
                <div>
                    <img data-u="image" src="<?= base_url(); ?>assets/slider/img/slider1.jpg" />
                    <div style="position:absolute;top:300px;left:30px;width:580px;height:130px;font-family:'Roboto Condensed',sans-serif;font-size:40px;color:#fff;line-height:1.5;padding:5px 5px 5px 5px;box-sizing:border-box;;">
                        Alumni are the heart of CCS
                    </div>
                </div>
                <div>
                    <img data-u="image" src="<?= base_url(); ?>assets/slider/img/71287547_455492781735453_6485111402452221952_n.jpg" />
                </div>
                <div>
                    <img data-u="image" src="<?= base_url(); ?>assets/slider/img/slider3.jpg" />

                </div>
                <div>
                    <img data-u="image" src="<?= base_url(); ?>assets/slider/img/slider4.jpg" />
                    <div style="position:absolute;top:300px;left:30px;width:580px;height:130px;font-family:'Roboto Condensed',sans-serif;font-size:40px;color:#fff;line-height:1.5;padding:5px 5px 5px 5px;box-sizing:border-box;;">
                        Alumni Entrepreneurs are the Voice of CCS
                    </div>
                </div>

            </div>
            <!-- Bullet Navigator -->
            <div data-u="navigator" class="jssorb032" style="position:absolute;bottom:12px;right:12px;" data-autocenter="1" data-scale="0.5" data-scale-bottom="0.75">
                <div data-u="prototype" class="i" style="width:16px;height:16px;">
                    <svg viewbox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
                    <circle class="b" cx="8000" cy="8000" r="5800"></circle>
                    </svg>
                </div>
            </div>
            <!-- Arrow Navigator -->
            <div data-u="arrowleft" class="jssora051" style="width:65px;height:65px;top:0px;left:25px;" data-autocenter="2" data-scale="0.75" data-scale-left="0.75">
                <svg viewbox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
                <polyline class="a" points="11040,1920 4960,8000 11040,14080 "></polyline>
                </svg>
            </div>
            <div data-u="arrowright" class="jssora051" style="width:65px;height:65px;top:0px;right:25px;" data-autocenter="2" data-scale="0.75" data-scale-right="0.75">
                <svg viewbox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
                <polyline class="a" points="4960,1920 11040,8000 4960,14080 "></polyline>
                </svg>
            </div>
        </div>


        <div id="home" class="page-content padding-none">
            <!-- section -->
            <section class="fullwidth-background padding-section">
                <div class="grid-row clear-fix">
                    <h3 class="center-text" style="color:red">Welcome to Chattogram collegiate school alumni association.<br>
                        <a href="<?= base_url('Auth/registration'); ?>"  class="cws-button  border-radius">Register as an Alumni</a>
                    </h3>

                    <div class="grid-col-row">
                        <div class="grid-col grid-col-6 clear-fix">
                            <h3>School Story</h3>
                            <p>
                                বন্দর নগরী চট্টগ্রামে ১৮৩৬ সালে চট্টগ্রাম কলেজিয়েট স্কুল প্রতিষ্ঠিত হয়। সে সময় এটির কার্যক্রম পরিচালিত হত বর্তমান চট্টগ্রাম কলেজ ক্যাম্পাসে। সময়ের সাথে সাথে উচ্চশিক্ষা প্রতিষ্ঠানের প্রয়োজনীয়তা দেখা দেয়ায় ১৮৬৯ সালে এখানে কলেজ শাখা খোলা হয়। তখন এ দু'টি শাখার নাম একত্রে রাখা হয় চট্টগ্রাম স্কুল এন্ড কলেজ। ক্রমান্বয়ে কলেজ শাখার ছাত্র বৃদ্ধির ফলে জায়গা সংকুলানের জন্য ১৯২৫ সালে স্কুল শাখাটিকে "চট্টগ্রাম কলেজিয়েট স্কুল" নামকরণ করে একতলা লাল বিল্ডিং তৈরি করে বর্তমান জায়গায় স্থানান্তর করা হয়। কলেজ শাখাটি "চট্টগ্রাম কলেজ" নামধারণ করে ঐ জায়গায় থেকে যায়।তবে ১৯২৫ সালের পূর্বে কিছু কাল স্কুলটির নাম ছিল চট্টগ্রাম জিলা স্কুল। 
                            </p> <a href="<?= base_url('About'); ?>" class="cws-button bt-color-3 border-radius alt icon-right float-right">Read More<i class="fa fa-angle-right"></i></a>
                        </div>
                        <div class="grid-col grid-col-6 clear-fix">
                            <aside class="widget-event" style="padding: 15px;border: 1px solid grey;" >
                                <h2>Our Events</h2>
                                <div class="vticker">
                                    <ul>
                                        <?php if (!empty($allevents)): ?>
                                            <?php foreach ($allevents AS $value): ?>
                                                <li>
                                                    <article class="clear-fix">
                                                        <a href="<?= base_url('About/event_details?id=' . $value->id); ?>">
                                                            <div class="date"><div class="day"><?= date("d", strtotime($value->date)) ?></div><div class="month"><?= date("M", strtotime($value->date)) ?></div></div>
                                                            <div class="event-description"><span><?= $value->place ?></span><p style="font-size: 16px;"><?= $value->event_tilte ?> </p></div>
                                                        </a>
                                                    </article>
                                                </li>
                                            <?php endforeach; ?>
                                        <?php else: ?>
                                            <li>
                                                <article class="clear-fix">
                                                    <div class="event-description"><p style="color:red;font-size: 16px;">No Event Available Now !!</p></div>
                                                </article>
                                            </li>
                                        <?php endif; ?>
                                    </ul>
                                </div>
                                <a href="<?= base_url('About/allevents'); ?>" style="margin-top: 20px;" class="cws-button bt-color-3 border-radius alt icon-right float-right">See all events<i class="fa fa-angle-right"></i></a>
                            </aside>
                        </div>
                    </div>
                </div>
            </section>

            <hr class="divider-color"/>
            <section class="padding-section">
                <div class="grid-row clear-fix">
                    <div class="grid-col-row">
                        <div class="grid-col grid-col-6">
                            <div class="video-player">
                                <iframe src="https://www.youtube.com/embed/HVHE-g9PgsY"></iframe>
                            </div>
                        </div>
                        <div class="grid-col grid-col-6 clear-fix">
                            <h2>A short story of Chattogram Collegiates</h2>
                            <p>[..............]</p>
                            <br>
                            <br>

                            <a href="<?= base_url('Auth/registration'); ?>" style="margin-top: 20px;" class="cws-button bt-color-3 border-radius alt icon-right float-right">Register Now<i class="fa fa-angle-right"></i></a>
                        </div>
                    </div>
                </div>
            </section>
            <hr class="divider-color"/>

            <section class="grid-row clear-fix padding-section">
                <h2 class="center-text">Our Executive Committee </h2>
                <div class="grid-col-row">
                    <div class="grid-col grid-col-6">
                        <!-- instructor item -->
                        <div class="item-instructor bg-color-1">
                            <a href="" class="instructor-avatar style2">
                                <img src="<?= base_url(); ?>assets/committe/1.jpg" data-at2x="pic/210x220-img-1@2x.jpg" alt>
                            </a>
                            <div class="info-box">
                                <h3>এম. এ মালেক</h3>
                                <span class="instructor-profession">সভাপতি</span>
                                <div class="divider"></div>
                                <div class="social-link"><!-- 
                                    --><a href="#" class="fa fa-facebook"></a>

                                </div>
                            </div>
                        </div>
                        <div class="item-instructor bg-color-3">
                            <a href="" class="instructor-avatar style2">
                                <img src="<?= base_url(); ?>assets/pic/demo.jpg" data-at2x="pic/210x220-img-4@2x.jpg" alt>
                            </a>
                            <div class="info-box">
                                <h3>আলহাজ্ব নবী দোভাষ</h3>
                                <span class="instructor-profession">সহ-সভাপতি</span>
                                <div class="divider"></div>

                                <div class="social-link"><!-- 
                                    --><a href="#" class="fa fa-facebook"></a>
                                </div>
                            </div>
                        </div>

                        <!-- / instructor item -->
                    </div>
                    <div class="grid-col grid-col-6">
                        <!-- instructor item -->
                        <div class="item-instructor bg-color-1">
                            <a href="" class="instructor-avatar style2">
                                <img src="<?= base_url(); ?>assets/committe/2.jpg" data-at2x="pic/210x220-img-2@2x.jpg" alt>
                            </a>
                            <div class="info-box">
                                <h3> আমীর হুমায়ুন মাহমুদ চৌধুরী</h3>
                                <span class="instructor-profession">সিনিয়র-সভাপতি</span>
                                <div class="divider"></div>

                                <div class="social-link"><!-- 
                                    --><a href="#" class="fa fa-facebook"></a>

                                </div>
                            </div>
                        </div>
                        <!-- / instructor item -->
                        <!-- instructor item -->
                        <div class="item-instructor bg-color-3">
                            <a href="" class="instructor-avatar style2">
                                <img src="<?= base_url(); ?>assets/pic/demo.jpg" data-at2x="pic/210x220-img-3@2x.jpg" alt>
                            </a>
                            <div class="info-box">
                                <h3>মোহাম্মদ মোস্তাক হোসাইন</h3>
                                <span class="instructor-profession">সাধারণ সম্পাদক</span>
                                <div class="divider"></div>

                                <div class="social-link"><!-- 
                                    --><a href="#" class="fa fa-facebook"></a>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

                <div class="grid-col-row">
                    <div class="grid-col grid-col-6" style="margin-top: 30px;">
                        <!-- instructor item -->
                        <div class="item-instructor bg-color-3">
                            <a href="" class="instructor-avatar style2">
                                <img src="<?= base_url(); ?>assets/committe/3.jpg" data-at2x="pic/210x220-img-1@2x.jpg" alt>
                            </a>
                            <div class="info-box">
                                <h3>আ.ন.ম ওয়াহিদ দুলাল</h3>
                                <span class="instructor-profession">যুগ্ম সাধারণ সম্পাদক </span>
                                <div class="divider"></div>

                                <div class="social-link"><!-- 
                                    --><a href="#" class="fa fa-facebook"></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="grid-col grid-col-6" style="margin-top: 30px;">
                        <!-- instructor item -->
                        <div class="item-instructor bg-color-3">
                            <a href="" class="instructor-avatar style2">
                                <img src="<?= base_url(); ?>assets/committe/6.jpg" data-at2x="pic/210x220-img-2@2x.jpg" alt>
                            </a>
                            <div class="info-box">
                                <h3>এ.এইচ.এম. আশরাফ উল আনোয়ার হিরন</h3>
                                <span class="instructor-profession">সাংগঠনিক সম্পাদক</span>
                                <div class="divider"></div>

                                <div class="social-link"><!-- 
                                    --><a href="#" class="fa fa-facebook"></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <a href="<?= base_url('Member/executive_member') ?>" class="cws-button bt-color-3 border-radius alt icon-right float-right">All executive Committee<i class="fa fa-angle-right"></i></a>
            </section>
            <!-- / section -->
            <hr class="divider-color" />
            <!-- section -->
            <section class="padding-section">
                <div class="grid-row clear-fix">
                    <div class="grid-col-row">
                        <div class="grid-col grid-col-6">
                            <div class="owl-carousel full-width-slider owl-theme" style="opacity: 1; display: block;">
                                <div class="owl-wrapper-outer">
                                    <div class="owl-wrapper" style="width: 2196px; left: 0px; display: block;">
                                        <div class="owl-item" style="width: 549px;"><div class="gallery-item picture">
                                                <img src="<?= base_url(); ?>assets/pic/1920-1280-img-2.jpg" data-at2x="pic/570x380-img-2@2x.jpg" alt="">
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="grid-col grid-col-6 clear-fix">
                        <h2>Terms & Condition</h2>
                        <div class="columns-col columns-col-6">
                            <ul class="check-list">
                                <li>Aliquam justo lorem, commodo eget tristique</li>
                                <li>Curabitur vehicula leo accumsan, varius tellus </li>
                                <li>Pellentesque imperdiet, leo ut pulvinar facilisis</li>
                                <li>Сonvallis lectus, vitae condimentum nulla odio</li>
                            </ul>
                            <a href="<?= base_url('About/rules'); ?>" class="cws-button bt-color-3 border-radius alt icon-right float-right">See More<i class="fa fa-angle-right"></i></a>
                        </div>
                    </div>
                </div>
        </div>
    </section>

    <hr class="divider-color" />
    <!-- section -->
    <section class="fullwidth-background testimonial padding-section">
        <div class="grid-row">
            <h2 class="center-text">Testimonial & Messages</h2>
            <div class="owl-carousel testimonials-carousel">
                <div class="gallery-item">
                    <div class="quote-avatar-author clear-fix"><img src="<?= base_url(); ?>assets/pic/testtimonial.jpg" data-at2x="pic/94x94-img-1@2x.jpg" alt=""><div class="author-info">xyx<br><span>Writer</span></div></div>
                    <p>Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. </p>
                </div>
                <div class="gallery-item">
                    <div class="quote-avatar-author clear-fix"><img src="<?= base_url(); ?>assets/pic/testtimonial.jpg" data-at2x="pic/94x94-img-1@2x.jpg" alt=""><div class="author-info">xyz<br><span>Writer</span></div></div>
                    <p>Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. </p>
                </div>
                <div class="gallery-item">
                    <div class="quote-avatar-author clear-fix"><img src="<?= base_url(); ?>assets/pic/testtimonial.jpg" data-at2x="pic/94x94-img-1@2x.jpg" alt=""><div class="author-info">mnh<br><span>Writer</span></div></div>
                    <p>Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. </p>
                </div>
            </div>
        </div>
    </section>
</div>
</body>
</html>
<script>
    $(document).ready(function() {
        $('#myModal').modal('show');
    });
</script>



