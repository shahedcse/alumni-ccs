<link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
<script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>

<style>

    .dataTables_filter input {
        border: 1px solid black;
        color:black;
    }
</style>
<div class="page-title" style="background-image: url(<?= base_url(); ?>assets/pic/1920-1280-img-2.jpg)">
    <div class="grid-row">
        <h1> Member List</h1>
        <nav class="bread-crumb">
            <a href="<?= base_url(); ?>">Home</a>
            <i class="fa fa-long-arrow-right"></i>
            <a href="#">Member List</a>
        </nav>
    </div>
</div>
<div class="page-content">
    <div class="container">

        <h2>Our Alumni Member List.</h2>
        <table id="member" class="display" style="width:100%">
            <thead>

                <tr style="background-color:#AA0000;color:#fff; ">
                    <th style="text-align: center;">SL.</th>
                    <th style="text-align: center;">L.M No.</th>
                    <th style="text-align: center;">Name</th>
                    <th style="text-align: center;">Passing Year</th>
                    <th style="text-align: center;">Profession</th>
                    <th style="text-align: center;">Blood Group</th>
                    <th style="text-align: center;">Photo</th>
                    <?php if (!empty($this->session->userdata('user_id'))): ?>
                        <th style="text-align: center;">Phone No.</th>
                        <th style="text-align: center;">FB Link</th>
                    <?php endif; ?>
                </tr>
                </tr>
            </thead>
            <tbody>
                <?php
                $i = 0;
                foreach ($allmember as $value):
                    $i++;
                    ?>
                    <tr>
                        <td style="text-align: center;"><?= $i; ?></td>
                        <td style="text-align: center;"><?= 'L.M - ' . $value->lm_no; ?></td>
                        <td style="text-align: center;"><?= $value->name; ?></td>
                        <td style="text-align: center;"><?= $value->passing_year; ?></td>
                        <td style="text-align: center;"><?= $value->profession; ?></td>
                        <td style="text-align: center;"><?= $value->blood_group; ?></td>
                        <td style="text-align: center;">
                            <?php
                            $filename = 'assets/img/profile/' . $value->image_path;
                            if (file_exists($filename)):
                                ?>
                                <img height="45px;" width="45px;" src="<?= base_url('assets/img/profile/' . $value->image_path); ?>" alt="">
                            <?php else: ?>
                                <img height="45px;" width="45px;"src="<?= base_url('assets/img/profile/demo.jpg'); ?>" alt="">
                            <?php endif; ?>

                        </td>
                        <?php if (!empty($this->session->userdata('user_id'))): ?>
                            <td style="text-align: center;"><?= $value->phone; ?></td>
                            <td style="text-align: center;">
                                <a href="#">
                                    <i class="share-icon fa fa-facebook"></i>
                                </a>
                            </td>
                        <?php endif; ?>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>

</div>

<script type="text/javascript">
    $(document).ready(function() {
        $('#member').DataTable({
            "pageLength": 25
        });
    });
</script>
