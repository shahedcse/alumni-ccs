<div class="content-wrapper">
    <div class="account-page login text-center">
        <div class="container">
            <div class="account-title">
                <h4 class="heading-light">Set Your Verification Code .</h4>
            </div>
            <?php
            if ($this->session->userdata('add')):
                echo '<div class="info-boxes confirmation-message"><div class="info-box-icon"><i class="fa fa-check"></i></div><strong>Congrats !</strong><br>' . $this->session->userdata('add') . '<div class="close-button"></div></div>';
                $this->session->unset_userdata('add');
            elseif ($this->session->userdata('notadd')):
                echo '<div class="info-boxes error-message"><div class="info-box-icon"><i class="fa fa-times"></i></div><strong>Sorry !!</strong><br>' . $this->session->userdata('notadd') . '<div class="close-button"></div> </div>';
                $this->session->unset_userdata('notadd');
            endif;
            ?>
            <span id="verific" style="font-size: 25px;"></span>
            <div class="login-block2" >
                <form class="login-form" action="<?php echo base_url('Auth/check_verification'); ?>" method="post">
                    <div class="form-group">
                        <center> <p style="color:green;">(Please Wait , An OTP  is Send to your phone.please check  and set here)</p>
                        </center><br>
                        <label class="col-md-4 control-label">Insert OTP Code :<span style="color:red">*</span></label>
                        <div class="form-group">
                            <input type="text" name="verification_code" id="verification_code" required="" class="login-input" placeholder="OTP Code">
                        </div>

                        <div class="buttons-set">
                            <center>   <button  type="submit"  title="Submit" class="btn btn-success ">Submit</button></center>
                        </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script>
    function check_verification() {
        var code = $("#verification_code").val();
        if (code == '') {
            alert('Please set your Code');
            return;
        }
        var dataString = 'verification_code=' + code;
        $.ajax({
            type: "POST",
            url: "<?php echo base_url('Auth/check_verification'); ?>",
            data: dataString,
            success: function(data)
            {
                if (data == 'dontmass') {
                    $("#verific").text("Sorry !! Your verification code does't match.please try again");
                    $("#verific").css('color', 'red');
                }

                if (data == 'mass') {
                    // document.getElementById('update').disabled = true;
                    $("#verific").text("Congrats !! Your Registration is successfully complete.");
                    $("#verific").css('color', 'green');
                    $("#verification_code").val("");
                    //   window.location = base_url + 'Auth';


                }
            }
        });

    }
</script>



