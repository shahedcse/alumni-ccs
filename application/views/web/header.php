<!DOCTYPE html>
<head>
    <title><?= $page_title; ?></title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0">
    <!-- style -->
    <link rel="shortcut icon" href="<?= base_url(); ?>assets/img/favicon.png">
    <link rel="stylesheet" href="<?= base_url(); ?>assets/css/font-awesome.css">
    <link rel="stylesheet" href="<?= base_url(); ?>assets/fi/flaticon.css">
    <link rel="stylesheet" href="<?= base_url(); ?>assets/css/main.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets/tuner/css/colorpicker.css" />
    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets/tuner/css/styles.css" />
    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets/css/jquery.fancybox.css" />
    <link rel="stylesheet" href="<?= base_url(); ?>assets/css/owl.carousel.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets/rs-plugin/css/settings.css" media="screen">
    <link rel="stylesheet" href="<?= base_url(); ?>assets/css/animate.css">
    <script src="<?= base_url(); ?>assets/js/jquery.min.js"></script>
    <link rel="stylesheet" href="<?= base_url(); ?>assets/css/bootstrap.min.css">

    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <link href="//fonts.googleapis.com/css?family=Roboto+Condensed:300,300italic,regular,italic,700,700italic&subset=latin-ext,greek-ext,cyrillic-ext,greek,vietnamese,latin,cyrillic" rel="stylesheet" type="text/css" />
    <style>
        /*jssor slider loading skin spin css*/

        .jssorl-009-spin img {
            animation-name: jssorl-009-spin;
            animation-duration: 1.6s;
            animation-iteration-count: infinite;
            animation-timing-function: linear;
        }

        @keyframes jssorl-009-spin {
            from { transform: rotate(0deg); }
            to { transform: rotate(360deg); }
        }

        /*jssor slider bullet skin 032 css*/
        .jssorb032 {position:absolute;}
        .jssorb032 .i {position:absolute;cursor:pointer;}
        .jssorb032 .i .b {fill:#fff;fill-opacity:0.7;stroke:#000;stroke-width:1200;stroke-miterlimit:10;stroke-opacity:0.25;}
        .jssorb032 .i:hover .b {fill:#000;fill-opacity:.6;stroke:#fff;stroke-opacity:.35;}
        .jssorb032 .iav .b {fill:#000;fill-opacity:1;stroke:#fff;stroke-opacity:.35;}
        .jssorb032 .i.idn {opacity:.3;}

        /*jssor slider arrow skin 051 css*/
        .jssora051 {display:block;position:absolute;cursor:pointer;}
        .jssora051 .a {fill:none;stroke:#fff;stroke-width:360;stroke-miterlimit:10;}
        .jssora051:hover {opacity:.8;}
        .jssora051.jssora051dn {opacity:.5;}
        .jssora051.jssora051ds {opacity:.3;pointer-events:none;}

        .vticker ul{
            padding: 0;
        }
        .vticker li{
            list-style: none;
        }
        .vticker ul li::before {
            color: #fff;
            font-weight: bold;
            display: inline-block; 
            width: 1em;
            margin-left: -1em;
        }
        .et-run{
            background: red;
        }
    </style>
</head>

<header class="only-color">
    <div class="page-header-top" style="background-color:#AA0000 ">
        <div class="grid-row clear-fix">
            <address>
               	<a href="<?= base_url('Auth'); ?>" class="cws-button border-radius alt smaller" style="color:#fff">Login/Register</a>
                <a href="#" class="email"><i class="fa fa-envelope-o"></i>info@chattogramcollegiatealumni.com</a>
                <a href="#" class="email"><i class="fa fa-dashboard"></i><?php echo date('l , F j, Y'); ?></a>
            </address>
            <div class="header-top-panel">
                <?php if (!empty($this->session->userdata('user_id'))): ?>
                    <div class="dropdown" >
                        <button class="btn btn-primary dropdown-toggle" style="background-color: #AA0000;" type="button" data-toggle="dropdown"><b>Hello Dear !! <?= $this->session->userdata('user_name'); ?></b>
                            <span class="caret"></span></button>
                        <ul class="dropdown-menu">
                            <li><a href="<?= base_url('Auth/profile') ?>">My profile</a></li>
                            <li><a href="<?= base_url('Auth/logout') ?>">logout</a></li>
                        </ul>
                    </div>
                <?php endif; ?>
                <a href="http://facebook.com/" class="cws_social_link" title="Facebook"><i class="share-icon fa fa-facebook"></i></a>
                <a href="https://plus.google.com/" class="cws_social_link" title="Google +"><i class="share-icon fa fa-google-plus"></i></a>
                <a href="http://twitter.com/" class="cws_social_link" title="Twitter"><i class="share-icon fa fa-twitter"></i></a>
                <a href="http://dribbble.com/" class="cws_social_link" title="Dribbble"><i class="share-icon fa fa-dribbble"></i></a>

            </div>

        </div>
    </div>
    <!-- / header top panel -->
    <!-- sticky menu -->
    <div class="sticky-wrapper">
        <div class="sticky-menu">
            <div class="grid-row clear-fix">
                <a href="<?= base_url(); ?>" class="logo">
                    <img src="<?= base_url(); ?>assets/img/logonew.PNG"  data-at2x="img/logo@2x.png" alt>
                  <!--  <p style="color:black;font-size: 20px;font-weight: bold">Chattagram Collegiates<br>
                        চট্টগ্রাম কলেজিয়েটস</p>-->
                </a>
                <!-- / logo -->
                <nav class="main-nav">
                    <ul class="clear-fix">
                        <li>
                            <a href="<?= base_url(); ?>">HOME</a>
                        </li>

                        <li>
                            <a href="#">ABOUT</a>
                            <ul>
                                <li><a href="<?= base_url('About'); ?>">About our journey</a></li>
                                <li><a href="<?= base_url('About/rules'); ?>">Terms & Condition</a></li>
                                <li><a href="<?= base_url('About/Donation'); ?>">Donation</a></li>
                                <li><a href="<?= base_url('About/Support'); ?>">Support</a></li>
                                <li><a href="#">Download</a></li>
                            </ul>
                        </li>
                        <li>
                            <a href="#">MEMBER</a>
                            <ul>
                                <li><a href="<?= base_url('Member/executive_member'); ?>">Executive Committee</a></li>
                                <li><a href="<?= base_url('Member'); ?>">Alumni Member List</a></li>
                            </ul>
                        </li>
                        <li>
                            <a href="#">MEDIA</a>
                            <ul>
                                <li> <a href="<?= base_url('Blog'); ?>">Blog & News</a></li>
                                <li><a href="<?= base_url('Media/image_gallery'); ?>">Photo Gallery</a></li>
                                <li><a href="<?= base_url('Media/videos'); ?>">Video Gallery</a></li>
                            </ul>
                        </li>

                        <li>
                            <a href="<?= base_url('About/contact'); ?>">CONTACT</a>
                        </li>
                        <li>
                            <a href="#">ACCOUNT</a>
                            <ul>
                                <li><a href="<?= base_url('Auth'); ?>">Login</a></li>
                                <li><a href="<?= base_url('Auth/registration'); ?>">Register</a></li>
                            </ul>
                        </li>
                        <li>
                            <a  target="_blank"href="http://www.ctgcs.edu.bd/index.php">
                                <button type="button"  class="cws-button border-radius">SCHOOL OFFICIAL SITE</button>
                            </a>
                        </li>
                    </ul>
                </nav>
            </div>
        </div>
    </div>
    <!-- sticky menu -->
</header>
