<div class="page-title" style="background-image: url(<?= base_url(); ?>assets/pic/1920-1280-img-2.jpg)">
    <div class="grid-row">
        <h1>Image Gallery</h1>
        <nav class="bread-crumb">
            <a href="<?= base_url(); ?>">Home</a>
            <i class="fa fa-long-arrow-right"></i>
            <a href="#">Image Gallery</a>
        </nav>
    </div>
</div>
<div class="page-content grid-row">
    <main>
        <div class="isotope-container">
            <div class="isotope-header clear-fix">
                <h2 class="margin-none">Photo Album</h2>
            </div>
            <div class="grid-col-row">
                <div class="isotope" style="position: relative; overflow: hidden; height: 1011px;">
                    <?php
                    foreach ($all_album as $value):
                        $imgcount = $this->db->query("SELECT COUNT(id) AS totl FROM gallery_photo where gallery_id='$value->id'")->row()->totl;
                        ?>
                        <div class="item  isotope-item" >
                            <a href="<?= base_url('Media/album_details?id=' . $value->id); ?>">
                                <div class="picture">
                                    <center> <b> <?= $value->album_name; ?> (<?= $imgcount ?>)</b></center>
                                    <img src="<?= base_url('assets/img/albums.png'); ?>"  alt="">

                                </div>
                            </a>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
    </main>
</div>
