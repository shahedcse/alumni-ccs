<style>
    #meter
    {
        margin-top:5px;
        height:15px;
        //  border-radius:
    }
    #pass_type
    {
        font-size:15px;
        margin-top:10px;
        margin-left:15%;
        text-align:center;
        color:black;
    }
</style>
<main>
    <section class="fullwidth-background bg-2">
        <div class="grid-row">
            <div class="login-block2">
                <div class="logo">
                    <center>   <img src="<?= base_url(); ?>assets/img/logo.png" data-at2x="img/logo@2x.png" alt="">
                        <h2>Alumni CCS  Registration.</h2></center>
                </div>
                <form class="login-form" action="<?= base_url('Auth/insert_regidata'); ?>" enctype="multipart/form-data" method="post">
                    <div class="form-group">
                        <input type="text" onkeypress="allowNumbersOnly(event)" onchange="get_details();" name="lm_no" required="" id="lm_no" class="login-input" placeholder="L.M No">
                        <span id="verific" style="font-size: 10px;"></span>
                    </div>
                    <div class="form-group">
                        <input type="text" name="name" id="name" required="" class="login-input" placeholder="Enter Your name">
                    </div>
                    <div class="form-group">
                        <input type="text" name="phone" onkeypress="allowNumbersOnly(event)" maxlength="11" required=""id="phone" class="login-input" placeholder="Phone No.">
                    </div>
                    <div class="form-group">
                        <input type="text"  name="email" required=""id="email" class="login-input" placeholder="Email">
                    </div>
                    <div class="form-group">
                        <input type="text" name="adress" id="adress" required="" class="login-input" placeholder="Adress">
                    </div>
                    <div class="form-group">
                        <select  name="passing_year"id="passing_year" required=""  class="login-input" >
                            <option>-Passing Year-</option>
                            <?php
                            $years_now = date("Y");
                            foreach (range($years_now, 1950) as $years):
                                ?>
                                <option value="<?= $years; ?>"><?= $years ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <select  name="profession"id="profession" required=""  class="login-input" >
                            <option>- Present profession-</option>
                            <?php foreach ($profession as $value): ?>
                                <option value="<?= $value->p_name; ?>"><?= $value->p_name; ?></option>
                            <?php endforeach; ?>
                        </select>

                    </div>
                    <div class="form-group">
                        <select  name="blood_group"id="blood_group"required=""  class="login-input">
                            <option>-Blood Group-</option>
                            <?php foreach ($blood as $value): ?>
                                <option value="<?= $value->blood_group; ?>"><?= $value->blood_group; ?></option>
                            <?php endforeach; ?>
                        </select>

                    </div>
                    <div class="form-group">
                        <input type="text" required="" name="password"id="password"  class="login-input" placeholder="Password">
                        <div id="meter"></div>
                        <span id="pass_type"></span>

                    </div>


                    <div  class="login-input" style="margin-bottom: 15px;">
                        <p>Upload Profile images</p>
                        <input id="fileToUpload" name="fileToUpload" type="file" >
                        <div id="preview" ></div>
                    </div>

                    <center>
                        <button type="submit" id="submit" class="button-fullwidth cws-button bt-color-3 border-radius ">Submit</button>
                    </center>
                </form>
            </div>
        </div>
    </section>
</main>
<div class="modal fade" id="alert1"tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                Alert !!
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <h3 style="color:red;">Sorry !! Your L.M No. is not Enlisted.Please insert your correct L.M No. To get your correct L.M or registration problem please contact with authority.</h3>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>
<script>
    function get_details() {
        var lm_no = $("#lm_no").val();
        $.ajax({
            type: "POST",
            url: "<?php echo base_url('Auth/checklm'); ?>",
            data: 'lm_no=' + lm_no,
            success: function(data)
            {
                if (data == 'dontmass') {
                    $.ajax({
                        type: "POST",
                        url: "<?php echo base_url('Auth/getdetails'); ?>",
                        data: 'lm_no=' + lm_no,
                        success: function(data)
                        {
                            var outputData = JSON.parse(data);
                            var response = outputData.memberdata;
                            if (response == null) {
                                $("#name").val('');
                                $("#phone").val('');
                                $("#email").val('');
                                $("#passing_year").val('');
                                $("#profession").val('');
                                $("#blood_group").val('');
                                $("#alert1").modal('show');
                            }
                            else {
                                $("#name").val(response.name);
                                $("#phone").val(response.phone);
                              //  $("#phone").prop("readonly", true);
                                $("#email").val(response.email);
                                $("#adress").val(response.adress);
                                $("#passing_year").val(response.passing_year);
                                $("#profession").val(response.profession);
                                $("#blood_group").val(response.blood_group);
                            }
                        }
                    });

                }
                if (data == 'mass') {
                    document.getElementById("submit").disabled = true;
                    alert('You are already registered member');
                }
            }
        });

    }

    $(document).ready(function() {
        $("#password").keyup(function() {
            check_pass();
        });
    });

    function check_pass()
    {
        var val = document.getElementById("password").value;
        var meter = document.getElementById("meter");
        var no = 0;
        if (val != "")
        {
            // If the password length is less than or equal to 6
            if (val.length <= 6)
                no = 1;

            // If the password length is greater than 6 and contain any lowercase alphabet or any number or any special character
            if (val.length > 6 && (val.match(/[a-z]/) || val.match(/\d+/) || val.match(/.[!,@,#,$,%,^,&,*,?,_,~,-,(,)]/)))
                no = 2;

            // If the password length is greater than 6 and contain alphabet,number,special character respectively
            if (val.length > 6 && ((val.match(/[a-z]/) && val.match(/\d+/)) || (val.match(/\d+/) && val.match(/.[!,@,#,$,%,^,&,*,?,_,~,-,(,)]/)) || (val.match(/[a-z]/) && val.match(/.[!,@,#,$,%,^,&,*,?,_,~,-,(,)]/))))
                no = 3;

            // If the password length is greater than 6 and must contain alphabets,numbers and special characters
            if (val.length > 6 && val.match(/[a-z]/) && val.match(/\d+/) && val.match(/.[!,@,#,$,%,^,&,*,?,_,~,-,(,)]/))
                no = 4;

            if (no == 1)
            {
                $("#meter").animate({width: '50px'}, 300);
                meter.style.backgroundColor = "red";
                $("#submit").attr("disabled", true);
                document.getElementById("pass_type").innerHTML = "Very Weak";
            }

            if (no == 2)
            {
                $("#meter").animate({width: '100px'}, 300);
                meter.style.backgroundColor = "#F5BCA9";
                $("#submit").attr("disabled", true);
                document.getElementById("pass_type").innerHTML = "Weak";
            }

            if (no == 3)
            {
                $("#meter").animate({width: '150px'}, 300);
                meter.style.backgroundColor = "#FF8000";
                $("#submit").attr("disabled", false);
                document.getElementById("pass_type").innerHTML = "Good";
            }

            if (no == 4)
            {
                $("#meter").animate({width: '200px'}, 300);
                meter.style.backgroundColor = "#00FF40";
                $("#submit").attr("disabled", false);
                document.getElementById("pass_type").innerHTML = "Strong";
            }
        }

        else
        {
            meter.style.backgroundColor = "white";
            document.getElementById("pass_type").innerHTML = "";
        }
    }


    function checklength() {
        var password = $("#password").val();
        if (password.length < 4) {
            $("#submit").attr("disabled", true);
            alert('please insert a strong password');
        }
        else {
            $("#submit").attr("disabled", false);
        }
    }

    function allowNumbersOnly(e) {
        var code = (e.which) ? e.which : e.keyCode;
        if (code > 31 && (code < 48 || code > 57)) {
            e.preventDefault();
        }
    }



    function previewImages() {

        var $preview = $('#preview').empty();
        if (this.files)
            $.each(this.files, readAndPreview);

        function readAndPreview(i, file) {

            if (!/\.(jpe?g|png|gif)$/i.test(file.name)) {
                return alert(file.name + " is not an image");
            } // else...

            var reader = new FileReader();

            $(reader).on("load", function() {
                $preview.append($("<img/>", {src: this.result, height: 100}));
            });

            reader.readAsDataURL(file);

        }

    }

    $('#fileToUpload').on("change", previewImages);
</script>