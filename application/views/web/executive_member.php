<style>
    .tableFixHead          { overflow-y: auto; height: 550px; }
    .tableFixHead thead th { position: sticky; top: 0; }

    /* Just common table stuff. Really. */
    table  { border-collapse: collapse; width: 100%; }
    th, td { padding: 8px 16px; }
    th     { background:#AA0000;color:#fff; }
</style>
<div class="page-title" style="background-image: url(<?= base_url(); ?>assets/pic/1920-1280-img-2.jpg)">
    <div class="grid-row">
        <h1>Executive Member</h1>
        <nav class="bread-crumb">
            <a href="<?= base_url(); ?>">Home</a>
            <i class="fa fa-long-arrow-right"></i>
            <a href="#">Executive Member</a>

        </nav>
    </div>
</div>
<div class="page-content">
    <main>
        <div class="container">
            <section class="clear-fix">
                <h2>কার্যকরী পরিষদ :</h2>
                <div class="grid-col-row tableFixHead">
                    <table class="table table-bordered">
                        <thead>
                            <tr style="background-color:#AA0000;color:#fff; ">
                                <th scope="col" style="text-align: center;" >সিরিয়াল </th>
                                <th scope="col" style="text-align: center;">পদবী</th>
                                <th scope="col" style="text-align: center;"> নাম</th>
                            </tr>
                        </thead>
                        <tbody >
                            <tr>
                                <td  style="text-align: center;"scope="row">1</td>
                                <td  style="text-align: center;">সভাপতি</td>
                                <td  style="text-align: center;">এম. এ মালেক</td>
                            </tr>
                            <tr>
                                <td  style="text-align: center;"scope="row">2</td>
                                <td  style="text-align: center;">সিনিয়র-সভাপতি</td>
                                <td  style="text-align: center;">আমীর হুমায়ুন মাহমুদ চৌধুরী</td>
                            </tr>
                            <tr>
                                <td  style="text-align: center;"scope="row">3</td>
                                <td  style="text-align: center;">সহ-সভাপতি</td>
                                <td  style="text-align: center;">আলহাজ্ব নবী দোভাষ</td>
                            </tr>
                            <tr>
                                <td  style="text-align: center;"scope="row">4</td>
                                <td  style="text-align: center;">সহ-সভাপতি</td>
                                <td  style="text-align: center;">অধ্যাপক সেলিম জাহাঙ্গীর</td>
                            </tr>
                            <tr>
                                <td  style="text-align: center;"scope="row">5</td>
                                <td  style="text-align: center;">সহ-সভাপতি</td>
                                <td  style="text-align: center;">নুরুল আমিন খান</td>
                            </tr>
                            <tr>
                                <td  style="text-align: center;"scope="row">6</td>
                                <td  style="text-align: center;">সাধাপরণ সম্পাদক</td>
                                <td  style="text-align: center;">মোহাম্মদ মোস্তাক হোসাইন</td>
                            </tr>



                            <tr>
                                <td  style="text-align: center;"scope="row">7</td>
                                <td  style="text-align: center;">যুগ্ম সাধারণ সম্পাদক</td>
                                <td  style="text-align: center;">ফরিদুল আলম</td>
                            </tr>
                            <tr>
                                <td  style="text-align: center;"scope="row">8</td>
                                <td  style="text-align: center;">যুগ্ম সাধারণ সম্পাদক</td>
                                <td  style="text-align: center;">আ.ন.ম ওয়াহিদ দুলাল</td>
                            </tr>
                            <tr>
                                <td  style="text-align: center;"scope="row">9</td>
                                <td  style="text-align: center;">যুগ্ম সাধারণ সম্পাদক</td>
                                <td  style="text-align: center;">প্রকৌশলী কামরুল ইসলাম</td>
                            </tr>
                            <tr>
                                <td  style="text-align: center;"scope="row">10</td>
                                <td  style="text-align: center;">সাংগঠনিক সম্পাদক</td>
                                <td  style="text-align: center;">এ.এইচ.এম. আশরাফ উল আনোয়ার হিরন</td>
                            </tr>
                            <tr>
                                <td  style="text-align: center;"scope="row">11</td>
                                <td  style="text-align: center;">সহ-সাংগঠনিক সম্পাদক</td>
                                <td  style="text-align: center;">সুমন চক্রবর্তী</td>
                            </tr>
                            <tr>
                                <td  style="text-align: center;"scope="row">12</td>
                                <td  style="text-align: center;">কোষাধ্যক্ষ</td>
                                <td  style="text-align: center;">ফজলুল হক</td>
                            </tr>
                            <tr>
                               <td  style="text-align: center;"scope="row">13</td>
                                <td  style="text-align: center;">সহ-কোষাধ্যক্ষ</td>
                                <td  style="text-align: center;">আবু বক্কর ছিদ্দিক মামুন </td>
                            </tr>
                            <tr>
                                <td  style="text-align: center;"scope="row">14</td>
                                <td  style="text-align: center;">দপ্তর সম্পাদক</td>
                                <td  style="text-align: center;">গোলাম মহিউদ্দিন মূকুল</td>
                            </tr>
                            <tr>
                                <td  style="text-align: center;"scope="row">15</td>
                                <td  style="text-align: center;">সহ-দপ্তর সম্পাদক</td>
                                <td  style="text-align: center;">এস. এম. এহতেশামুল ইসলাম রিশ্তা</td>
                            </tr>
                            <tr>
                                <td  style="text-align: center;"scope="row">16</td>
                                <td  style="text-align: center;">প্রচার সম্পাদক</td>
                                <td  style="text-align: center;">আবুল হাসনাম মো: বেলাল</td>
                            </tr>
                            <tr>
                                <td  style="text-align: center;"scope="row">17</td>
                                <td  style="text-align: center;">সহ-প্রচার সম্পাদক</td>
                                <td  style="text-align: center;">মো: রায়হান সিদ্দিকী</td>
                            </tr>
                            <tr>
                                <td  style="text-align: center;"scope="row">18</td>
                                <td  style="text-align: center;">সাহিত্য ও প্রকাশনা সম্পাদক</td>
                                <td  style="text-align: center;">প্রফেসর ড. নুরুল আমিন</td>
                            </tr>
                            <tr>
                                <td  style="text-align: center;"scope="row">19</td>
                                <td  style="text-align: center;">সহ-সাহিত্য ও প্রকাশনা সম্পাদক</td>
                                <td  style="text-align: center;">রাজিউর রহমান বিতান</td>
                            </tr>
                            <tr>
                                <td  style="text-align: center;"scope="row">20</td>
                                <td  style="text-align: center;">ক্রীড়া সম্পাদক</td>
                                <td  style="text-align: center;">এরাদত উল্লাহ</td>
                            </tr>
                            <tr>
                               <td  style="text-align: center;"scope="row">21</td>
                                <td  style="text-align: center;">সাংস্কৃতিক সম্পাদক</td>
                                <td  style="text-align: center;">এহিউদ্দিন শাহ্ আলম নিপু </td>
                            </tr>

                            <tr>
                                <td  style="text-align: center;"scope="row">22</td>
                                <td  style="text-align: center;">সহ-সাংস্কৃতিক সম্পাদক</td>
                                <td  style="text-align: center;">বাহাউদ্দিন আহমেদ জুয়েল  </td>
                            </tr>
                            <tr>
                                <td  style="text-align: center;"scope="row">23</td>
                                <td  style="text-align: center;">আপ্যায়ন সম্পাদক</td>
                                <td  style="text-align: center;">মোহাম্মদ জাকারিয়া  </td>
                            </tr>
                            <tr>
                                <td  style="text-align: center;"scope="row">24</td>
                                <td  style="text-align: center;">সহ-আপ্যায়ন সম্পাদক</td>
                                <td  style="text-align: center;">মোহাম্মদ নজরুল ইসলাম </td>
                            </tr>
                            <tr>
                                <td  style="text-align: center;"scope="row">25</td>
                                <td  style="text-align: center;">তথ্য প্রযুক্তি সম্পাদক</td>
                                <td  style="text-align: center;">সাহেদুল কবির চৌধুরী </td>
                            </tr>
                            <tr>
                                <td  style="text-align: center;"scope="row">26</td>
                                <td  style="text-align: center;">সহ-তথ্য প্রযুক্তি সম্পাদক</td>
                                <td  style="text-align: center;">তুহি কান্তি শীল </td>
                            </tr>
                            <tr>
                                <td  style="text-align: center;"scope="row">27</td>
                                <td  style="text-align: center;">সদস্য</td>
                                <td  style="text-align: center;">প্রফেসর ডা. মোহাম্মদ আলী চৌধুরী</td>
                            </tr>
                            <tr>
                               <td  style="text-align: center;"scope="row">28</td>
                                <td  style="text-align: center;">সদস্য</td>
                                <td  style="text-align: center;">নুরুল হাসান বাবুল</td>
                            </tr>
                            <tr>
                                <td  style="text-align: center;"scope="row">29</td>
                                <td  style="text-align: center;">সদস্য</td>
                                <td  style="text-align: center;">লে: কর্ণেল (অব:) জয়নুর রশীদ</td>
                            </tr>
                            <tr>
                                <td  style="text-align: center;"scope="row">30</td>
                                <td  style="text-align: center;">সদস্য</td>
                                <td  style="text-align: center;">বশিরুল ইসলাম চৌধুরী</td>
                            </tr>

                            <tr>
                                <td  style="text-align: center;"scope="row">31</td>
                                <td  style="text-align: center;">সদস্য</td>
                                <td  style="text-align: center;">জাহাঙ্গীর মিঞা</td>
                            </tr>
                            <tr>
                                <td  style="text-align: center;"scope="row">32</td>
                                <td  style="text-align: center;">সদস্য</td>
                                <td  style="text-align: center;">শামসুদ্দিন চৌধুরী</td>
                            </tr>
                            <tr>
                               <td  style="text-align: center;"scope="row">33</td>
                                <td  style="text-align: center;">সদস্য</td>
                                <td  style="text-align: center;">সমর বড়ুয়া</td>
                            </tr>

                            <tr>
                                <td  style="text-align: center;"scope="row">34</td>
                                <td  style="text-align: center;">সদস্য</td>
                                <td  style="text-align: center;">আশীষ বড়ুয়া</td>
                            </tr>
                            <tr>
                                <td  style="text-align: center;"scope="row">35</td>
                                <td  style="text-align: center;">সদস্য</td>
                                <td  style="text-align: center;">ঘাসান আনোয়ারুল কবির সাজু</td>
                            </tr>
                            <tr>
                                <td  style="text-align: center;"scope="row">36</td>
                                <td  style="text-align: center;">সদস্য</td>
                                <td  style="text-align: center;">ডা: মোহাম্মদ পারভেজ ইকবাল শরীফ</td>
                            </tr>

                            <tr>
                                <td  style="text-align: center;"scope="row">37</td>
                                <td  style="text-align: center;">সদস্য</td>
                                <td  style="text-align: center;">ইমাম হোসেন মিল্লাত</td>
                            </tr>
                            <tr>
                                <td  style="text-align: center;"scope="row">38</td>
                                <td  style="text-align: center;">সদস্য</td>
                                <td  style="text-align: center;">গোফরান উদ্দিন টিটু</td>
                            </tr>
                            <tr>
                                <td  style="text-align: center;"scope="row">1</td>
                                <td  style="text-align: center;">সদস্য</td>
                                <td  style="text-align: center;">মফিজ আহমেদ জাহেদ</td>
                            </tr>
                            <tr>
                                <td  style="text-align: center;"scope="row">39</td>
                                <td  style="text-align: center;">সদস্য</td>
                                <td  style="text-align: center;">মো: জিল্লুর রহমান পাটোয়ারী</td>
                            </tr>
                            <tr>
                                <td  style="text-align: center;"scope="row">40</td>
                                <td  style="text-align: center;">সদস্য</td>
                                <td  style="text-align: center;">মোহাম্মদ জোবায়েদ</td>
                            </tr>
                            <tr>
                                <td  style="text-align: center;"scope="row">1</td>
                                <td  style="text-align: center;">সদস্য</td>
                                <td  style="text-align: center;">মো: মিজানুর রহমান </td>
                            </tr>
                            <tr>
                                <td  style="text-align: center;"scope="row">41</td>
                                <td  style="text-align: center;">সদস্য</td>
                                <td  style="text-align: center;">দেবাশীষ চক্রবর্তী</td>
                            </tr>
                            <tr>
                                <td  style="text-align: center;"scope="row">42</td>
                                <td  style="text-align: center;">সদস্য</td>
                                <td  style="text-align: center;">দ্বৈপায়ন পাল </td>
                            </tr>

                        </tbody>
                    </table>
                </div>
            </section>

        </div>
    </main>
</div>
