<!doctype html>
<html class="no-js" lang="en">
    <body class="materialdesign">
        <div class="wrapper-pro">
            <div class="content-inner-all">

                <div class="header-top-area"></div>
                <!-- Breadcome start-->
                <div class="breadcome-area mg-b-30 small-dn">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="breadcome-list map-mg-t-40-gl shadow-reset">
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <ul class="breadcome-menu">
                                                <li><span class="bread-blod"> All Support Messages .</span>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="income-order-visit-user-area">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="data-table-area mg-b-15">
                                <div class="container-fluid">
                                    <?php
                                    if ($this->session->userdata('add')):
                                        echo '<div class="alert alert-success fade in"><button data-dismiss="alert" class="close close-sm" type="button"><i class="fa fa-times"></i></button><strong>Success Message !!! </strong> ' . $this->session->userdata('add') . '</div>';
                                        $this->session->unset_userdata('add');
                                    endif;
                                    if ($this->session->userdata('notadd')):
                                        echo '<div class="alert alert-block alert-danger fade in"><button data-dismiss="alert" class="close close-sm" type="button"><i class="fa fa-times"></i></button><strong>Failed Meaasge !!! </strong> ' . $this->session->userdata('notadd') . '</div>';
                                        $this->session->unset_userdata('notadd');
                                    endif;
                                    ?>
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="sparkline13-list shadow-reset">
                                                <div class="sparkline13-hd">
                                                    <div class="main-sparkline13-hd">
                                                        <div class="sparkline13-outline-icon">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="sparkline13-graph">
                                                    <div class="datatable-dashv1-list custom-datatable-overright">
                                                        <div id="toolbar">
                                                            <select class="form-control">
                                                                <option value="">Export Basic</option>
                                                                <option value="all">Export All</option>
                                                                <option value="selected">Export Selected</option>
                                                            </select>

                                                        </div>
                                                        <table id="table" data-toggle="table" data-pagination="true" data-search="true" data-show-columns="true" data-show-pagination-switch="true" data-show-refresh="true" data-key-events="true" data-show-toggle="true" data-resizable="true" data-cookie="true" data-cookie-id-table="saveId" data-show-export="true" data-click-to-select="true" data-toolbar="#toolbar">
                                                            <thead>
                                                                <tr>
                                                                    <th data-field="state" data-checkbox="true"></th>
                                                                    <th data-field="id" >SI.</th>
                                                                    <th data-field="Name" >Msg. FROM</th>
                                                                    <th data-field="Phone" >Phone</th>
                                                                    <th data-field="Passing">Date</th>
                                                                    <th data-field="Profession" >Options</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <?php
                                                                $id = 0;
                                                                foreach ($all_msg as $value):
                                                                    $id++;
                                                                    ?>
                                                                    <tr>
                                                                        <td></td>
                                                                        <td><?= $id ?></td>
                                                                        <td><?= $value->name; ?></td>
                                                                        <td><?= $value->phone; ?></td>
                                                                        <td><?= $value->date; ?></td>
                                                                        <td>
                                                                            <?php if ($value->status == 0): ?>
                                                                                <a href="#" onclick="view_details('<?= $value->id; ?>');">
                                                                                    <button  type="button" class="btn-success">Not Checked</button>
                                                                                </a>
                                                                            <?php else: ?>
                                                                                <a href="#" onclick="view_details('<?= $value->id; ?>');">
                                                                                    <button type="button"class="btn-default">Checked</button><br>
                                                                                    <?php
                                                                                    if (!empty($value->replied_by)):
                                                                                        $repliedby = $this->db->query("SELECT name FROM users where id='$value->replied_by'")->row()->name;
                                                                                        ?>
                                                                                        <p style="color: green;">Replied by <?= $repliedby; ?></p>
                                                                                    <?php else: ?>
                                                                                        <p style="color: red;">Not Replied.</p>
                                                                                    <?php endif; ?>
                                                                                </a>
                                                                            <?php endif; ?>
                                                                        </td>
                                                                    </tr>
                                                                <?php endforeach; ?>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- income order visit user End -->
            </div>
        </div>

    </body>

</html>
<div id="message_modal" class="modal fade " role="dialog" style="display: hidden;">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-header" style="background-color:honeydew;">
                <b>Message Details.</b>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body" style="margin-bottom: 30px;">
                <p  id="details" style="font-size: 25px;"></p><br><br>
                <button type="button" id="replybutton" class="btn btn-block">Reply</button><br><br>

                <form action="<?= base_url('Sadmin/messages_reply'); ?>" method="post" id="smsbox" style="display: none;" >
                    <input type="hidden" id="smsid" name="smsid" class="form-control">
                    <textarea name="smsreply" style="background-color: #d3f5ea;margin-bottom: 5px;  " id="smsreply" class="form-control"  >Write Here</textarea>
                    <button type="submit" class="btn btn-info pull-right">Send SMS</button>
                </form>

            </div>
            <div class="modal-footer">
                <a href="<?= base_url('Sadmin/messages'); ?>">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </a>
            </div>
        </div>
    </div>
</div>
<script>

    $('#replybutton').click(function() {
        $('#smsbox').show();
    });

    $('.btn-success').click(function() {
        var $this = $(this);
        $this.toggleClass('btn-success');
        if ($this.hasClass('btn-success')) {
            $this.text('Not Checked');
        } else {
            $this.text('Checked');
        }
    });
    function view_details(id) {
        $('#smsid').val(id);
        $.ajax({
            type: "POST",
            url: "<?php echo base_url('Sadmin/getDetailsData'); ?>",
            data: 'id=' + id,
            success: function(data) {
                var outputData = JSON.parse(data);
                var response = outputData.detailsdata;
                $("#details").text(response.message);

                $('#message_modal').modal('show');
            }
        });

    }
</script>
