<!doctype html>
<html class="no-js" lang="en">
    <body class="materialdesign">
        <div class="wrapper-pro">
            <div class="content-inner-all">
                <div class="breadcome-area mg-b-30 small-dn">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="breadcome-list shadow-reset" style="margin-top: 80px;">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <center><h4>Welcome to chattagram collegiate school alumni. </h4></center>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="user-prfile-activity-area mg-b-40 mg-t-30">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-lg-3">
                                <div class="user-profile-about shadow-reset">
                                    <div class="user-profile-img-post">
                                        <a href="#"><img src="<?= base_url('assets/img/profile/') . $allinfo->image_path ?>" alt="">
                                        </a>
                                    </div>
                                    <center>
                                        <div class="user-profile-post-name">
                                            <h2><a href="#"><?= $allinfo->name; ?></a></h2>
                                            <p><span>Profession:<?= $allinfo->profession; ?></span>
                                            </p>
                                        </div>
                                        <div class="profile-userbuttons">
                                            <a href="<?= base_url('Auth/profile'); ?>">
                                                <button type="button" class="btn btn-success btn-sm">My profile</button>
                                            </a>
                                        </div>

                                        <span><i class="fa fa-circle user-profile-online"></i> Active Now</span>
                                    </center>
                                </div>

                            </div>
                            <div class="col-lg-7">
                                <div class="post-user-profile-awrap shadow-reset">
                                    <div class="user-profile-post">
                                        <div class="row">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <form action="<?= base_url('Auth/update_profiledata'); ?>" method="post" enctype="multipart/form-data">
                                                    <div class="profile-user-post-content">
                                                        <center>  <h2><a href="#">Update Profile Information.</a></h2></center>
                                                        <div class="row">
                                                            <div class="col-lg-5">
                                                                <div class="login-input-head">
                                                                    <p>Full Name :</p>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-7">
                                                                <div class="login-input-area">
                                                                    <input type="text" name="name" id="name" value="<?= $allinfo->name; ?>">
                                                                    <i class="fa fa-user login-user"></i>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-lg-5">
                                                                <div class="login-input-head">
                                                                    <p>Gender :</p>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-7">
                                                                <div class="login-input-area">
                                                                    <select class="form-control custom-select-value" name="gender" id="gender">
                                                                        <option value="">--Select--</option>
                                                                        <option value="Male"<?php if ($allinfo->gender == 'Male'): ?>selected=""<?php endif; ?>>Male</option>
                                                                        <option value="Female"<?php if ($allinfo->gender == 'Female'): ?>selected=""<?php endif; ?>>Female</option>
                                                                        <option value="Others"<?php if ($allinfo->gender == 'Others'): ?>selected=""<?php endif; ?>>Others</option>
                                                                    </select>

                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-lg-5">
                                                                <div class="login-input-head">
                                                                    <p>Phone No :</p>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-7">
                                                                <div class="login-input-area">
                                                                    <input type="text" name="phone" id="phoneg" value="<?= $allinfo->phone; ?>">
                                                                    <i class="fa fa-user login-user"></i>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-lg-5">
                                                                <div class="login-input-head">
                                                                    <p>Email :</p>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-7">
                                                                <div class="login-input-area">
                                                                    <input type="text"name="email" id="email" value="<?= $allinfo->email; ?>">
                                                                    <i class="fa fa-user login-user"></i>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-lg-5">
                                                                <div class="login-input-head">
                                                                    <p>Passing Year :</p>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-7">
                                                                <div class="login-input-area">
                                                                    <input type="text"name="passing_year" id="passing_year" value="<?= $allinfo->passing_year; ?>">
                                                                    <i class="fa fa-user login-user"></i>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-lg-5">
                                                                <div class="login-input-head">
                                                                    <p>Profession :</p>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-7">
                                                                <div class="login-input-area">
                                                                    <select class="form-control custom-select-value" name="profession" id="profession">
                                                                        <option value="">--Select--</option>
                                                                        <?php
                                                                        foreach ($profession as $value):
                                                                            if ($allinfo->profession == $value->p_name):
                                                                                ?>
                                                                                <option value="<?= $value->p_name; ?>" selected=""><?= $value->p_name; ?></option>
                                                                            <?php else: ?>
                                                                                <option value="<?= $value->p_name; ?>"><?= $value->p_name; ?></option>
                                                                            <?php
                                                                            endif;
                                                                        endforeach;
                                                                        ?>
                                                                    </select>

                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-lg-5">
                                                                <div class="login-input-head">
                                                                    <p>Field :</p>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-7">
                                                                <div class="login-input-area">
                                                                    <select class="form-control custom-select-value" name="profession_field" id="profession_field">
                                                                        <option value="">--Select--</option>
                                                                        <?php
                                                                        foreach ($profession_field as $value):
                                                                            if ($allinfo->profession_field == $value->field_name):
                                                                                ?>
                                                                                <option value="<?= $value->field_name; ?>" selected=""><?= $value->field_name; ?></option>
                                                                            <?php else: ?>
                                                                                <option value="<?= $value->field_name; ?>"><?= $value->field_name; ?></option>
                                                                            <?php
                                                                            endif;
                                                                        endforeach;
                                                                        ?>
                                                                        <i class="fa fa-user login-user"></i>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-lg-5">
                                                                <div class="login-input-head">
                                                                    <p>Specialization :</p>
                                                                </div>
                                                            </div>

                                                            <div class="col-lg-7">
                                                                <div class="login-input-area">
                                                                    <input type="text"name="specialization" id="specialization" value="<?= $allinfo->specialization; ?>">
                                                                    <i class="fa fa-user login-user"></i>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-lg-5">
                                                                <div class="login-input-head">
                                                                    <p>Present Loc: :</p>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-7">
                                                                <div class="login-input-area">
                                                                    <select class="form-control custom-select-value" name="present_loc" id="present_loc">
                                                                        <option value="">--Select--</option>
                                                                        <?php
                                                                        foreach ($districtall as $value):
                                                                            if ($allinfo->present_loc == $value->districtname):
                                                                                ?>
                                                                                <option value="<?= $value->districtname; ?>"selected><?= $value->districtname; ?></option>
                                                                            <?php else: ?>
                                                                                <option value="<?= $value->districtname; ?>"><?= $value->districtname; ?></option>

                                                                            <?php
                                                                            endif;
                                                                        endforeach;
                                                                        ?>
                                                                        <i class="fa fa-user login-user"></i>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-lg-5">
                                                                <div class="login-input-head">
                                                                    <p>Present Address: :</p>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-7">
                                                                <div class="login-input-area">
                                                                    <input type="text" name="present_address" id="present_address" value="<?= $allinfo->present_address; ?>">
                                                                    <i class="fa fa-user login-user"></i>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-lg-5">
                                                                <div class="login-input-head">
                                                                    <p>Blood Group :</p>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-7">
                                                                <div class="login-input-area">
                                                                    <select class="form-control custom-select-value" name="blood_group" id="blood_group">
                                                                        <option value="">--Select--</option>
                                                                        <?php
                                                                        foreach ($blood as $value):
                                                                            if ($allinfo->blood_group == $value->blood_group):
                                                                                ?>
                                                                                <option value="<?= $value->blood_group; ?>"selected=""><?= $value->blood_group; ?></option>
                                                                            <?php else: ?>
                                                                                <option value="<?= $value->blood_group; ?>"><?= $value->blood_group; ?></option>
                                                                            <?php
                                                                            endif;
                                                                        endforeach;
                                                                        ?>
                                                                        <i class="fa fa-user login-user"></i>
                                                                    </select>
                                                                </div>
                                                            </div>

                                                        </div>
                                                        <div class="row">
                                                            <div class="col-lg-5">
                                                                <div class="login-input-head">
                                                                    <p>Facebook profile :</p>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-7">
                                                                <div class="login-input-area">
                                                                    <input type="text" name="fblink" id="fblink" value="<?= $allinfo->fblink; ?>">
                                                                    <i class="fa fa-user login-user"></i>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="row">
                                                            <div class="col-lg-5">
                                                                <div class="login-input-head">
                                                                    <p>Profile Img:</p>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-7">
                                                                <div class="login-input-area">
                                                                    <input type="file" name="fileToUpload" id="fileToUpload" >
                                                                    <i class="fa fa-user login-user"></i>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="login-button-pro pull-right">
                                                        <a href="<?= base_url('Auth/profile'); ?>">
                                                            <button type="button" class="login-button login-button-rg">Back</button>
                                                        </a>
                                                        <button type="submit" class="login-button login-button-lg">Update</button>

                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>

    </body>

</html>

