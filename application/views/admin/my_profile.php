<!doctype html>
<html class="no-js" lang="en">
    <body class="materialdesign">
        <div class="wrapper-pro">
            <div class="content-inner-all">
                <div class="breadcome-area mg-b-30 small-dn">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="breadcome-list shadow-reset" style="margin-top: 80px;">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <center><h4>Welcome to chattagram collegiate school alumni. </h4></center>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="user-prfile-activity-area mg-b-40 mg-t-30">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-lg-3">
                                <div class="user-profile-about shadow-reset">
                                    <div class="user-profile-img-post">
                                        <a href="#"><img src="<?= base_url('assets/img/profile/') . $allinfo->image_path ?>" alt="">
                                        </a>
                                    </div>
                                    <center>
                                        <div class="user-profile-post-name">
                                            <h2><a href="#"><?= $allinfo->name; ?></a></h2>
                                            <p><span>Profession:<?= $allinfo->profession; ?></span>
                                            </p>
                                        </div>
                                        <div class="profile-userbuttons">
                                            <a href="<?= base_url('Auth/update_profile'); ?>">
                                                <button type="button" class="btn btn-success btn-sm">Update profile</button>
                                            </a>
                                        </div>

                                        <span><i class="fa fa-circle user-profile-online"></i> Active Now</span>
                                    </center>
                                </div>

                            </div>
                            <div class="col-lg-9">
                                <div class="post-user-profile-awrap shadow-reset">
                                    <div class="user-profile-post">
                                        <div class="row">
                                            <div class="col-lg-10 col-md-12 col-sm-12 col-xs-12">
                                                <div class="profile-user-post-content">
                                                    <?php
                                                    if ($this->session->userdata('add')):
                                                        echo '<div class="alert alert-success fade in"><button data-dismiss="alert" class="close close-sm" type="button"><i class="fa fa-times"></i></button><strong>Success Message !!! </strong> ' . $this->session->userdata('add') . '</div>';
                                                        $this->session->unset_userdata('add');
                                                    endif;
                                                    if ($this->session->userdata('notadd')):
                                                        echo '<div class="alert alert-block alert-danger fade in"><button data-dismiss="alert" class="close close-sm" type="button"><i class="fa fa-times"></i></button><strong>Failed Meaasge !!! </strong> ' . $this->session->userdata('notadd') . '</div>';
                                                        $this->session->unset_userdata('notadd');
                                                    endif;
                                                    ?>


                                                    <center>  <h2><a href="#">Profile Information.</a></h2></center>
                                                    <div class="static-table-list">
                                                        <table class="table hover-table">
                                                            <tbody>
                                                                <tr>
                                                                    <td>L.M NO.</td>
                                                                    <td> <?= $allinfo->lm_no; ?></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Full Name</td>
                                                                    <td><?= $allinfo->name; ?></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Phone No </td>
                                                                    <td><?= $allinfo->phone; ?></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Email</td>
                                                                    <td><?= $allinfo->email; ?></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Passing Year :</td>
                                                                    <td><?= $allinfo->passing_year; ?></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Profession </td>
                                                                    <td><?= $allinfo->profession; ?></td>
                                                                </tr>

                                                                <tr>
                                                                    <td>Field :</td>
                                                                    <td><?= $allinfo->profession_field; ?></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Specialization :</td>
                                                                    <td><?= $allinfo->specialization; ?></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Present Loc : </td>
                                                                    <td><?= $allinfo->present_loc; ?></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Present Address : </td>
                                                                    <td><?= $allinfo->present_address; ?></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Blood Group : </td>
                                                                    <td><?= $allinfo->blood_group; ?></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Facebook profile : </td>
                                                                    <td><?php if (!empty($allinfo->fblink)): ?>
                                                                            <a  target="_blank" href="<?= $allinfo->fblink; ?>" class="button btn-social basic-ele-mg-b-10 facebook span-left"> <span><i class="fa fa-facebook"></i></span> Facebook </a>
                                                                                <?php else: ?>
                                                                            N/A
                                                                        <?php endif; ?> </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>

    </body>

</html>

