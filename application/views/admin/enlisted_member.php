<!doctype html>
<html class="no-js" lang="en">
    <body class="materialdesign">
        <div class="wrapper-pro">
            <div class="content-inner-all">

                <div class="header-top-area"></div>
                <!-- Breadcome start-->
                <div class="breadcome-area mg-b-30 small-dn">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="breadcome-list map-mg-t-40-gl shadow-reset">
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <ul class="breadcome-menu">
                                                <li><span class="bread-blod"> All Enlisted Member .</span>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="income-order-visit-user-area">
                    <div class="container-fluid">
                         <?php
                        if ($this->session->userdata('add')):
                            echo '<div class="alert alert-success fade in"><button data-dismiss="alert" class="close close-sm" type="button"><i class="fa fa-times"></i></button><strong>Success Message !!! </strong> ' . $this->session->userdata('add') . '</div>';
                            $this->session->unset_userdata('add');
                        endif;
                        if ($this->session->userdata('notadd')):
                            echo '<div class="alert alert-block alert-danger fade in"><button data-dismiss="alert" class="close close-sm" type="button"><i class="fa fa-times"></i></button><strong>Failed Meaasge !!! </strong> ' . $this->session->userdata('notadd') . '</div>';
                            $this->session->unset_userdata('notadd');
                        endif;
                        ?>
                        <div class="row">
                            <div class="data-table-area mg-b-15">
                                <div class="container-fluid">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="sparkline13-list shadow-reset">
                                                <div class="sparkline13-hd">
                                                    <div class="main-sparkline13-hd">

                                                        <button type="button" data-toggle="modal" data-target="#addmembermodal" class="btn btn-custon-four btn-success">Add New </button>

                                                        <div class="sparkline13-outline-icon">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="sparkline13-graph">
                                                    <div class="datatable-dashv1-list custom-datatable-overright">
                                                        <div id="toolbar">
                                                            <select class="form-control">
                                                                <option value="">Export Basic</option>
                                                                <option value="all">Export All</option>
                                                                <option value="selected">Export Selected</option>
                                                            </select>

                                                        </div>
                                                        <table id="table" data-toggle="table" data-pagination="true" data-search="true" data-show-columns="true" data-show-pagination-switch="true" data-show-refresh="true" data-key-events="true" data-show-toggle="true" data-resizable="true" data-cookie="true" data-cookie-id-table="saveId" data-show-export="true" data-click-to-select="true" data-toolbar="#toolbar">
                                                            <thead>
                                                                <tr>
                                                                    <th data-field="state" data-checkbox="true"></th>
                                                                    <th data-field="id" >LM NO.</th>
                                                                    <th data-field="Name" >Name</th>
                                                                    <th data-field="Address" >Address</th>
                                                                    <th data-field="Phone" >Phone</th>
                                                                    <th data-field="Passing">Passing Year</th>
                                                                    <th data-field="Profession" >Profession</th>
                                                                    <th data-field="Email" >Email</th>
                                                                    <th data-field="Blood">Blood group</th>
                                                                    <th data-field="action">Action</th>

                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <?php foreach ($allmember as $value): ?>
                                                                    <tr>
                                                                        <td></td>
                                                                        <td><?= 'LM- ' . $value->lm_no; ?></td>
                                                                        <td><?= $value->name; ?></td>
                                                                        <td><?= $value->adress; ?></td>
                                                                        <td><?= $value->phone; ?></td>
                                                                        <td><?= $value->passing_year; ?></td>
                                                                        <td><?= $value->profession; ?></td>
                                                                        <td><?= $value->email; ?></td>
                                                                        <td><?= $value->blood_group; ?></td>
                                                                        <td>
                                                                            <select class="form-control">
                                                                                <option value="">Action</option>
                                                                                <option  Onclick="showeditModal();"value="">Edit</option>
                                                                                <option  Onclick="showdeleteModal();"value="">Delete</option>
                                                                            </select>
                                                                        </td>
                                                                    </tr>
                                                                <?php endforeach; ?>

                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- income order visit user End -->


            </div>
        </div>

    </body>

</html>
<div id="AlertModalalert" class="modal modal-adminpro-general FullColor-popup-AlertModal fade bounceInUp animated in" role="dialog" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-close-area modal-close-df">
                <a class="close" data-dismiss="modal" href="#"><i class="fa fa-close"></i></a>
            </div>
            <div class="modal-body">
                <span class="adminpro-icon adminpro-warning modal-check-pro information-icon-pro"></span>
                <h2>Alert!</h2>
                <p style="font-size: 20px;color: red;">Are You Sure Want To Delete This User ?</p>
            </div>
            <div class="modal-footer" style="padding-right: 40px;">

                <button type="button" data-dismiss="modal" class="btn btn-custon-four btn-default">No</button>

                <button type="button" class="btn btn-custon-four btn-success">Yes</button>

            </div>
        </div>
    </div>
</div>
<div id="addmembermodal" class="modal modal-adminpro-general fullwidth-popup-InformationproModal fade bounceInDown animated in" role="dialog" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header header-color-modal bg-color-2">
                <h4 class="modal-title">Add Member Information .</h4>
                <div class="modal-close-area modal-close-df">
                    <a class="close" data-dismiss="modal" href="#"><i class="fa fa-close"></i></a>
                </div>
            </div>
            <form action="<?= base_url('Member_panel/insert_enlist');  ?>" method="POST">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-5">
                            <div class="login-input-head">
                                <p>LM No :<span style="color: red">*</span></p>
                            </div>
                        </div>
                        <div class="col-lg-7">
                            <div class="login-input-area">
                                <input type="text" name="lm_no" id="lm_no">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-5">
                            <div class="login-input-head">
                                <p>Full Name :<span style="color: red">*</span></p>
                            </div>
                        </div>
                        <div class="col-lg-7">
                            <div class="login-input-area">
                                <input type="text" name="name" id="name">
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-5">
                            <div class="login-input-head">
                                <p>Phone No. :<span style="color: red">*</span></p>
                            </div>
                        </div>
                        <div class="col-lg-7">
                            <div class="login-input-area">
                                <input type="text" name="phone" id="phone2">

                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-5">
                            <div class="login-input-head">
                                <p>Passing Year. :<span style="color: red">*</span></p>
                            </div>
                        </div>
                        <div class="col-lg-7">
                            <div class="interested-input-area">
                                <select name="passing_year" required=""id="passing_year" >
                                    <option>-Passing Year-</option>
                                    <?php
                                    $years_now = date("Y");
                                    foreach (range($years_now, 1950) as $years):
                                        ?>
                                        <option value="<?= $years; ?>"><?= $years ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-5">
                            <div class="login-input-head">
                                <p>Profession. :<span style="color: red">*</span></p>
                            </div>
                        </div>
                        <div class="col-lg-7">
                            <div class="interested-input-area">
                                <select name="profession" required=""id="profession" >
                                    <option value="" >-Select-</option>
                                    <?php foreach ($profession as $value): ?>
                                        <option value="<?= $value->p_name; ?>"><?= $value->p_name; ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-5">
                            <div class="login-input-head">
                                <p>Email. :<span style="color: red">*</span></p>
                            </div>
                        </div>
                        <div class="col-lg-7">
                            <div class="login-input-area">
                                <input type="text" name="email" id="email">

                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-5">
                            <div class="login-input-head">
                                <p>Blood Group :<span style="color: red">*</span></p>
                            </div>
                        </div>
                        <div class="col-lg-7">
                            <div class="interested-input-area">
                                <select name="blood_group" required=""id="blood_group" >
                                    <option value="" >-Select-</option>
                                    <?php foreach ($blood as $value): ?>
                                        <option value="<?= $value->blood_group; ?>"><?= $value->blood_group; ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-5">
                            <div class="login-input-head">
                                <p>Address :<span style="color: red">*</span></p>
                            </div>
                        </div>
                        <div class="col-lg-7">
                            <div class="login-textarea-area">
                                <textarea class="contact-message" name="adress" id="adress" cols="30" rows="10"></textarea>
                                <i class="fa fa-comment login-user"></i>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" data-dismiss="modal" class="btn btn-custon-four btn-default">Cancel</button>

                    <button type="submit" class="btn btn-custon-four btn-success">Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div id="editmembermodal" class="modal modal-adminpro-general fullwidth-popup-InformationproModal fade bounceInDown animated in" role="dialog" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header header-color-modal bg-color-2">
                <h4 class="modal-title">Edit Member Information .</h4>
                <div class="modal-close-area modal-close-df">
                    <a class="close" data-dismiss="modal" href="#"><i class="fa fa-close"></i></a>
                </div>
            </div>
            <form action="#" method="POST">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-5">
                            <div class="login-input-head">
                                <p>LM No :<span style="color: red">*</span></p>
                            </div>
                        </div>
                        <div class="col-lg-7">
                            <div class="login-input-area">
                                <input type="text" name="lm_no" id="lm_no">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-5">
                            <div class="login-input-head">
                                <p>Full Name :<span style="color: red">*</span></p>
                            </div>
                        </div>
                        <div class="col-lg-7">
                            <div class="login-input-area">
                                <input type="text" name="name" id="name">
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-5">
                            <div class="login-input-head">
                                <p>Phone No. :<span style="color: red">*</span></p>
                            </div>
                        </div>
                        <div class="col-lg-7">
                            <div class="login-input-area">
                                <input type="text" name="phone" id="phone">

                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-5">
                            <div class="login-input-head">
                                <p>Passing Year. :<span style="color: red">*</span></p>
                            </div>
                        </div>
                        <div class="col-lg-7">
                            <div class="interested-input-area">
                                <select name="passing_year" required=""id="passing_year" >
                                    <option>-Passing Year-</option>
                                    <?php
                                    $years_now = date("Y");
                                    foreach (range($years_now, 1950) as $years):
                                        ?>
                                        <option value="<?= $years; ?>"><?= $years ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-5">
                            <div class="login-input-head">
                                <p>Profession. :<span style="color: red">*</span></p>
                            </div>
                        </div>
                        <div class="col-lg-7">
                            <div class="interested-input-area">
                                <select name="profession" required=""id="profession" >
                                    <option value="" >-Select-</option>
                                    <?php foreach ($profession as $value): ?>
                                        <option value="<?= $value->p_name; ?>"><?= $value->p_name; ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-5">
                            <div class="login-input-head">
                                <p>Email. :<span style="color: red">*</span></p>
                            </div>
                        </div>
                        <div class="col-lg-7">
                            <div class="login-input-area">
                                <input type="text" name="email" id="email">

                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-5">
                            <div class="login-input-head">
                                <p>Blood Group :<span style="color: red">*</span></p>
                            </div>
                        </div>
                        <div class="col-lg-7">
                            <div class="interested-input-area">
                                <select name="blood_group" required=""id="blood_group" >
                                    <option value="" >-Select-</option>
                                    <?php foreach ($blood as $value): ?>
                                        <option value="<?= $value->blood_group; ?>"><?= $value->blood_group; ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-5">
                            <div class="login-input-head">
                                <p>Address :<span style="color: red">*</span></p>
                            </div>
                        </div>
                        <div class="col-lg-7">
                            <div class="login-textarea-area">
                                <textarea class="contact-message" name="adress" id="adress" cols="30" rows="10"></textarea>
                                <i class="fa fa-comment login-user"></i>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" data-dismiss="modal" class="btn btn-custon-four btn-default">Cancel</button>

                    <button type="submit" class="btn btn-custon-four btn-success">Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>
<script>
    function showeditModal() {
        $('#editmembermodal').modal('show');
    }

    function showdeleteModal() {
        $('#AlertModalalert').modal('show');
    }
</script>


