<!doctype html>
<html class="no-js" lang="en">
    <body class="materialdesign">
        <div class="wrapper-pro">
            <div class="content-inner-all">

                <div class="header-top-area"></div>
                <!-- Breadcome start-->
                <div class="breadcome-area mg-b-30 small-dn">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="breadcome-list map-mg-t-40-gl shadow-reset">
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <ul class="breadcome-menu">
                                                <li><span class="bread-blod"> All Inactive Member .</span>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="income-order-visit-user-area">
                    <div class="container-fluid">
                        <?php
                        if ($this->session->userdata('add')):
                            echo '<div class="alert alert-success fade in"><button data-dismiss="alert" class="close close-sm" type="button"><i class="fa fa-times"></i></button><strong>Success Message !!! </strong> ' . $this->session->userdata('add') . '</div>';
                            $this->session->unset_userdata('add');
                        endif;
                        if ($this->session->userdata('notadd')):
                            echo '<div class="alert alert-block alert-danger fade in"><button data-dismiss="alert" class="close close-sm" type="button"><i class="fa fa-times"></i></button><strong>Failed Meaasge !!! </strong> ' . $this->session->userdata('notadd') . '</div>';
                            $this->session->unset_userdata('notadd');
                        endif;
                        ?>
                        <div class="row">
                            <div class="data-table-area mg-b-15">
                                <div class="container-fluid">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="sparkline13-list shadow-reset">
                                                <div class="sparkline13-hd">
                                                    <div class="main-sparkline13-hd">



                                                        <div class="sparkline13-outline-icon">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="sparkline13-graph">
                                                    <div class="datatable-dashv1-list custom-datatable-overright">
                                                        <div id="toolbar">
                                                            <select class="form-control">
                                                                <option value="">Export Basic</option>
                                                                <option value="all">Export All</option>
                                                                <option value="selected">Export Selected</option>
                                                            </select>

                                                        </div>
                                                        <table id="table" data-toggle="table" data-pagination="true" data-search="true" data-show-columns="true" data-show-pagination-switch="true" data-show-refresh="true" data-key-events="true" data-show-toggle="true" data-resizable="true" data-cookie="true" data-cookie-id-table="saveId" data-show-export="true" data-click-to-select="true" data-toolbar="#toolbar">
                                                            <thead>
                                                                <tr>
                                                                    <th data-field="state" data-checkbox="true"></th>
                                                                    <th data-field="id" >LM NO.</th>
                                                                    <th data-field="Name" >Name</th>
                                                                    <th data-field="Address" >Address</th>
                                                                    <th data-field="Phone" >Phone</th>
                                                                    <th data-field="Passing">Passing Year</th>
                                                                    <th data-field="Profession" >Profession</th>
                                                                    <th data-field="Email" >Email</th>
                                                                    <th data-field="Blood">Blood group</th>
                                                                    <th data-field="action">Action</th>


                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <?php foreach ($allmember as $value): ?>
                                                                    <tr>
                                                                        <td></td>
                                                                        <td><?= 'LM- ' . $value->lm_no; ?></td>
                                                                        <td><?= $value->name; ?></td>
                                                                        <td><?= $value->present_address; ?></td>
                                                                        <td><?= $value->phone; ?></td>
                                                                        <td><?= $value->passing_year; ?></td>
                                                                        <td><?= $value->profession; ?></td>
                                                                        <td><?= $value->email; ?></td>
                                                                        <td><?= $value->blood_group; ?></td>
                                                                        <td>
                                                                            <a href="#" onclick="makeactive('<?= $value->id; ?>');">
                                                                                <button type="button"class="btn-default">Make Active</button>
                                                                            </a>
                                                                            <a href="#" onclick="show_deletemodal('<?= $value->id; ?>');">
                                                                                <button type="button"class="btn-danger">Delete</button>
                                                                            </a>

                                                                        </td>

                                                                    </tr>
                                                                <?php endforeach; ?>

                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- income order visit user End -->


            </div>
        </div>

    </body>

</html>
<div id="delete_modal" class="modal fade " role="dialog" style="display: hidden;">
    <div class="modal-dialog">
        <div class="modal-content">
            <form action="<?= base_url('Member/delete_user'); ?>" method="POST">
                <div class="modal-header" style="background-color:honeydew;">
                    <b> Alert !!</b>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <input type="hidden" id="user_id" name="user_id" class="form-control">
                    <p style="font-size: 25px;color: red;">Are you sure,You want to delete this user ?</p><br><br>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success">YES</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">NO</button>
                </div>
            </form>
        </div>

    </div>
</div>
<script>
    function makeactive(id) {
        var user_id = id;

        var dataString = 'user_id=' + user_id;
        $.ajax({
            type: "POST",
            url: "<?php echo base_url('Member/make_activate'); ?>",
            data: dataString,
            success: function(data)
            {
                if (data == 'mass') {
                    $("#verific").text("User activation Successfully .");
                    $("#verific").css('color', 'green');
                    alert('Activation successfull');
                    location.reload();
                }
            }
        });

    }

    function show_deletemodal(id) {
        var user_id = id;
        $('#user_id').val(user_id);
        $('#delete_modal').modal('show');
    }
</script>


