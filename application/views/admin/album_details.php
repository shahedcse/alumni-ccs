<!doctype html>
<html class="no-js" lang="en">
    <body class="materialdesign">
        <div class="wrapper-pro">
            <div class="content-inner-all">

                <div class="header-top-area"></div>
                <!-- Breadcome start-->
                <div class="breadcome-area mg-b-30 small-dn">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="breadcome-list map-mg-t-40-gl shadow-reset">
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <ul class="breadcome-menu">
                                                <li><span class="bread-blod"> Album Details .</span>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="income-order-visit-user-area">
                    <div class="container-fluid">
                        <?php
                        if ($this->session->userdata('add')):
                            echo '<div class="alert alert-success fade in"><button data-dismiss="alert" class="close close-sm" type="button"><i class="fa fa-times"></i></button><strong>Success Message !!! </strong> ' . $this->session->userdata('add') . '</div>';
                            $this->session->unset_userdata('add');
                        endif;
                        if ($this->session->userdata('notadd')):
                            echo '<div class="alert alert-block alert-danger fade in"><button data-dismiss="alert" class="close close-sm" type="button"><i class="fa fa-times"></i></button><strong>Failed Meaasge !!! </strong> ' . $this->session->userdata('notadd') . '</div>';
                            $this->session->unset_userdata('notadd');
                        endif;
                        ?>
                        <div class="row">
                            <div class="data-table-area mg-b-15">
                                <div class="container-fluid">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="sparkline13-list shadow-reset">
                                                <div class="sparkline13-hd">
                                                    <div class="main-sparkline13-hd">
                                                        <a href="<?= base_url('Admin_media/image_gallery'); ?>">
                                                            <button type="button"   class="btn btn-custon-four btn-default"><-- Back to Album List</button>
                                                        </a>
                                                        <button type="button" data-toggle="modal" data-target="#addphotomodal" class="btn btn-custon-four btn-success pull-right">Add New </button>

                                                        <div class="sparkline13-outline-icon">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="sparkline13-graph">
                                                    <div class="datatable-dashv1-list custom-datatable-overright">
                                                        <div id="toolbar">
                                                            <select class="form-control">
                                                                <option value="">Export Basic</option>
                                                                <option value="all">Export All</option>
                                                                <option value="selected">Export Selected</option>
                                                            </select>

                                                        </div>
                                                        <table id="table" data-toggle="table" data-pagination="true" data-search="true" data-show-columns="true" data-show-pagination-switch="true" data-show-refresh="true" data-key-events="true" data-show-toggle="true" data-resizable="true" data-cookie="true" data-cookie-id-table="saveId" data-show-export="true" data-click-to-select="true" data-toolbar="#toolbar">
                                                            <thead>
                                                                <tr>
                                                                    <th data-field="state" data-checkbox="true"></th>
                                                                    <th data-field="id" >ID.</th>
                                                                    <th data-field="Name" >Image</th>
                                                                    <th data-field="action">Action</th>

                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <?php
                                                                $id = 0;
                                                                foreach ($album_details as $value):
                                                                    $id++;
                                                                    ?>
                                                                    <tr>
                                                                        <td></td>
                                                                        <td><?= $id ?></td>
                                                                        <td>
                                                                            <a  target="_blank" href="<?= base_url('assets/img/gallery/' . $value->image_path); ?>">
                                                                                <img  height="50px;" width="50px;"src="<?= base_url('assets/img/gallery/' . $value->image_path); ?>" class="img-responsive img-circle">
                                                                            </a>
                                                                        </td>
                                                                        <td>
                                                                            <button type="button"Onclick="showdeleteModal('<?= $value->id; ?>');"  class="btn btn-custon-four btn-danger ">Delete</button>  

                                                                        </td>
                                                                    </tr>
                                                                <?php endforeach; ?>

                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- income order visit user End -->


            </div>
        </div>

    </body>

</html>

<div id="addphotomodal" class="modal modal-adminpro-general fullwidth-popup-InformationproModal fade bounceInDown animated in" role="dialog" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header header-color-modal bg-color-2">
                <h4 class="modal-title">Add Another Image .</h4>
                <div class="modal-close-area modal-close-df">
                    <a class="close" data-dismiss="modal" href="#"><i class="fa fa-close"></i></a>
                </div>
            </div>
            <form action="<?= base_url('admin_media/addalbumphoto'); ?>"  enctype="multipart/form-data" method="POST">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-5">
                            <div class="login-input-head">
                                <p>Select Photo :</p>
                            </div>
                        </div>
                        <div class="col-lg-7">
                            <div class="login-input-area">
                                <input type="hidden"  name="album_id" id="album_id" value="<?= $album_id ?>">
                                <input type="file" multiple=""  name="userfile[]" id="file-input">
                            </div>
                            <div id="preview"></div>
                        </div>
                    </div>



                </div>
                <div class="modal-footer">
                    <button type="button" data-dismiss="modal" class="btn btn-custon-four btn-default">Cancel</button>

                    <button type="submit" class="btn btn-custon-four btn-success">Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div id="AlertModalalert" class="modal modal-adminpro-general FullColor-popup-AlertModal fade bounceInUp animated in" role="dialog" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-close-area modal-close-df">
                <a class="close" data-dismiss="modal" href="#"><i class="fa fa-close"></i></a>
            </div>
            <form action="<?= base_url('Admin_media/delete_photo'); ?>" method="POST">
                <div class="modal-body">
                    <span class="adminpro-icon adminpro-warning modal-check-pro information-icon-pro"></span>
                    <h2>Alert!</h2>
                    <p style="font-size: 20px;color: red;">Are You Sure Want To Delete  ?</p>
                    <input type="hidden"  name="photo_id" id="photo_id">
                    <input type="hidden"  name="album_id" id="album_id" value="<?= $album_id ?>">
                </div>
                <div class="modal-footer" style="padding-right: 40px;">
                    <button type="button" data-dismiss="modal" class="btn btn-custon-four btn-default">No</button>
                    <button type="submit" class="btn btn-custon-four btn-success">Yes</button>

                </div>
            </form>
        </div>
    </div>
</div>

<script>
    function showdeleteModal(id) {
        var photoid = id;
        $('#photo_id').val(photoid);
        $('#AlertModalalert').modal('show');
    }
</script>





