<!doctype html>
<html class="no-js" lang="en">
    <body class="materialdesign">
        <div class="wrapper-pro">
            <div class="content-inner-all">

                <div class="header-top-area"></div>
                <!-- Breadcome start-->
                <div class="breadcome-area mg-b-30 small-dn">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="breadcome-list map-mg-t-40-gl shadow-reset">
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <ul class="breadcome-menu">
                                                <li><span class="bread-blod"> Add Event .</span>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="login-form-area mg-t-30 mg-b-15">
                    <div class="container-fluid">
                        <div class="row">

                            <form id="adminpro-register-form" class="adminpro-form" action="<?= base_url('Sadmin/insert_event'); ?>" method="post" enctype="multipart/form-data">
                                <div class="col-lg-10 col-lg-offset-1">
                                    <div class="login-bg">
                                        <div class="form-group">

                                            <label class="col-md-4 control-label">Title :<span style="color:red">*</span></label>
                                            <div class="col-md-6 inputGroupContainer">
                                                <div class="input-group"><span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span><input id="event_tilte" name="event_tilte" placeholder="event_tilte" class="form-control" required="true" value="" type="text"></div>
                                            </div><br>
                                        </div><br>


                                        <div class="form-group">
                                            <label class="col-md-4 control-label">Event/News Place :</label>
                                            <div class="col-md-6 inputGroupContainer">
                                                <div class="input-group"><span class="input-group-addon"><i class="glyphicon glyphicon-pencil"></i></span><input id="place" name="place" placeholder="Event place" class="form-control"  value="" type="text"></div>
                                            </div><br>
                                        </div><br>
                                        <div class="form-group">
                                            <label class="col-md-4 control-label">Attach File:</label>
                                            <div class="col-md-6 inputGroupContainer">
                                                <div class="input-group"><span class="input-group-addon"><i class="glyphicon glyphicon-file"></i></span><input id="fileToUpload" name="fileToUpload"  class="form-control"  type="file"></div>
                                            </div><br>
                                        </div><br>
                                        <div class="form-group">
                                            <label class="col-md-4 control-label">Status:</label>
                                            <div class="col-md-6">
                                                <div class="input-group form-select-list">
                                                    <span class="input-group-addon" style="max-width: 100%;"><i class="glyphicon glyphicon-list"></i></span>
                                                    <select class=" form-control custom-select-value"id="status" name="status">
                                                        <option value="1">Active</option>
                                                        <option value="0">Inactive</option>

                                                    </select>
                                                </div>
                                            </div><br>
                                        </div><br>
                                        <div class="form-group">

                                            <label class="col-md-2 control-label">Event/News Details :<span style="color:red">*</span></label>
                                            <div class="col-md-9">
                                                <textarea class="form-control" type="text" id="summernote4" name="summernote">
                                                               Write event Details...
                                                </textarea>
                                                <center>
                                                    <button  type="Submit"   title="publish" class="btn btn-success ">Publish</button>
                                                </center>
                                            </div><br>

                                        </div><br>
                                    </div>
                                </div>
                            </form>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>





