<!doctype html>
<style>
    .typeahead, .tt-query, .tt-hint {

        border: 2px solid #ccc;
        -webkit-border-radius: 8px;
        -moz-border-radius: 8px;
        border-radius: 8px;
        outline: none;

    }

    .tt-suggestion {

        font-size: 14px;
        line-height: 20px;

    }
</style>

<html class="no-js" lang="en">
    <body class="materialdesign">
        <div class="wrapper-pro">
            <div class="content-inner-all">

                <div class="header-top-area"></div>
                <!-- Breadcome start-->
                <div class="breadcome-area mg-b-30 small-dn">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="breadcome-list map-mg-t-40-gl shadow-reset">
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <ul class="breadcome-menu">
                                                <li><span class="bread-blod"> Alumni Report .</span>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="income-order-visit-user-area">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="data-table-area mg-b-15">
                                <div class="container-fluid">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="sparkline13-list shadow-reset">
                                                <div class="sparkline13-hd">
                                                    <div class="sparkline10-graph"style="background-color: honeydew">
                                                        <div class="basic-login-form-ad">
                                                            <div class="row">
                                                                <div class="col-lg-12" >
                                                                    <div class="basic-login-inner inline-basic-form" >
                                                                        <form action="<?= base_url('Report'); ?>" method="POST">
                                                                            <div class="form-group-inner">
                                                                                <div class="row" id="prefetch">
                                                                                    <div class="col-lg-3" >
                                                                                        Name:
                                                                                        <input type="text" autocomplete="off" name="name" id="name" class="form-control basic-ele-mg-b-10 typeahead"  placeholder="Enter Name.">
                                                                                    </div>
                                                                                    <div class="col-lg-2">
                                                                                        LM No:
                                                                                        <div class="form-select-list">
                                                                                            <select class="form-control custom-select-value" name="lm_no" id="lm_no">
                                                                                                <option value="">-Select-</option>
                                                                                                <?php foreach ($all_lm as $value): ?>
                                                                                                    <option value="<?= $value->lm_no; ?>">LM-<?= $value->lm_no; ?></option>
                                                                                                <?php endforeach; ?>
                                                                                            </select>
                                                                                        </div>
                                                                                    </div>

                                                                                    <div class="col-lg-2">
                                                                                        Passing Year:
                                                                                        <div class="form-select-list">
                                                                                            <select class="form-control custom-select-value" name="pass_year" id="pass_year">
                                                                                                <option value="">-Select-</option>
                                                                                                <?php
                                                                                                $years_now = date("Y");
                                                                                                foreach (range($years_now, 1950) as $years):
                                                                                                    ?>
                                                                                                    <option value="<?= $years; ?>"><?= $years ?></option>
                                                                                                <?php endforeach; ?>
                                                                                            </select>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-lg-2">
                                                                                        Blood Group:
                                                                                        <div class="form-select-list">
                                                                                            <select class="form-control custom-select-value" name="blood_group" id="blood_group">
                                                                                                <option value="">-Select-</option>
                                                                                                <?php foreach ($blood as $value): ?>
                                                                                                    <option value="<?= $value->blood_group; ?>"><?= $value->blood_group; ?></option>
                                                                                                <?php endforeach; ?>
                                                                                            </select>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-lg-3" style="margin-top: 20px;">
                                                                                        <div class="login-btn-inner">
                                                                                            <div class="row">
                                                                                                <div class="col-lg-6">
                                                                                                    <button class="btn btn-sm btn-primary login-submit-cs" type="submit">Search</button>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </form>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="main-sparkline13-hd">
                                                        <div class="sparkline13-outline-icon">

                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="sparkline13-graph">
                                                    <div class="datatable-dashv1-list custom-datatable-overright">
                                                        <div id="toolbar">
                                                            <select class="form-control">
                                                                <option value="">Export Basic</option>
                                                                <option value="all">Export All</option>
                                                                <option value="selected">Export Selected</option>
                                                            </select>

                                                        </div>
                                                        <table id="table" data-toggle="table" data-pagination="true" data-search="true" data-show-columns="true" data-show-pagination-switch="true" data-show-refresh="true" data-key-events="true" data-show-toggle="true" data-resizable="true" data-cookie="true" data-cookie-id-table="saveId" data-show-export="true" data-click-to-select="true" data-toolbar="#toolbar">
                                                            <thead>
                                                                <tr>
                                                                    <th data-field="state" data-checkbox="true"></th>
                                                                    <th data-field="id" >LM NO.</th>
                                                                    <th data-field="Name" >Name</th>
                                                                    <th data-field="Address" >Address</th>
                                                                    <th data-field="Phone" >Phone</th>
                                                                    <th data-field="Passing">Passing Year</th>
                                                                    <th data-field="Profession" >Profession</th>
                                                                    <th data-field="Email" >Email</th>
                                                                    <th data-field="Blood">Blood group</th>
                                                                    <th data-field="photo">Photo</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <?php if (empty($allmember)): ?>
                                                                    <tr>
                                                                        <td></td>
                                                                        <td style="text-align: center;"colspan="9">No result Found</td>
                                                                    </tr>
                                                                <?php else: ?>
                                                                    <?php foreach ($allmember as $value): ?>
                                                                        <tr>
                                                                            <td></td>
                                                                            <td><?= 'LM- ' . $value->lm_no; ?></td>
                                                                            <td><?= $value->name; ?></td>
                                                                            <td><?= $value->adress; ?></td>
                                                                            <td><?= $value->phone; ?></td>
                                                                            <td><?= $value->passing_year; ?></td>
                                                                            <td><?= $value->profession; ?></td>
                                                                            <td><?= $value->email; ?></td>
                                                                            <td><?= $value->blood_group; ?></td>
                                                                            <td>
                                                                                <?php
                                                                                $filename = 'assets/img/profile/' . $value->image_path;
                                                                                if (file_exists($filename)):
                                                                                    ?>
                                                                                    <img height="45px;" width="45px;" src="<?= base_url('assets/img/profile/' . $value->image_path); ?>" alt="">
                                                                                <?php else: ?>
                                                                                    <img height="45px;" width="45px;"src="<?= base_url('assets/img/profile/demo.jpg'); ?>" alt="">
                                                                                <?php endif; ?>

                                                                            </td>
                                                                        </tr>
                                                                    <?php endforeach; ?>
                                                                <?php endif; ?>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </body>

</html>

<script>
    $(document).ready(function() {
        var sample_data = new Bloodhound({
            datumTokenizer: Bloodhound.tokenizers.obj.whitespace('value'),
            queryTokenizer: Bloodhound.tokenizers.whitespace,
            prefetch: '<?php echo base_url(); ?>Report/fetch',
            remote: {
                url: '<?php echo base_url(); ?>Report/fetch/%QUERY',
                wildcard: '%QUERY'
            }
        });
        $('#prefetch .typeahead').typeahead(null, {
            name: 'sample_data',
            display: 'name',
            source: sample_data,
            limit: 10,
            templates: {
                suggestion: Handlebars.compile('<div class="row"><div class=" form-control custom-select-value col-lg-6" >{{name}}</div></div>')
            }
        });
    });

    /* When the user clicks on the button, 
     toggle between hiding and showing the dropdown content */
    function myFunction() {
        document.getElementById("myDropdown").classList.toggle("show");
    }

    // Close the dropdown if the user clicks outside of it
    window.onclick = function(event) {
        if (!event.target.matches('.dropbtn')) {

            var dropdowns = document.getElementsByClassName("dropdown-content");
            var i;
            for (i = 0; i < dropdowns.length; i++) {
                var openDropdown = dropdowns[i];
                if (openDropdown.classList.contains('show')) {
                    openDropdown.classList.remove('show');
                }
            }
        }
    }
</script>


