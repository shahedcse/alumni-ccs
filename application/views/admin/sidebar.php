<div class="left-sidebar-pro">
    <nav id="sidebar">
        <div class="sidebar-header">
            <a href="<?= base_url('Auth/profile'); ?>"><img  width="10px" height="50px"src="<?= base_url(); ?>assets/img/logo.png" alt="" />
                <p>Alumni CCS</p>
                <strong>CCS</strong>
            </a>
        </div>
        <div class="left-custom-menu-adp-wrap">
            <ul class="nav navbar-nav left-sidebar-menu-pro">
                <?php if (in_array($this->session->userdata('user_role'), array(1, 3))) : ?>
                    <li class="nav-item">
                        <a href="<?= base_url('Sadmin/dashboard'); ?>"  role="button" aria-expanded="false" ><i class="fa big-icon fa-home"></i> <span class="mini-dn">Dashboard</span> <span class="indicator-right-menu mini-dn"></span></a>
                    </li>

                    <li class="nav-item "><a href="#" data-toggle="dropdown" role="button" aria-expanded="false" class="nav-link dropdown-toggle"><i class="fa big-icon fa-laptop"></i> <span class="mini-dn">Admin</span> <span class="indicator-right-menu mini-dn"><i class="fa indicator-mn fa-angle-left"></i></span></a>
                        <div role="menu" class="dropdown-menu left-menu-dropdown animated flipInX">
                            <a href="<?= base_url('Sadmin/user_panel'); ?>" class="dropdown-item">System User </a>
                            <a href="<?= base_url('Sadmin/allblog'); ?>" class="dropdown-item">Blog List</a>
                            <a href="<?= base_url('Sadmin/event'); ?>" class="dropdown-item">Event List </a>
                            <a href="<?= base_url('Admin_media/image_gallery'); ?>" class="dropdown-item">Image Gallery </a>
                            <a href="#" class="dropdown-item">Video Gallery </a>
                        </div>
                    </li>
                    <li class="nav-item "><a href="#" data-toggle="dropdown" role="button" aria-expanded="false" class="nav-link dropdown-toggle"><i class="fa big-icon fa-users"></i> <span class="mini-dn">Member Panel</span> <span class="indicator-right-menu mini-dn"><i class="fa indicator-mn fa-angle-left"></i></span></a>
                        <div role="menu" class="dropdown-menu left-menu-dropdown animated flipInX">
                            <a href="<?= base_url('Member_panel/enlisted_member'); ?>" class="dropdown-item">Enlisted Member</a>
                            <a href="<?= base_url('Member_panel/registered_member'); ?>" class="dropdown-item">Registered Member</a>
                            <a href="<?= base_url('Member_panel/inactivie_member'); ?>" class="dropdown-item">Inactive Member</a>
                        </div>
                    </li>
                <?php endif; ?>
                <li class="nav-item">
                    <a href="<?= base_url('Auth/profile'); ?>"  role="button" aria-expanded="false" ><i class="fa big-icon fa-user"></i> <span class="mini-dn">My Profile</span> <span class="indicator-right-menu mini-dn"></span></a>
                </li>

                <li class="nav-item">
                    <a href="<?= base_url('Blog/bloglist'); ?>"  role="button" aria-expanded="false" ><i class="fa big-icon fa-list"></i> <span class="mini-dn">My Blog</span> <span class="indicator-right-menu mini-dn"></span></a>
                </li>
                <?php if (in_array($this->session->userdata('user_role'), array(1, 3))) : ?>
                    <li class="nav-item">
                        <a href="<?= base_url('Report'); ?>"  role="button" aria-expanded="false" ><i class="fa big-icon fa-reorder"></i> <span class="mini-dn">Report</span> <span class="indicator-right-menu mini-dn"></span></a>
                    </li>

                    <li class="nav-item">
                        <a href="<?= base_url('Sadmin/messages'); ?>"  role="button" aria-expanded="false" ><i class="fa big-icon fa-envelope"></i> <span class="mini-dn">Support Msg.</span> <span class="indicator-right-menu mini-dn"></span></a>
                    </li>
                <?php endif; ?>
<!--    <li class="nav-item "><a href="#" data-toggle="dropdown" role="button" aria-expanded="false" class="nav-link dropdown-toggle"><i class="fa big-icon fa-sitemap"></i> <span class="mini-dn">Website panel</span> <span class="indicator-right-menu mini-dn"><i class="fa indicator-mn fa-angle-left"></i></span></a>
        <div role="menu" class="dropdown-menu left-menu-dropdown animated flipInX">
            <a href="#" class="dropdown-item">Alumni Story</a>
            <a href="#" class="dropdown-item">Terms & Condition</a>
            <a href="#" class="dropdown-item">Embeded Video</a>
            <a href="#" class="dropdown-item">Home Popup </a>
            <a href="#" class="dropdown-item">Testimonial & Messages</a>
        </div>
    </li>-->
                <li class="nav-item">
                    <a href="#" onclick="showlogoutModal();"  role="button" aria-expanded="false" ><i class="fa big-icon fa-lock"></i> <span class="mini-dn">Logout</span> <span class="indicator-right-menu mini-dn"></span></a>
                </li>
            </ul>
        </div>
    </nav>
</div>

<div class="mobile-menu-area">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="mobile-menu">
                    <nav id="dropdown">
                        <ul class="mobile-menu-nav">
                            <li class="nav-item">
                                <a href="#"  role="button" aria-expanded="false" ><i class="fa big-icon fa-home"></i> <span class="mini-dn">Dashboard</span> <span class="indicator-right-menu mini-dn"></span></a>
                            </li>
                            <li class="nav-item "><a href="#" data-toggle="dropdown" role="button" aria-expanded="false" class="nav-link dropdown-toggle"><i class="fa big-icon fa-users"></i> <span class="mini-dn">Admin</span> <span class="indicator-right-menu mini-dn"><i class="fa indicator-mn fa-angle-left"></i></span></a>
                                <div role="menu" class="dropdown-menu left-menu-dropdown animated flipInX">
                                    <a href="#" class="dropdown-item">System User </a>
                                    <a href="#" class="dropdown-item">Blog List</a>
                                    <a href="#" class="dropdown-item">Event List </a>
                                    <a href="#" class="dropdown-item">Image Gallery </a>
                                    <a href="#" class="dropdown-item">Video Gallery </a>
                                </div>
                            </li>
                            <li class="nav-item "><a href="#" data-toggle="dropdown" role="button" aria-expanded="false" class="nav-link dropdown-toggle"><i class="fa big-icon fa-users"></i> <span class="mini-dn">Member Panel</span> <span class="indicator-right-menu mini-dn"><i class="fa indicator-mn fa-angle-left"></i></span></a>
                                <div role="menu" class="dropdown-menu left-menu-dropdown animated flipInX">
                                    <a href="#" class="dropdown-item">Enlisted Member</a>
                                    <a href="#" class="dropdown-item">Registered Member</a>
                                    <a href="#" class="dropdown-item">Inactive Member</a>
                                </div>
                            </li>
                            <li class="nav-item">
                                <a href="#"  role="button" aria-expanded="false" ><i class="fa big-icon fa-home"></i> <span class="mini-dn">My Blog</span> <span class="indicator-right-menu mini-dn"></span></a>
                            </li>


                            <li class="nav-item">
                                <a href="#" onclick="showlogoutModal();"  role="button" aria-expanded="false" ><i class="fa big-icon fa-lock"></i> <span class="mini-dn">Logout</span> <span class="indicator-right-menu mini-dn"></span></a>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="zoomInDown1" class="modal modal-adminpro-general modal-zoomInDown fade zoomInDown animated in" role="dialog" style="display: hidden;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-close-area modal-close-df">
                <a class="close" data-dismiss="modal" href="#"><i class="fa fa-close"></i></a>
            </div>
            <div class="modal-body">
                <div class="modal-login-form-inner">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="login-social-inner">

                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="basic-login-inner modal-basic-inner">
                                <h3>Are You Sure,want to logout?</h3>

                                <div class="create-account-sign">
                                    <center>
                                        <a href="<?= base_url('Auth/logout'); ?>"><button type="button"  class="btn btn-lg">LOG OUT</button></a>

                                        <button type="button"  data-dismiss="modal" class="btn  btn-success">Stay Here</button>
                                    </center>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="zoomInDown2" class="modal modal-adminpro-general modal-zoomInDown fade zoomInDown animated in" role="dialog" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-close-area modal-close-df">
                <a class="close" data-dismiss="modal" href="#"><i class="fa fa-close"></i></a>
            </div>
            <div class="modal-body">
                <div class="modal-login-form-inner">

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="basic-login-inner modal-basic-inner">
                                <h3>Change Your Password</h3>

                                <form action="<?= base_url('Auth/changepassword'); ?>" method="post">
                                    <?php
                                    $id = $this->session->userdata('user_id');
                                    $passquery = $this->db->query("SELECT password from users WHERE id='$id'")->row()->password;
                                    ?>
                                    <div class="form-group-inner">
                                        <div class="row">
                                            <div class="col-lg-4">
                                                <label class="login2">Current Password</label>
                                            </div>
                                            <div class="col-lg-8">
                                                <input type="text" class="form-control" value="<?= $passquery; ?>" name="cpassword" placeholder="password">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group-inner">
                                        <div class="row">
                                            <div class="col-lg-4">
                                                <label class="login2"> New Password</label>
                                            </div>
                                            <div class="col-lg-8">
                                                <input type="password" name="npassword" id=npassword class="form-control" placeholder="password">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group-inner">
                                        <div class="row">
                                            <div class="col-lg-4">
                                                <label class="login2">Confirm New Password</label>
                                            </div>
                                            <div class="col-lg-8">
                                                <input type="password" name="npassword2" id="npassword2" class="form-control" placeholder="password">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="login-btn-inner">
                                        <div class="row">
                                            <div class="col-lg-4"></div>
                                            <div class="col-lg-8">
                                                <div class="login-horizental">
                                                    <button class="btn btn-sm btn-primary login-submit-cs" id="update" type="submit">Update</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    var password = document.getElementById("npassword")
            , confirm_password = document.getElementById("npassword2");

    function validatePassword() {
        if (password.value != confirm_password.value) {
            confirm_password.setCustomValidity("Password Don't Match");
        } else {
            confirm_password.setCustomValidity('');
        }
    }

    password.onchange = validatePassword;
    confirm_password.onkeyup = validatePassword;
</script>

