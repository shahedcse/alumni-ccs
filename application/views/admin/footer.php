<!-- Footer Start-->
<div class="footer-copyright-area" style="margin-top: 30px;">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="footer-copy-right">
                    <p style="color: #D10074;">Design & Developed by  <a  target="_blank"href="http://logicandthoughts.com/">Logic & Thoughts</a>.</p>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="<?= base_url(); ?>admin/assets/js/jquery.meanmenu.js"></script>
<!-- mCustomScrollbar JS
            ============================================ -->


<script src="<?= base_url(); ?>admin/assets/js/jquery.mCustomScrollbar.concat.min.js"></script>
<!-- sticky JS
            ============================================ -->
<script src="<?= base_url(); ?>admin/assets/js/jquery.sticky.js"></script>
<!-- scrollUp JS
            ============================================ -->
<script src="<?= base_url(); ?>admin/assets/js/jquery.scrollUp.min.js"></script>
<!-- scrollUp JS
            ============================================ -->
<script src="<?= base_url(); ?>admin/assets/js/wow/wow.min.js"></script>
<!-- counterup JS
            ============================================ -->
<script src="<?= base_url(); ?>admin/assets/js/counterup/jquery.counterup.min.js"></script>
<script src="<?= base_url(); ?>admin/assets/js/counterup/waypoints.min.js"></script>
<script src="<?= base_url(); ?>admin/assets/js/counterup/counterup-active.js"></script>
<!-- rounded-counter JS
                ============================================ -->
<script src="<?= base_url(); ?>admin/assets/js/rounded-counter/jquery.countdown.min.js"></script>
<script src="<?= base_url(); ?>admin/assets/js/rounded-counter/jquery.knob.js"></script>
<script src="<?= base_url(); ?>admin/assets/js/rounded-counter/jquery.appear.js"></script>
<script src="<?= base_url(); ?>admin/assets/js/rounded-counter/knob-active.js"></script>
<!-- jvectormap JS
            ============================================ -->
<script src="<?= base_url(); ?>admin/assets/js/jvectormap/jquery-jvectormap-2.0.2.min.js"></script>
<script src="<?= base_url(); ?>admin/assets/js/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!--<script src="<?= base_url(); ?>admin/assets/js/jvectormap/jvectormap-active.js"></script>-->
<!-- peity JS
            ============================================ -->
<script src="<?= base_url(); ?>admin/assets/js/peity/jquery.peity.min.js"></script>
<script src="<?= base_url(); ?>admin/assets/js/peity/peity-active.js"></script>
<!-- input-mask JS
              ============================================ -->
<script src="<?= base_url(); ?>admin/assets/js/input-mask/jasny-bootstrap.min.js"></script>
<!-- sparkline JS
            ============================================ -->
<script src="<?= base_url(); ?>admin/assets/js/sparkline/jquery.sparkline.min.js"></script>
<script src="<?= base_url(); ?>admin/assets/js/sparkline/sparkline-active.js"></script>

<!-- maskedinput JS
            ============================================ -->
<script src="<?= base_url(); ?>admin/assets/js/jquery.maskedinput.min.js"></script>
<script src="<?= base_url(); ?>admin/assets/js/masking-active.js"></script>
<!-- chosen JS
              ============================================ -->
<script src="<?= base_url(); ?>admin/assets/js/chosen/chosen.jquery.js"></script>
<script src="<?= base_url(); ?>admin/assets/js/chosen/chosen-active.js"></script>
<!-- datepicker JS
            ============================================ -->

<script src="<?= base_url(); ?>admin/assets/js/jquery-ui.min.js"></script>
<script src="<?= base_url(); ?>admin/assets/js/datapicker/bootstrap-datepicker.js"></script>
<script src="<?= base_url(); ?>admin/assets/js/datepicker-active.js"></script>
<!-- form validate JS
            ============================================ -->
<script src="<?= base_url(); ?>admin/assets/js/jquery.form.min.js"></script>
<script src="<?= base_url(); ?>admin/assets/js/jquery.validate.min.js"></script>
<script src="<?= base_url(); ?>admin/assets/js/form-active.js"></script>
<!-- flot JS
            ============================================ -->
<script src="<?= base_url(); ?>admin/assets/js/flot/Chart.min.js"></script>
<script src="<?= base_url(); ?>admin/assets/js/flot/dashtwo-flot-active.js"></script>
<!-- data table JS
            ============================================ -->
<script src="<?= base_url(); ?>admin/assets/js/data-table/bootstrap-table.js"></script>
<script src="<?= base_url(); ?>admin/assets/js/data-table/tableExport.js"></script>
<script src="<?= base_url(); ?>admin/assets/js/data-table/data-table-active.js"></script>
<script src="<?= base_url(); ?>admin/assets/js/data-table/bootstrap-table-editable.js"></script>
<script src="<?= base_url(); ?>admin/assets/js/data-table/bootstrap-editable.js"></script>
<script src="<?= base_url(); ?>admin/assets/js/data-table/bootstrap-table-resizable.js"></script>
<script src="<?= base_url(); ?>admin/assets/js/data-table/colResizable-1.5.source.js"></script>
<script src="<?= base_url(); ?>admin/assets/js/data-table/bootstrap-table-export.js"></script>
<!-- select2 JS
                ============================================ -->
<script src="<?= base_url(); ?>admin/assets/js/select2/select2.full.min.js"></script>
<script src="<?= base_url(); ?>admin/assets/js/select2/select2-active.js"></script>
<!-- ionRangeSlider JS
            ============================================ -->
<script src="<?= base_url(); ?>admin/assets/js/ionRangeSlider/ion.rangeSlider.min.js"></script>
<script src="<?= base_url(); ?>admin/assets/js/ionRangeSlider/ion.rangeSlider.active.js"></script>
<!-- rangle-slider JS
            ============================================ -->
<!--<script src="<?= base_url(); ?>admin/assets/js/rangle-slider/jquery-ui-1.10.4.custom.min.js"></script>
<script src="<?= base_url(); ?>admin/assets/js/rangle-slider/jquery-ui-touch-punch.min.js"></script>
<script src="<?= base_url(); ?>admin/assets/js/rangle-slider/rangle-active.js"></script>-->
<!-- knob JS
            ============================================ -->
<script src="<?= base_url(); ?>admin/assets/js/knob/jquery.knob.js"></script>
<script src="<?= base_url(); ?>admin/assets/js/knob/knob-active.js"></script>
<!-- summernote JS
              ============================================ -->
<script src="<?= base_url(); ?>admin/assets/js/summernote.min.js"></script>
<script src="<?= base_url(); ?>admin/assets/js/summernote-active.js"></script>
<!--  editable JS
              ============================================ -->
<script src="<?= base_url(); ?>admin/assets/js/jquery.mockjax.js"></script>
<script src="<?= base_url(); ?>admin/assets/js/mock-active.js"></script>
<script src="<?= base_url(); ?>admin/assets/js/select2.js"></script>
<script src="<?= base_url(); ?>admin/assets/js/moment.min.js"></script>
<script src="<?= base_url(); ?>admin/assets/js/bootstrap-datetimepicker.js"></script>
<script src="<?= base_url(); ?>admin/assets/js/bootstrap-editable.js"></script>
<script src="<?= base_url(); ?>admin/assets/js/xediable-active.js"></script>
<!--  Chat JS
            ============================================ -->
<script src="<?= base_url(); ?>admin/assets/js/chat-active/jquery.chat.js"></script>
<!--  todo JS
            ============================================ -->
<script src="<?= base_url(); ?>admin/assets/js/todo/jquery.todo.js"></script>


<!-- main JS
            ============================================ -->
<script src="<?= base_url(); ?>admin/assets/js/main.js"></script>
<script type="text/javascript">

    (function() {
        var options = {
            facebook: "157089424390855", // Facebook page ID
            call_to_action: "Message us", // Call to action
            position: "Right" // Position may be 'right' or 'left'
        };
        var proto = document.location.protocol, host = "whatshelp.io", url = proto + "//static." + host;
        var s = document.createElement('script');
        s.type = 'text/javascript';
        s.async = true;
        s.src = url + '/widget-send-button/js/init.js';
        s.onload = function() {
            WhWidgetSendButton.init(host, proto, options);
        };
        var x = document.getElementsByTagName('script')[0];
        x.parentNode.insertBefore(s, x);
    })();
</script>
<script>
    function showlogoutModal() {

        $('#zoomInDown1').modal('show');
    }


    function changepassModal() {

        $('#zoomInDown2').modal('show');
    }
</script>


