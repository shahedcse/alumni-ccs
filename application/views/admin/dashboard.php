<!doctype html>
<html class="no-js" lang="en">
    <body class="materialdesign">
        <div class="wrapper-pro">
            <div class="content-inner-all">

                <div class="header-top-area"></div>
                <!-- Breadcome start-->
                <div class="breadcome-area mg-b-30 small-dn">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="breadcome-list map-mg-t-40-gl shadow-reset">
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <ul class="breadcome-menu">
                                                <li><span class="bread-blod"> Dashboard.</span>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="income-order-visit-user-area">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-lg-3">
                                <div class="income-dashone-total income-monthly shadow-reset nt-mg-b-30">
                                    <div class="income-title">
                                        <div class="main-income-head">
                                            <h2>Enlisted Member</h2>
                                        </div>
                                    </div>
                                    <div class="income-dashone-pro">
                                        <div class="income-rate-total">
                                            <div class="price-adminpro-rate">
                                                <h3><span class="counter"><?= $enlistuser; ?></span></h3>
                                            </div>
                                            <div class="price-graph">
                                                <span id="sparkline1"><canvas style="display: inline-block; width: 27px; height: 19px; vertical-align: top;" width="27" height="19"></canvas></span>
                                            </div>
                                        </div>
                                        <div class="clear"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <div class="income-dashone-total orders-monthly shadow-reset nt-mg-b-30">
                                    <div class="income-title">
                                        <div class="main-income-head">
                                            <h2>Registered Member</h2>
                                        </div>
                                    </div>
                                    <div class="income-dashone-pro">
                                        <div class="income-rate-total">
                                            <div class="price-adminpro-rate">
                                                <h3><span class="counter"><?= $alluser; ?></span></h3>
                                            </div>
                                            <div class="price-graph">
                                                <span id="sparkline6"><canvas style="display: inline-block; width: 56px; height: 19px; vertical-align: top;" width="56" height="19"></canvas></span>
                                            </div>
                                        </div>

                                        <div class="clear"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <div class="income-dashone-total visitor-monthly shadow-reset nt-mg-b-30">
                                    <div class="income-title">
                                        <div class="main-income-head">
                                            <h2>Total Messages</h2>
                                        </div>
                                    </div>
                                    <div class="income-dashone-pro">
                                        <div class="income-rate-total">
                                            <div class="price-adminpro-rate">
                                                <h3><span class="counter"><?= $allmessage; ?></span></h3>
                                            </div>
                                            <div class="price-graph">
                                                <span id="sparkline2"><canvas style="display: inline-block; width: 39px; height: 19px; vertical-align: top;" width="39" height="19"></canvas></span>
                                            </div>
                                        </div>
                                        <div class="clear"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <div class="income-dashone-total user-monthly shadow-reset nt-mg-b-30">
                                    <div class="income-title">
                                        <div class="main-income-head">
                                            <h2>Total News & Blog</h2>

                                        </div>
                                    </div>
                                    <div class="income-dashone-pro">
                                        <div class="income-rate-total">
                                            <div class="price-adminpro-rate">
                                                <h3><span class="counter"><?= $allblog; ?></span></h3>
                                            </div>
                                            <div class="price-graph">
                                                <span id="sparkline5"><canvas style="display: inline-block; width: 59px; height: 19px; vertical-align: top;" width="59" height="19"></canvas></span>
                                            </div>
                                        </div>
                                        <div class="clear"></div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-12">
                                <div class="dashtwo-order-list shadow-reset">
                                    <div class="row">
                                        <div class="col-lg-9">
                                            <div class="wrapper"><iframe class="chartjs-hidden-iframe" style="width: 100%; display: block; border: 0px none; height: 0px; margin: 0px; position: absolute; inset: 0px;"></iframe>
                                                <canvas id="myChartsrs" width="802" height="200" style="display: block; width: 802px; height: 200px;"></canvas>
                                            </div>
                                        </div>
                                        <div class="col-lg-3">
                                            <div class="skill-content-3">
                                                <div class="skill">
                                                    <div class="progress">
                                                        <div class="lead-content">
                                                            <h3>2,346</h3>
                                                            <p>Total orders in period</p>
                                                        </div>
                                                        <div class="progress-bar wow fadeInLeft" data-progress="95%" style="width: 95%;" data-wow-duration="1.5s" data-wow-delay="1.2s"> <span>95%</span>
                                                        </div>
                                                    </div>
                                                    <div class="progress">
                                                        <div class="lead-content">
                                                            <h3>9,346</h3>
                                                            <p>Orders in last month</p>
                                                        </div>
                                                        <div class="progress-bar wow fadeInLeft" data-progress="85%" style="width: 85%;" data-wow-duration="1.5s" data-wow-delay="1.2s"><span>85%</span> </div>
                                                    </div>
                                                    <div class="progress progress-bt">
                                                        <div class="lead-content">
                                                            <h3>2,34,600</h3>
                                                            <p>Monthly income from order</p>
                                                        </div>
                                                        <div class="progress-bar wow fadeInLeft" data-progress="93%" style="width: 93%;" data-wow-duration="1.5s" data-wow-delay="1.2s"><span>93%</span> </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </body>
</html>



