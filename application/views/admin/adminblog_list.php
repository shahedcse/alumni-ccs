<!doctype html>
<html class="no-js" lang="en">
    <body class="materialdesign">
        <div class="wrapper-pro">
            <div class="content-inner-all">

                <div class="header-top-area"></div>
                <!-- Breadcome start-->
                <div class="breadcome-area mg-b-30 small-dn">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="breadcome-list map-mg-t-40-gl shadow-reset">
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <ul class="breadcome-menu">
                                                <li><span class="bread-blod"> All News & Blog .</span>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="income-order-visit-user-area">
                    <div class="container-fluid">
                        <?php
                        if ($this->session->userdata('add')):
                            echo '<div class="alert alert-success fade in"><button data-dismiss="alert" class="close close-sm" type="button"><i class="fa fa-times"></i></button><strong>Success Message !!! </strong> ' . $this->session->userdata('add') . '</div>';
                            $this->session->unset_userdata('add');
                        endif;
                        if ($this->session->userdata('notadd')):
                            echo '<div class="alert alert-block alert-danger fade in"><button data-dismiss="alert" class="close close-sm" type="button"><i class="fa fa-times"></i></button><strong>Failed Meaasge !!! </strong> ' . $this->session->userdata('notadd') . '</div>';
                            $this->session->unset_userdata('notadd');
                        endif;
                        ?>
                        <div class="row">
                            <div class="data-table-area mg-b-15">
                                <div class="container-fluid">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="sparkline13-list shadow-reset">
                                                <div class="sparkline13-hd">
                                                    <div class="main-sparkline13-hd">
                                                        <div class="sparkline13-outline-icon">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="sparkline13-graph">
                                                    <div class="datatable-dashv1-list custom-datatable-overright">
                                                        <div id="toolbar">
                                                            <select class="form-control">
                                                                <option value="">Export Basic</option>
                                                                <option value="all">Export All</option>
                                                                <option value="selected">Export Selected</option>
                                                            </select>

                                                        </div>
                                                        <table id="table" data-toggle="table" data-pagination="true" data-search="true" data-show-columns="true" data-show-pagination-switch="true" data-show-refresh="true" data-key-events="true" data-show-toggle="true" data-resizable="true" data-cookie="true" data-cookie-id-table="saveId" data-show-export="true" data-click-to-select="true" data-toolbar="#toolbar">
                                                            <thead>
                                                                <tr>
                                                                    <th data-field="state" data-checkbox="true"></th>
                                                                    <th data-field="id" >ID.</th>
                                                                    <th data-field="title">Title</th>
                                                                    <th data-field="cat">Category</th>
                                                                    <th data-field="date">Published Date</th>
                                                                    <th data-field="action">Action</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <?php
                                                                $i = 0;
                                                                if (sizeof($all_blogadmin)):
                                                                    foreach ($all_blogadmin as $value):
                                                                        $i++;
                                                                        ?>
                                                                        <tr>
                                                                            <td></td>
                                                                            <td><?= $i ?></td>
                                                                            <td><?= $value->blog_tilte; ?></td>
                                                                            <td><?= $value->category_name; ?></td>
                                                                            <td><?= $value->created_date; ?></td>
                                                                            <td>
                                                                                <a  target="_blank" href="<?= base_url('Blog/blog_view?id=' . $value->id); ?>">
                                                                                    <button type="button"class="btn-default">Web View</button>
                                                                                </a>
                                                                                <a href="#">
                                                                                    <button type="button"class="btn-success">Approved</button>
                                                                                </a>
                                                                                <?php if ($this->session->userdata("user_role") == '1'): ?>
                                                                                    <a href="#" onclick="show_deletemodal('<?= $value->id; ?>');">
                                                                                        <button type="button"class="btn-danger">Delete</button>
                                                                                    </a>
                                                                                <?php endif; ?>
                                                                            </td>

                                                                        </tr>
                                                                        <?php
                                                                    endforeach;
                                                                endif;
                                                                ?>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </body>

</html>
<div id="delete_modal" class="modal fade " role="dialog" style="display: hidden;">
    <div class="modal-dialog">
        <div class="modal-content">
            <form action="<?= base_url('Blog/delete_blog_admin'); ?>" method="POST">
                <div class="modal-header" style="background-color:honeydew;">
                    <b> Alert !!</b>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <input type="hidden" id="blog_id" name="blog_id" class="form-control">
                    <p style="font-size: 25px;color: red;">Are you sure,You want to delete this blog post ?</p><br><br>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success">YES</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">NO</button>
                </div>
            </form>
        </div>

    </div>
</div>
<script>
    function show_deletemodal(id) {
        var blog_id = id;
        $('#blog_id').val(blog_id);
        $('#delete_modal').modal('show');
    }
</script>




