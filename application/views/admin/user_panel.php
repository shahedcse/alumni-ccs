<!doctype html>
<html class="no-js" lang="en">
    <body class="materialdesign">
        <div class="wrapper-pro">
            <div class="content-inner-all">

                <div class="header-top-area"></div>
                <!-- Breadcome start-->
                <div class="breadcome-area mg-b-30 small-dn">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="breadcome-list map-mg-t-40-gl shadow-reset">
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <ul class="breadcome-menu">
                                                <li><span class="bread-blod"> System User Panel .</span>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="income-order-visit-user-area">
                    <div class="container-fluid">
                        <?php
                        if ($this->session->userdata('add')):
                            echo '<div class="alert alert-success fade in"><button data-dismiss="alert" class="close close-sm" type="button"><i class="fa fa-times"></i></button><strong>Success Message !!! </strong> ' . $this->session->userdata('add') . '</div>';
                            $this->session->unset_userdata('add');
                        endif;
                        if ($this->session->userdata('notadd')):
                            echo '<div class="alert alert-block alert-danger fade in"><button data-dismiss="alert" class="close close-sm" type="button"><i class="fa fa-times"></i></button><strong>Failed Meaasge !!! </strong> ' . $this->session->userdata('notadd') . '</div>';
                            $this->session->unset_userdata('notadd');
                        endif;
                        ?>
                        <div class="row">
                            <div class="data-table-area mg-b-15">
                                <div class="container-fluid">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="sparkline13-list shadow-reset">
                                                <div class="sparkline13-hd">
                                                    <div class="main-sparkline13-hd">

                                                        <button type="button" data-toggle="modal" data-target="#member_modal" class="btn btn-custon-four btn-success pull-right">Add New </button>

                                                        <div class="sparkline13-outline-icon">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="sparkline13-graph">
                                                    <div class="datatable-dashv1-list custom-datatable-overright">
                                                        <div id="toolbar">
                                                            <select class="form-control">
                                                                <option value="">Export Basic</option>
                                                                <option value="all">Export All</option>
                                                                <option value="selected">Export Selected</option>
                                                            </select>

                                                        </div>
                                                        <table id="table" data-toggle="table" data-pagination="true" data-search="true" data-show-columns="true" data-show-pagination-switch="true" data-show-refresh="true" data-key-events="true" data-show-toggle="true" data-resizable="true" data-cookie="true" data-cookie-id-table="saveId" data-show-export="true" data-click-to-select="true" data-toolbar="#toolbar">
                                                            <thead>
                                                                <tr>
                                                                    <th data-field="state" data-checkbox="true"></th>
                                                                    <th data-field="id" >ID.</th>
                                                                    <th data-field="Name">Name</th>
                                                                    <th data-field="Phone">Phone</th>
                                                                    <th data-field="role">Role</th>
                                                                    <th data-field="action">Action</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <?php
                                                                foreach ($allmember as $value):
                                                                    $roleqr = $this->db->query("SELECT role_name FROM userrole JOIN users ON users.role=userrole.id WHERE userrole.id='$value->role'")->row();
                                                                    if (!empty($roleqr)):
                                                                        $role_name = $roleqr->role_name;
                                                                    else:
                                                                        $role_name = 'N/A';
                                                                    endif;
                                                                    ?>
                                                                    <tr>
                                                                        <td></td>
                                                                        <td><?= $value->id; ?></td>
                                                                        <td><?= $value->name; ?></td>
                                                                        <td><?= $value->phone; ?></td>
                                                                        <td><?= $role_name ?></td>

                                                                        <td>
                                                                            <a href="#" onclick="change_role('<?= $value->id; ?>');">
                                                                                <button type="button"class="btn-default">Change Role</button>
                                                                            </a>

                                                                        </td>
                                                                    </tr>
                                                                <?php endforeach; ?>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </body>

</html>
<div id="rolechange_modal" class="modal fade " role="dialog" style="display: hidden;">
    <div class="modal-dialog">
        <div class="modal-content">
            <form action="<?= base_url('Sadmin/change_role'); ?>" method="POST">
                <div class="modal-header" style="background-color:orangered;">
                    <b> Select a role for this user.</b>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <input type="hidden" id="user_id" name="user_id" class="form-control">
                    <div class="form-group" style="padding: 20px;">
                        <label class="col-md-4 control-label">Role Name :</label>
                        <div class="col-md-6 inputGroupContainer">
                            <div class="input-group">
                                <span class="input-group-addon" style="max-width: 100%;"><i class="glyphicon glyphicon-list"></i></span>
                                <select class="selectpicker form-control" required=""id="user_role" name="user_role">
                                    <option value="">--Select Role--</option>
                                    <?php foreach ($role as $value): ?>
                                        <option value="<?= $value->id; ?>"><?= $value->role_name; ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div><br>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success">Submit</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">NO</button>
                </div>
            </form>
        </div>

    </div>
</div>
<div id="member_modal" class="modal modal-adminpro-general fullwidth-popup-InformationproModal fade bounceInDown animated in" role="dialog" style="display: hidden;">
    <div class="modal-dialog">
        <div class="modal-content">
            <form action="<?= base_url('Sadmin/add_system_user'); ?>" method="POST">
                <div class="modal-header" style="background-color:orangered;">
                    <b> Select a user to make system user.</b>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <input type="hidden" id="user_id" name="user_id" class="form-control">
                    <div class="form-group" style="padding-top: 20px;">
                        <label class="col-md-4 control-label">Member Name :</label>
                        <div class="col-md-6 inputGroupContainer">
                            <div class="input-group">
                                <span class="input-group-addon" style="max-width: 100%;"><i class="glyphicon glyphicon-list"></i></span>
                                <select class="selectpicker form-control" required=""id="member_name" name="member_name">
                                    <option value="">--Select Member--</option>
                                    <?php foreach ($member as $value): ?>
                                        <option value="<?= $value->id; ?>"><?= $value->name; ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div><br>
                        </div><br>
                    </div>

                    <div class="form-group" style="padding-bottom: 20px;">
                        <label class="col-md-4 control-label">Role Name :</label>
                        <div class="col-md-6 inputGroupContainer">
                            <div class="input-group">
                                <span class="input-group-addon" style="max-width: 100%;"><i class="glyphicon glyphicon-list"></i></span>
                                <select class="selectpicker form-control" required=""id="user_role" name="user_role">
                                    <option value="">--Select Role--</option>
                                    <?php foreach ($role as $value): ?>
                                        <option value="<?= $value->id; ?>"><?= $value->role_name; ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div><br>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success">Submit</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">NO</button>
                </div>
            </form>
        </div>

    </div>
</div>


<script>
    function change_role(id) {
        var user_id = id;
        $('#user_id').val(user_id);
        $('#rolechange_modal').modal('show');
    }
</script>




