<!doctype html>
<html class="no-js" lang="en">
    <body class="materialdesign">
        <div class="wrapper-pro">
            <div class="content-inner-all">

                <div class="header-top-area"></div>
                <!-- Breadcome start-->
                <div class="breadcome-area mg-b-30 small-dn">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="breadcome-list map-mg-t-40-gl shadow-reset">
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <ul class="breadcome-menu">
                                                <li><span class="bread-blod"> Edit Blog .</span>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="login-form-area mg-t-30 mg-b-15">
                    <div class="container-fluid">
                        <div class="row">

                            <form id="adminpro-register-form" class="adminpro-form" action="<?= base_url('Blog/update_blog'); ?>" method="post" enctype="multipart/form-data">
                                <div class="col-lg-10 col-lg-offset-1">
                                    <div class="login-bg">
                                        <div class="form-group">
                                            <input type="hidden" name="blog_id" id="blog_id" value="<?= $details->id; ?>">
                                            <label class="col-md-4 control-label">Blog title :<span style="color:red">*</span></label>
                                            <div class="col-md-6 inputGroupContainer">
                                                <div class="input-group"><span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span><input id="blog_tilte" value="<?= $details->blog_tilte; ?>" name="blog_tilte" placeholder="blog_tilte" class="form-control" required="true" value="" type="text"></div>
                                            </div><br>
                                        </div><br>
                                        <div class="form-group">

                                            <label class="col-md-4 control-label">Blog category :<span style="color:red">*</span></label>
                                            <div class="col-md-6 inputGroupContainer">
                                                <div class="input-group">
                                                    <span class="input-group-addon" style="max-width: 100%;"><i class="glyphicon glyphicon-list"></i></span>
                                                    <select class="selectpicker form-control" required=""id="blog_category" name="blog_category">
                                                        <option value="">--Select--</option>
                                                        <?php
                                                        foreach ($all_category as $value):
                                                            if ($details->blog_category == $value->id):
                                                                ?>
                                                                <option value="<?= $value->id ?>"selected=""><?= $value->category_name; ?></option>
                                                            <?php else: ?>
                                                                <option value="<?= $value->id ?>"><?= $value->category_name; ?></option>
                                                            <?php
                                                            endif;
                                                        endforeach;
                                                        ?>

                                                    </select>
                                                </div>
                                            </div><br>
                                        </div><br>
                                        <div class="form-group">
                                            <label class="col-md-4 control-label">Featured Image:<span style="color:red;">*</span></label>
                                            <div class="col-md-6 inputGroupContainer">
                                                <div class="input-group"><span class="input-group-addon"><i class="glyphicon glyphicon-file"></i></span><input id="fileToUpload" name="fileToUpload"  class="form-control"  type="file"></div>
                                            </div><br>
                                            <img height="40px" width="40px;" src="<?= $base_url ?>assets/img/blog/<?= $details->fetured_image; ?>" alt="">
                                        </div><br>
                                        <div class="form-group">
                                            <label class="col-md-2 control-label">Blog Details :<span style="color:red">*</span></label>
                                            <div class="col-md-9">
                                                <textarea class="form-control" type="text" id="summernote4" name="summernote">
                                                    <?= $details->details; ?>
                                                </textarea>
                                                <center>
                                                    <a href="<?= base_url('Blog/bloglist'); ?>">
                                                        <button  type="button"   title="Back" class="btn btn-default ">Back</button>
                                                    </a>
                                                    <button  type="Submit"  id="submit" title="update" class="btn btn-success ">Update</button>
                                                </center>
                                            </div><br>

                                        </div><br>
                                    </div>
                                </div>
                            </form>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
<script>
    function validate() {
        var title = $('#blog_tilte').val();
        var cat = $('#blog_category').val();
        var image = $('#fileToUpload').val();
        if (title == '' || cat == '' || image == '') {
            alert('Please fill all required field');
        }
        else {

            document.getElementById("blogform").submit();
        }
    }
    document.getElementById("submit").disabled = false;
    $('#summernote4').on('summernote.keyup', function(event) {

        var text = $(this).next('.note-editor').find('.note-editable').text();
        var length = text.length;
        var words = this.value.match(/\S+/g).length;
        $('#wordcount').text(words);
        if (words >= 120) {
            document.getElementById("submit").disabled = false;
        }
        else {
            document.getElementById("submit").disabled = true;
        }
    });
    $(document).ready(function() {
        $('#summernote4').summernote({
            // set editor height
            height: 350, // set editor height
            minHeight: 350, // set minimum height of editor
            maxHeight: 350
        });
    });
    $('document').ready(function()
    {
        $('textarea').each(function() {
            $(this).val($(this).val().trim());
        }
        );
    });
</script>





