<!doctype html>


<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title> <?= $page_title; ?></title>
    <meta name="jhg" content="by Shahed CSE">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- favicon
                ============================================ -->
    <link rel="shortcut icon" type="image/x-icon" href="<?= base_url(); ?>assets/img/favicon.png">
    <!-- Google Fonts
                ============================================ -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,700,700i,800" rel="stylesheet">
    <!-- Bootstrap CSS
                ============================================ -->
    <link rel="stylesheet" href="<?= base_url(); ?>admin/assets/css/bootstrap.min.css">
    <!-- Bootstrap CSS
                ============================================ -->
    <link rel="stylesheet" href="<?= base_url(); ?>admin/assets/css/font-awesome.min.css">
    <!-- adminpro icon CSS
                ============================================ -->
    <link rel="stylesheet" href="<?= base_url(); ?>admin/assets/css/adminpro-custon-icon.css">
    <!-- meanmenu icon CSS
                ============================================ -->
    <link rel="stylesheet" href="<?= base_url(); ?>admin/assets/css/meanmenu.min.css">
    <!-- mCustomScrollbar CSS
                ============================================ -->
    <link rel="stylesheet" href="<?= base_url(); ?>admin/assets/css/jquery.mCustomScrollbar.min.css">
    <!-- animate CSS
                ============================================ -->
    <link rel="stylesheet" href="<?= base_url(); ?>admin/assets/css/animate.css">
    <!-- modals CSS
              ============================================ -->
    <link rel="stylesheet" href="<?= base_url(); ?>admin/assets/css/modals.css">
    <!-- jvectormap CSS
                ============================================ -->
    <link rel="stylesheet" href="<?= base_url(); ?>admin/assets/css/jvectormap/jquery-jvectormap-2.0.3.css">
    <!-- normalize CSS
                ============================================ -->
    <link rel="stylesheet" href="<?= base_url(); ?>admin/assets/css/data-table/bootstrap-table.css">
    <link rel="stylesheet" href="<?= base_url(); ?>admin/assets/css/data-table/bootstrap-editable.css">
    <!-- normalize CSS
                ============================================ -->
    <link rel="stylesheet" href="<?= base_url(); ?>admin/assets/css/normalize.css">
    <!-- charts CSS
                ============================================ -->
    <link rel="stylesheet" href="<?= base_url(); ?>admin/assets/css/c3.min.css">
    <!-- style CSS
                ============================================ -->
    <link rel="stylesheet" href="<?= base_url(); ?>admin/assets/style.css">
    <link rel="stylesheet" href="<?= base_url(); ?>admin/assets/css/form.css">
    <link rel="stylesheet" href="<?= base_url(); ?>admin/assets/css/form/all-type-forms.css">
    <!-- responsive CSS
                ============================================ -->
    <link rel="stylesheet" href="<?= base_url(); ?>admin/assets/css/responsive.css">
    <!-- modernizr JS
                ============================================ -->


    <link rel="stylesheet" href="<?= base_url(); ?>admin/assets/css/touchspin/jquery.bootstrap-touchspin.min.css">
    <!-- datapicker CSS
                ============================================ -->
    <link rel="stylesheet" href="<?= base_url(); ?>admin/assets/css/datapicker/datepicker3.css">
    <!-- forms CSS
                ============================================ -->
<!--   <link rel="stylesheet" href="<?= base_url(); ?>admin/assets/css/form/themesaller-forms.css">-->
    <!-- colorpicker CSS
                ============================================ -->
    <link rel="stylesheet" href="<?= base_url(); ?>admin/assets/css/colorpicker/colorpicker.css">
    <!-- select2 CSS
                ============================================ -->
    <link rel="stylesheet" href="<?= base_url(); ?>admin/assets/css/select2/select2.min.css">
    <link href="<?php echo $base_url; ?>admin/assets/css/multi-select.css" rel="stylesheet" type="text/css"/>
    <!-- chosen CSS
                ============================================ -->
    <link rel="stylesheet" href="<?= base_url(); ?>admin/assets/css/chosen/bootstrap-chosen.css">
    <!-- ionRangeSlider CSS
                ============================================ -->
    <link rel="stylesheet" href="<?= base_url(); ?>admin/assets/css/ionRangeSlider/ion.rangeSlider.css">
    <link rel="stylesheet" href="<?= base_url(); ?>admin/assets/css/ionRangeSlider/ion.rangeSlider.skinFlat.css">
    <!-- x-editor CSS
               ============================================ -->
    <link rel="stylesheet" href="<?= base_url(); ?>admin/assets/css/select2.css">
    <link rel="stylesheet" href="<?= base_url(); ?>admin/assets/css/datetimepicker.css">
    <link rel="stylesheet" href="<?= base_url(); ?>admin/assets/css/bootstrap-editable.css">
    <link rel="stylesheet" href="<?= base_url(); ?>admin/assets/css/x-editor-style.css">
 <link rel="stylesheet" href="<?= base_url(); ?>admin/assets/css/summernote.css">


    <script src="<?= base_url(); ?>admin/assets/js/vendor/jquery-1.11.3.min.js"></script>
    <!-- bootstrap JS
                ============================================ -->
    <script src="<?= base_url(); ?>admin/assets/js/bootstrap.min.js"></script>

    <script src="<?= base_url(); ?>admin/assets/js/vendor/modernizr-2.8.3.min.js"></script>
    <script type="text/javascript" src="<?= base_url(); ?>admin/assets/js/jquery.multi-select.js"></script>
    <script src="<?php echo base_url(); ?>admin/assets/js/handlebars.js"></script>
<script src="<?php echo base_url(); ?>admin/assets/js/typeahead.bundle.js"></script>


<div class="fixed-header-top">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-1 col-md-6 col-sm-6 col-xs-12">
                <button type="button" id="sidebarCollapse" class="btn bar-button-pro header-drl-controller-btn btn-info navbar-btn">
                    <i class="fa fa-bars"></i>
                </button>
                <div class="admin-logo logo-wrap-pro">
                 <!--   <a href="#"><img src="<?= base_url(); ?>admin/assets/img/logo/log.png" alt="" />
                    </a> -->
                </div>
            </div>
            <div class="col-lg-6 col-md-1 col-sm-1 col-xs-12">
                <div class="header-top-menu tabl-d-n">
                    <a href="<?= base_url(); ?>">
                        <button type="button" style="margin-top: 20px;" class="btn btn-custon-four btn-default"><span class="adminpro-icon adminpro-home-admin"></span> Go To Website</button>
                    </a>   
                </div>
            </div>
            <div class="col-lg-5 col-md-5 col-sm-6 col-xs-12">
                <div class="header-right-info">
                    <ul class="nav navbar-nav mai-top-nav header-right-menu">

                        <li class="nav-item">
                            <a href="#" data-toggle="dropdown" role="button" aria-expanded="false" class="nav-link dropdown-toggle">
                                <span class="user-profile-comment-img"><img  height="25px;" width="25px;" src="<?= base_url('assets/img/profile/' . $this->session->userdata('image_path')) ?>"></span>
                                <span class="admin-name"><?= $this->session->userdata('user_name'); ?></span>
                                <span class="author-project-icon adminpro-icon adminpro-down-arrow"></span>
                            </a>
                            <ul role="menu" class="dropdown-header-top author-log dropdown-menu animated flipInX">

                                <li><a href="<?= base_url('Auth/profile') ?>"><span class="adminpro-icon adminpro-user-rounded author-log-ic"></span>My Profile</a>
                                </li>
                                <li><a href="#" onclick="changepassModal();"><span class="adminpro-icon adminpro-money author-log-ic"></span>Change Password</a>
                                </li>
                                <li><a href="<?= base_url(); ?>"><span class="adminpro-icon adminpro-money author-log-ic"></span>Go to website</a>
                                </li>
                                <li><a href="#" onclick="showlogoutModal();"><span class="adminpro-icon adminpro-locked author-log-ic"></span>Log Out</a>
                                </li>
                            </ul>
                        </li>

                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

</head>






