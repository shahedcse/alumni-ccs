<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Member_panel extends CI_Controller {

    public function enlisted_member() {
        $data['base_url'] = $this->config->item('base_url');
        $data['page_title'] = "Enlisted Member";
        $data['allmember'] = $this->db->query("SELECT * FROM enlisted_member  order by name ")->result();
        $data['blood'] = $this->db->query("SELECT * FROM allblood_group order by blood_group ASC")->result();
        $data['profession'] = $this->db->query("SELECT * FROM profession ")->result();
        $this->load->view('admin/header', $data);
        $this->load->view('admin/sidebar', $data);
        $this->load->view('admin/enlisted_member', $data);
        $this->load->view('admin/footer', $data);
    }

    public function registered_member() {
        $data['base_url'] = $this->config->item('base_url');
        $data['page_title'] = "Registered Member";
        $data['allmember'] = $this->db->query("SELECT * FROM users WHERE status='1'  order by name ")->result();

        $this->load->view('admin/header', $data);
        $this->load->view('admin/sidebar', $data);
        $this->load->view('admin/registered_member', $data);
        $this->load->view('admin/footer', $data);
    }

    public function inactivie_member() {
        $data['base_url'] = $this->config->item('base_url');
        $data['page_title'] = "Inactive Member";
        $data['allmember'] = $this->db->query("SELECT * FROM users WHERE status='0'  order by name ")->result();

        $this->load->view('admin/header', $data);
        $this->load->view('admin/sidebar', $data);
        $this->load->view('admin/inactivie_member', $data);
        $this->load->view('admin/footer', $data);
    }

    function insert_enlist() {
        $lm = $this->input->post('lm_no');
        $userData = array(
            'lm_no' => $lm,
            'name' => $this->input->post('name'),
            'email' => $this->input->post('email'),
            'phone' => $this->input->post('phone'),
            'adress' => $this->input->post('adress'),
            'passing_year' => $this->input->post('passing_year'),
            'profession' => $this->input->post('profession'),
            'blood_group' => $this->input->post('blood_group'),
            'added_by' => $this->session->userdata('user_id')
        );
        $status = $this->db->insert('enlisted_member', $userData);
        //sms Code

        $to_number = (int) 88 . $this->input->post('phone');
        $message = "Congrats !! You are now enlisted in chattagram collegiate school alumni association.Your L.M No. is - $lm.Please register now in our alumni site. Thank You";
        $queryString = [
            'ApiKey' => '9WSQ1tffrnrrjNOICNu1NGohCHqHnUS5tZtfKlfUHmE=',
            'ClientId' => 'd0ea432b-1c9c-4d3e-8879-ab8712cba7de',
            'SenderId' => 'batch99',
            'Message' => $message,
            'MobileNumber' => $to_number,
            'Is_Unicode' => 'false',
            'Is_Flash' => 'false'
        ];

        $requestresult = $this->callApi('GET', 'http://188.138.33.132:6005/api/v2/SendSMS', $queryString);
        if ($status):
            $this->session->set_userdata('add', 'Successfully Enlisted user data');
        else:
            $this->session->set_userdata('notadd', 'User Enlisted failed');
        endif;
        redirect('Member_panel/enlisted_member');
    }

    public function callApi($method, $url, $data = false) {
        $curl = curl_init();

        switch ($method) {
            case "POST":
                curl_setopt($curl, CURLOPT_POST, 1);
                if ($data) {
                    curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
                }
                break;
            case "PUT":
                curl_setopt($curl, CURLOPT_PUT, 1);
                break;
            default:
                if ($data) {
                    $url = sprintf("%s?%s", $url, http_build_query($data));
                }
        }

        /** Optional Authentication: */
        curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($curl, CURLOPT_USERPWD, "username:password");

        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

        $result = curl_exec($curl);

        if (!curl_errno($curl)) {
            $result = json_decode($result);
            $result = json_encode($result, JSON_PRETTY_PRINT);
        }

        curl_close($curl);
        return $result;
    }

}
