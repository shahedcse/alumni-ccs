<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Media extends CI_Controller {

    public function image_gallery() {
        $data['base_url'] = $this->config->item('base_url');
        $data['page_title'] = "image_gallery";

        $data['all_album'] = $this->db->query("SELECT * FROM gallery_album ")->result();

        $this->load->view('web/header', $data);
        $this->load->view('web/image_gallery', $data);
        $this->load->view('web/footer', $data);
    }

    public function videos() {
        $data['base_url'] = $this->config->item('base_url');
        $data['page_title'] = "Videos";


        $this->load->view('web/header', $data);
        $this->load->view('web/videos', $data);
        $this->load->view('web/footer', $data);
    }

    public function album_details() {
        $data['base_url'] = $this->config->item('base_url');
        $albumid = $this->input->get('id');
        $data['photoall'] = $this->db->query("SELECT * FROM gallery_photo WHERE gallery_id='$albumid'")->result();

        $data['page_title'] = "Album photo";


        $this->load->view('web/header', $data);
        $this->load->view('web/album_photo', $data);
        $this->load->view('web/footer', $data);
    }

}
