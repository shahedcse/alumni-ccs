<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_media extends CI_Controller {

    public function image_gallery() {
        $data['base_url'] = $this->config->item('base_url');
        $data['page_title'] = "Image Gallery";
        $data['allalbum'] = $this->db->query("SELECT * FROM gallery_album  order by id DESC ")->result();

        $this->load->view('admin/header', $data);
        $this->load->view('admin/sidebar', $data);
        $this->load->view('admin/image_gallery', $data);
        $this->load->view('admin/footer', $data);
    }

    public function album_details() {
        $data['base_url'] = $this->config->item('base_url');
        $data['page_title'] = "Album Details";
        $id = $this->input->get('id');
        $data['album_id'] = $id;
        $data['album_details'] = $this->db->query("SELECT * FROM gallery_photo where gallery_id='$id' ")->result();

        $this->load->view('admin/header', $data);
        $this->load->view('admin/sidebar', $data);
        $this->load->view('admin/album_details', $data);
        $this->load->view('admin/footer', $data);
    }

    public function fileUpload() {
        $data['base_url'] = $this->config->item('base_url');
        $name_array = array();
        $config['upload_path'] = 'assets/img/gallery';
        $config['allowed_types'] = 'gif|jpg|png|jpeg|pdf|';
        $config['overwrite'] = FALSE;
        $config['max_size'] = 0;
        $config['max_width'] = 0;
        $config['max_height'] = 0;
        $count = count($_FILES['userfile']['size']);

        foreach ($_FILES as $key => $value)
            for ($s = 0; $s <= $count - 1; $s++) {
                $_FILES['userfile']['name'] = $value['name'][$s];
                $_FILES['userfile']['type'] = $value['type'][$s];
                $_FILES['userfile']['tmp_name'] = $value['tmp_name'][$s];
                $_FILES['userfile']['error'] = $value['error'][$s];
                $_FILES['userfile']['size'] = $value['size'][$s];


                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                $this->upload->do_upload();
                $data = $this->upload->data();
                $name_array[] = $data['file_name'];
            }
        $albumdata = array(
            'album_name' => $this->input->post('album_name'),
            'description' => $this->input->post('description'),
            'created_date' => date('Y-m-d'),
            'created_by' => $this->session->userdata("user_id")
        );
        $status = $this->db->insert('gallery_album', $albumdata);

        $queryp = $this->db->query("SELECT  MAX(id) AS max_id  FROM  gallery_album;");
        $master_id = $queryp->row()->max_id;

        for ($i = 0; $i <= $count - 1; $i++) {
            $imagenamedata[] = array(
                'gallery_id' => $master_id,
                'image_path' => $name_array[$i],
            );
        }
        $this->db->insert_batch('gallery_photo', $imagenamedata);
        if ($status):
            $this->session->set_userdata('add', 'Gallery Album added Sucessfully');
        else:
            $this->session->set_userdata('notadd', 'Gallery Album added failed');
        endif;
        redirect('Admin_media/image_gallery');
    }

    public function addalbumphoto() {
        $data['base_url'] = $this->config->item('base_url');
        $name_array = array();
        $config['upload_path'] = 'assets/img/gallery';
        $config['allowed_types'] = 'gif|jpg|png|jpeg|pdf|';
        $config['overwrite'] = FALSE;
        $config['max_size'] = 0;
        $config['max_width'] = 0;
        $config['max_height'] = 0;
        $count = count($_FILES['userfile']['size']);

        foreach ($_FILES as $key => $value)
            for ($s = 0; $s <= $count - 1; $s++) {
                $_FILES['userfile']['name'] = $value['name'][$s];
                $_FILES['userfile']['type'] = $value['type'][$s];
                $_FILES['userfile']['tmp_name'] = $value['tmp_name'][$s];
                $_FILES['userfile']['error'] = $value['error'][$s];
                $_FILES['userfile']['size'] = $value['size'][$s];


                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                $this->upload->do_upload();
                $data = $this->upload->data();
                $name_array[] = $data['file_name'];
            }
        $album_id = $this->input->post('album_id');
        for ($i = 0; $i <= $count - 1; $i++) {
            $imagenamedata[] = array(
                'gallery_id' => $album_id,
                'image_path' => $name_array[$i],
            );
        }
        $status = $this->db->insert_batch('gallery_photo', $imagenamedata);
        if ($status):
            $this->session->set_userdata('add', 'Image added Sucessfully');
        else:
            $this->session->set_userdata('notadd', 'Image added failed');
        endif;
        redirect('Admin_media/album_details?id=' . $album_id);
    }

    function delete_photo() {
        $album_id = $this->input->post('album_id');
        $photo_id = $this->input->post('photo_id');
        $this->db->where('id', $photo_id);
        $status = $this->db->delete('gallery_photo');
        if ($status):
            $this->session->set_userdata('add', 'Image Deleted Sucessfully');
        else:
            $this->session->set_userdata('notadd', 'Image Delettion failed');
        endif;
        redirect('Admin_media/album_details?id=' . $album_id);
    }

}
