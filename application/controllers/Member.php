<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Member extends CI_Controller {

    public function index() {
        $data['base_url'] = $this->config->item('base_url');
        $data['page_title'] = "All Member";
        $data['allmember'] = $this->db->query("SELECT * FROM enlisted_member  order by lm_no ")->result();
        $this->load->view('web/header', $data);
        $this->load->view('web/member_list', $data);
        $this->load->view('web/footer', $data);
    }

    public function executive_member() {
        $data['base_url'] = $this->config->item('base_url');
        $data['page_title'] = "Executive_member";


        $data['allmember'] = $this->db->query("SELECT * FROM users where status='1' order by name ")->result();
        $this->load->view('web/header', $data);
        $this->load->view('web/executive_member', $data);
        $this->load->view('web/footer', $data);
    }

    function setmemberid() {
        if (in_array($this->session->userdata('user_role'), array(1, 3))) :
            $id = $this->input->post('user_id2');
            $submitid = $this->input->post('member_id');
            $user_id = $this->session->userdata('user_id');
            $memberid = substr($submitid, 5);
            $userData = array(
                'member_id' => $memberid,
                'verified_by' => $user_id
            );
            $this->db->where('id', $id);
            $status = $this->db->update('users', $userData);
            $memberinfo = $this->db->query("SELECT * FROM users where id='$id'")->row();

            $to_number = (int) 88 . $memberinfo->phone;
            $message = "Congrats !! Friend, Your registration info is verified by admin.Your member id is '$submitid' . Now You can log in our website.Stay Connected. from  - SSC'99 BD";
            $queryString = [
                'ApiKey' => '9WSQ1tffrnrrjNOICNu1NGohCHqHnUS5tZtfKlfUHmE=',
                'ClientId' => 'd0ea432b-1c9c-4d3e-8879-ab8712cba7de',
                'SenderId' => 'batch99',
                'Message' => $message,
                'MobileNumber' => $to_number,
                'Is_Unicode' => 'false',
                'Is_Flash' => 'false'
            ];

            $requestresult = $this->callApi('GET', 'http://188.138.33.132:6005/api/v2/SendSMS', $queryString);
            //Email Sending Code
            $name = $memberinfo->name;
            $emailto = $memberinfo->email;

            require FCPATH . 'vendor/autoload.php';
            $email = new \SendGrid\Mail\Mail();
            $email->setFrom("info@ssc99.org", "SSC-99");
            $email->setSubject('Verified' . $name);
            $email->addTo("$emailto", "SSC-99");

            $email->addContent("text/html", "Dear $name,  <br><br> 
          Congrats !! Friend, Your registration info is verified by admin.Your member id is '$submitid' . Now You can log in our website.Stay Connected. from  - SSC'99 BD.  <br> <br> 
        
        (NB: This is an automatic generated email. Please do not reply to this email) ");

            $sendgrid = new \SendGrid('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');

            try {
                $response = $sendgrid->send($email);
            } catch (Exception $e) {
                echo 'Caught exception: ' . $e->getMessage() . "\n";
            }
            if ($status):
                $this->session->set_userdata('add', 'Member Verification Successfull');
            else:
                $this->session->set_userdata('notadd', 'Member Verification  failed');
            endif;
            redirect('Member');
        else:
            redirect('home');
        endif;
    }

    public function callApi($method, $url, $data = false) {
        $curl = curl_init();

        switch ($method) {
            case "POST":
                curl_setopt($curl, CURLOPT_POST, 1);
                if ($data) {
                    curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
                }
                break;
            case "PUT":
                curl_setopt($curl, CURLOPT_PUT, 1);
                break;
            default:
                if ($data) {
                    $url = sprintf("%s?%s", $url, http_build_query($data));
                }
        }

        /** Optional Authentication: */
        curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($curl, CURLOPT_USERPWD, "username:password");

        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

        $result = curl_exec($curl);

        if (!curl_errno($curl)) {
            $result = json_decode($result);
            $result = json_encode($result, JSON_PRETTY_PRINT);
        }

        curl_close($curl);
        return $result;
    }

    function check_member() {
        $memberid = $this->input->post('memberid');
        $userQuery = $this->db->query("SELECT * FROM users WHERE member_id='$memberid'");
        if ($userQuery->num_rows() > 0):
            echo 'mass';
        else:
            echo 'dontmass';
        endif;
    }

    public function blood_donor() {
        if (in_array($this->session->userdata('user_role'), array(1, 2, 3))) :
            $data['base_url'] = $this->config->item('base_url');
            $data['page_title'] = "All Member";
            $data['use_role'] = $this->session->userdata("user_role");
            $data['allmember'] = $this->db->query("SELECT * FROM users where  blood_donor='1' AND status='1' order by name ")->result();
            $this->load->view('web/header', $data);
            $this->load->view('web/blooddonor_list', $data);
            $this->load->view('web/footer', $data);
        else:
            redirect('home');
        endif;
    }

    public function professional_list() {
        if (in_array($this->session->userdata('user_role'), array(1, 3))) :
            $data['base_url'] = $this->config->item('base_url');
            $data['page_title'] = "Inactive Member";
            $data['use_role'] = $this->session->userdata("user_role");
            $data['profession'] = $this->db->query("SELECT * FROM profession  ")->result();
            $profession = $this->input->post('Profession');
            if (!empty($profession)):
                $data['allmember'] = $this->db->query("SELECT * FROM users where  status ='1' AND Profession='$profession' order by name ")->result();
            else:
                $data['allmember'] = '';
            endif;
            $data['pro_name'] = $profession;


            $this->load->view('web/header', $data);
            $this->load->view('web/professional_list', $data);
            $this->load->view('web/footer', $data);
        else:
            redirect('home');
        endif;
    }

    public function inactive_user() {
        if (in_array($this->session->userdata('user_role'), array(1, 3))) :
            $data['base_url'] = $this->config->item('base_url');
            $data['page_title'] = "Inactive Member";
            $data['use_role'] = $this->session->userdata("user_role");
            $data['allmember'] = $this->db->query("SELECT * FROM users where  status !='1' order by name ")->result();
            $this->load->view('web/header', $data);
            $this->load->view('web/inactivemember_list', $data);
            $this->load->view('web/footer', $data);
        else:
            redirect('home');
        endif;
    }

    function make_activate() {
        $id = $this->input->post('user_id');
        $userData = array(
            'status' => 1
        );
        $this->db->where('id', $id);
        $this->db->update('users', $userData);
        echo 'mass';
    }

    function getDetailsData() {
        $id = $this->input->post('id');
        $query1 = $this->db->query("SELECT users.*, division_govt.divisionname, district_govt.districtname, allblood_group.blood_group AS group_name FROM users JOIN division_govt ON division_govt.id = users.division JOIN district_govt ON district_govt.id = users.district LEFT JOIN allblood_group ON allblood_group.id = users.blood_group WHERE users.id = '$id'  ");
        $dataresult = $query1->row();
        $outputData = array(
            'memberdata' => $dataresult,
        );

        echo json_encode($outputData);
    }

    function delete_user() {
        $id = $this->input->post('user_id');
        $this->db->where('id', $id);
        $status = $this->db->delete('users');
        $this->db->where('created_by', $id);
        $this->db->delete('blog');

        if ($status):
            $this->session->set_userdata('add', 'Member deleted Successfull');
        else:
            $this->session->set_userdata('notadd', 'Member deletion  failed');
        endif;
        redirect('Member_panel/inactivie_member');
    }

}
