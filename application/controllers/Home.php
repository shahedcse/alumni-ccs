<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

    public function index() {
        $data['base_url'] = $this->config->item('base_url');
        $data['page_title'] = "Alumni CCS ";
        $data['allevents'] = $this->db->query("SELECT * FROM event  order by id DESC LIMIT 3")->result();
        $this->load->view('web/header', $data);
        $this->load->view('web/home', $data);
        $this->load->view('web/footer', $data);
    }

  

}
