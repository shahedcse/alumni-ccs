<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Blog extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('Admin');
    }

    public function index() {
        $data['base_url'] = $this->config->item('base_url');
        $data['page_title'] = "Blog & News";
        $data['all_blog'] = $this->db->query("SELECT blog.*,users.name,users.image_path FROM blog JOIN users ON users.id=blog.created_by order by id asc")->result();

        $this->load->view('web/header', $data);
        $this->load->view('web/blog_home', $data);
        $this->load->view('web/footer', $data);
    }

    function bloglist() {
        if (in_array($this->session->userdata('user_role'), array(1, 2))) :
            $data['base_url'] = $this->config->item('base_url');
            $data['page_title'] = "Blog List";
            $user_id = $this->session->userdata('user_id');
            $data['user_role'] = $this->session->userdata('user_role');
            $user_role = $data['user_role'];
            $data['all_blog'] = $this->db->query("SELECT blog.*,users.name,blog_category.category_name FROM blog JOIN users ON users.id=blog.created_by join blog_category ON blog_category.id=blog.blog_category WHERE blog.created_by='$user_id' order by id desc")->result();

            $this->load->view('admin/header', $data);
            $this->load->view('admin/sidebar', $data);
            $this->load->view('admin/blog_list', $data);
            $this->load->view('admin/footer', $data);
        else:
            redirect('home');
        endif;
    }

    function edit_blog() {
        if (in_array($this->session->userdata('user_role'), array(1, 2))) :
            $data['base_url'] = $this->config->item('base_url');
            $data['page_title'] = "Edit Blog";
            $data['user_role'] = $this->session->userdata('user_role');
            $data['all_category'] = $this->db->query("SELECT * FROM blog_category order by id asc")->result();
            $user_role = $data['user_role'];
            $blog_id = $this->input->get('id');

            $data['details'] = $this->db->query("SELECT blog.*,users.name,blog_category.category_name FROM blog JOIN users ON users.id=blog.created_by join blog_category ON blog_category.id=blog.blog_category WHERE blog.id='$blog_id'")->row();

            $this->load->view('admin/header', $data);
            $this->load->view('admin/sidebar', $data);
            $this->load->view('admin/edit_blog', $data);
            $this->load->view('admin/footer', $data);
        else:
            redirect('home');
        endif;
    }

    function add_new() {
        if (in_array($this->session->userdata('user_role'), array(1, 2))) :
            $data['base_url'] = $this->config->item('base_url');
            $data['page_title'] = "Add New Blog";
            $data['user_role'] = $this->session->userdata('user_role');
            $data['all_category'] = $this->db->query("SELECT * FROM blog_category order by id asc")->result();
            $user_role = $data['user_role'];


            $this->load->view('admin/header', $data);
            $this->load->view('admin/sidebar', $data);
            $this->load->view('admin/new_blog', $data);
            $this->load->view('admin/footer', $data);
        else:
            redirect('home');
        endif;
    }

    function delete_blog() {
        $id = $this->input->post('blog_id');
        $this->db->where('id', $id);
        $status = $this->db->delete('blog');
        if ($status):
            $this->session->set_userdata('add', 'Blog is deleted Successfull');
        else:
            $this->session->set_userdata('notadd', 'Blog deletion  failed');
        endif;
        redirect('Blog/bloglist');
    }

    function delete_blog_admin() {
        $id = $this->input->post('blog_id');
        $this->db->where('id', $id);
        $status = $this->db->delete('blog');
        if ($status):
            $this->session->set_userdata('add', 'Blog is deleted Successfull');
        else:
            $this->session->set_userdata('notadd', 'Blog deletion  failed');
        endif;
        redirect('Sadmin/allblog');
    }

    function blog_view() {
        $data['base_url'] = $this->config->item('base_url');
        $data['page_title'] = "Blog Details";
        $data['user_role'] = $this->session->userdata('user_role');
        $blog_id = $this->input->get('id');
        $data['comment_count'] = $this->db->query("SELECT COUNT(id) AS totl FROM blog_comment WHERE blog_id='$blog_id'")->row()->totl;
        $data['comments'] = $this->db->query("SELECT * FROM blog_comment WHERE blog_id='$blog_id'")->result();
        $data['details'] = $this->db->query("SELECT blog.*,users.name,blog_category.category_name FROM blog JOIN users ON users.id=blog.created_by join blog_category ON blog_category.id=blog.blog_category WHERE blog.id='$blog_id'")->row();


        $this->load->view('web/header', $data);
        $this->load->view('web/view_blog', $data);
        $this->load->view('web/footer', $data);
    }

    function insert_comment() {
        $id = $this->session->userdata('user_id');
        $blog_id = $this->input->post('blog_id');
        $blogData = array(
            'blog_id' => $blog_id,
            'comment' => $this->input->post('comment'),
            'created_by' => $id,
            'created_date' => date('Y-m-d')
        );

        $status = $this->db->insert('blog_comment', $blogData);
        if ($status):
            $this->session->set_userdata('add', 'Comment added Successfull');
        else:
            $this->session->set_userdata('notadd', 'Comment added failed');
        endif;
        redirect('Blog/blog_view?id=' . $blog_id);
    }

    function insert_blog() {
        if (in_array($this->session->userdata('user_role'), array(1, 2))) :
            $id = $this->session->userdata('user_id');
            $target_dir = "assets/img/blog/";
            $target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
            $imgFile = $_FILES['fileToUpload']['name'];


            if (empty($imgFile)) :
                $image_path = 'nofile.jpg';
            else:
                if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) :
                    $image_path = basename($_FILES["fileToUpload"]["name"]);
                else:
                    $data['error'] = "Sorry, there was an error uploading your file";
                endif;
            endif;

            $blogData = array(
                'blog_tilte' => $this->input->post('blog_tilte'),
                'blog_category' => $this->input->post('blog_category'),
                'fetured_image' => $image_path,
                'details' => $this->input->post('summernote'),
                'created_by' => $id,
                'created_date' => date('Y-m-d')
            );

            $status = $this->db->insert('blog', $blogData);

            if ($status):
                $this->session->set_userdata('add', 'Blog added Successfull');
            else:
                $this->session->set_userdata('notadd', 'Blog added failed');
            endif;
            redirect('Blog/bloglist');
        else:
            redirect('home');
        endif;
    }

    function update_blog() {
        if (in_array($this->session->userdata('user_role'), array(1, 2))) :
            $id = $this->input->post('blog_id');
            $target_dir = "assets/img/blog/";
            $target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
            $imgFile = $_FILES['fileToUpload']['name'];
            if (empty($imgFile)) :
                $image_path = '';
            else:
                if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) :
                    $image_path = basename($_FILES["fileToUpload"]["name"]);
                else:
                    $data['error'] = "Sorry, there was an error uploading your file";
                endif;
            endif;
            if (empty($imgFile)) :
                $blogData = array(
                    'blog_tilte' => $this->input->post('blog_tilte'),
                    'blog_category' => $this->input->post('blog_category'),
                    'details' => $this->input->post('summernote'),
                );
            else:
                $blogData = array(
                    'blog_tilte' => $this->input->post('blog_tilte'),
                    'blog_category' => $this->input->post('blog_category'),
                    'fetured_image' => $image_path,
                    'details' => $this->input->post('summernote'),
                );
            endif;

            $this->db->where('id', $id);
            $status = $this->db->update('blog', $blogData);

            if ($status):
                $this->session->set_userdata('add', 'Blog edited Successfull');
            else:
                $this->session->set_userdata('notadd', 'Blog edited failed');
            endif;
            redirect('Blog/bloglist');
        else:
            redirect('home');
        endif;
    }

}
