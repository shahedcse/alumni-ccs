<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('Admin');
    }

    public function index() {
        $data['base_url'] = $this->config->item('base_url');
        $data['page_title'] = "Login";


        $this->load->view('web/header', $data);
        $this->load->view('web/login', $data);
        $this->load->view('web/footer', $data);
    }

    function check_verification() {
        $code = $this->input->post('verification_code');
        $userQuery = $this->db->query("SELECT verification_code,id,email,phone FROM users WHERE verification_code = '$code'");
        if ($userQuery->num_rows() > 0):
            $userData = array(
                'status' => 1
            );
            $id = $userQuery->row()->id;
            $this->db->where('id', $id);
            $this->db->update('users', $userData);

            $userQuery = $this->db->query("SELECT lm_no,password FROM users WHERE verification_code='$code' ");
            $lmno = $userQuery->row()->lm_no;
            $pass = $userQuery->row()->password;
            $this->login_withregistration($lmno, $pass);

        else:
            $this->session->set_userdata('notadd', 'Verification Code Not Matched.');
            redirect('Auth/registration_final');

        endif;
    }

    public function login_withregistration($lmno, $pass) {
        $lm_no = $lmno;
        $password = $pass;
        $userstatus = 1;
        $data['lm_no'] = $lm_no;
        $queryResult = $this->Admin->login($data);
        if ($queryResult) :
            $userdbstatus = $queryResult->status;
            $this->session->set_userdata('user_name', $queryResult->name);
            $dbpass = $queryResult->password;

            if ($userdbstatus == $userstatus && $dbpass == $password):
                $this->session->set_userdata('user_id', $queryResult->id);
                $this->session->set_userdata("user_role", $queryResult->role);
                $this->session->set_userdata("user_name", $queryResult->name);
                $this->session->set_userdata('image_path', $queryResult->image_path);
                $this->session->set_userdata('add', 'Your registration is successfully Completed.');
                redirect('Auth/profile');
            endif;
        else:
            $this->session->set_userdata('login_error', 'Something error is occured in your registration .');
            redirect('Auth');
        endif;
    }

    public function login() {
        $this->form_validation->set_rules('lm_no', 'lm_no', 'required');
        $this->form_validation->set_rules('password', 'password', 'required');
        $lmno = $this->input->post('lm_no');

        if ($this->form_validation->run() == FALSE):
            $this->session->set_userdata('login_error', 'Please Enter lm no. & password correctly');
            redirect('Auth');
        else:
            $pass = $this->input->post('password');
            $userstatus = 1;
            $data['lmno'] = $lmno;
            $queryResult = $this->Admin->login($data);

            if ($queryResult) :
                $userdbstatus = $queryResult->status;
                $this->session->set_userdata('user_name', $queryResult->name);
                $dbpass = $queryResult->password;

                if ($userdbstatus == $userstatus && $dbpass == $pass):
                    $this->session->set_userdata('user_id', $queryResult->id);
                    $this->session->set_userdata("user_role", $queryResult->role);
                    $this->session->set_userdata("user_name", $queryResult->name);
                    $this->session->set_userdata('image_path', $queryResult->image_path);
                    if ($this->session->userdata("user_role") == 1):
                        redirect('Sadmin/dashboard');
                    else:
                        redirect('Auth/profile');
                    endif;

                else:
                    $this->session->set_userdata('login_error', 'Please Check LM No. & Password.');
                    redirect('Auth');
                endif;

            else:
                $this->session->set_userdata('login_error', 'Not a Valid User .');
                redirect('Auth');
            endif;
        endif;
    }

    function profile() {
        if (in_array($this->session->userdata('user_role'), array(1, 2, 3))) :
            $data['base_url'] = $this->config->item('base_url');
            $data['page_title'] = "My profile";
            $data['divisionall'] = $this->db->query("SELECT * FROM division_govt order by divisionname ASC")->result();
            $data['blood'] = $this->db->query("SELECT * FROM allblood_group order by blood_group ASC")->result();
            $id = $this->session->userdata('user_id');
            $data['allinfo'] = $this->db->query("SELECT * FROM users where id='$id'")->row();

            $this->load->view('admin/header', $data);
            $this->load->view('admin/sidebar', $data);
            $this->load->view('admin/my_profile', $data);
            $this->load->view('admin/footer', $data);
        else:
            redirect('home');
        endif;
    }

    function update_profile() {
        if (in_array($this->session->userdata('user_role'), array(1, 2, 3))) :
            $data['base_url'] = $this->config->item('base_url');
            $data['page_title'] = "Update profile";
            $data['divisionall'] = $this->db->query("SELECT * FROM division_govt order by divisionname ASC")->result();
            $data['districtall'] = $this->db->query("SELECT * FROM district_govt order by districtname ASC")->result();
            $data['blood'] = $this->db->query("SELECT * FROM allblood_group order by blood_group ASC")->result();
            $data['profession'] = $this->db->query("SELECT * FROM profession ")->result();
            $data['profession_field'] = $this->db->query("SELECT * FROM profession_field ")->result();
            $id = $this->session->userdata('user_id');
            $data['allinfo'] = $this->db->query("SELECT * FROM users where id='$id'")->row();

            $this->load->view('admin/header', $data);
            $this->load->view('admin/sidebar', $data);
            $this->load->view('admin/update_profile', $data);
            $this->load->view('admin/footer', $data);
        else:
            redirect('home');
        endif;
    }

    public function registration() {
        $data['base_url'] = $this->config->item('base_url');
        $data['page_title'] = "Registration";
        $data['divisionall'] = $this->db->query("SELECT * FROM division_govt order by divisionname ASC")->result();
        $data['districtall'] = $this->db->query("SELECT * FROM district_govt order by districtname ASC")->result();
        $data['blood'] = $this->db->query("SELECT * FROM allblood_group order by blood_group ASC")->result();
        $data['profession'] = $this->db->query("SELECT * FROM profession ")->result();
        $data['profession_field'] = $this->db->query("SELECT * FROM profession_field ")->result();
        $this->load->view('web/header', $data);
        $this->load->view('web/registration', $data);
        $this->load->view('web/footer', $data);
    }

    function getdetails() {
        $lm = $this->input->post('lm_no');
        $userQuery = $this->db->query("SELECT * FROM enlisted_member WHERE lm_no = '$lm' ");
        $dataresult = $userQuery->row();
        $outputData = array(
            'memberdata' => $dataresult,
        );

        echo json_encode($outputData);
    }

    function checklm() {
        $lm = $this->input->post('lm_no');
        $userQuery = $this->db->query("SELECT lm_no FROM users WHERE lm_no = '$lm'");
        if ($userQuery->num_rows() > 0):
            echo 'mass';
        else:
            echo 'dontmass';
        endif;
    }

    function insert_regidata() {
        $characters = '012345';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $charactersLength; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }

        $target_dir = "assets/img/profile/";
        $target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
        $imgFile = $_FILES['fileToUpload']['name'];


        if (empty($imgFile)) :
            $image_path = 'demo.jpg';
        else:
            if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) :
                $image_path = basename($_FILES["fileToUpload"]["name"]);
            else:
                $data['error'] = "Sorry, there was an error uploading your file";
            endif;
        endif;
        $lm_no = $this->input->post('lm_no');
        $userData = array(
            'name' => $this->input->post('name'),
            'email' => $this->input->post('email'),
            'phone' => $this->input->post('phone'),
            'adress' => $this->input->post('adress'),
            'passing_year' => $this->input->post('passing_year'),
            'profession' => $this->input->post('profession'),
            'blood_group' => $this->input->post('blood_group'),
            'image_path' => $image_path
        );
        $this->db->where('lm_no', $lm_no);
        $this->db->update('enlisted_member', $userData);

        $userData = array(
            'lm_no' => $this->input->post('lm_no'),
            'name' => $this->input->post('name'),
            'email' => $this->input->post('email'),
            'phone' => $this->input->post('phone'),
            'present_address' => $this->input->post('adress'),
            'passing_year' => $this->input->post('passing_year'),
            'profession' => $this->input->post('profession'),
            'blood_group' => $this->input->post('blood_group'),
            'password' => $this->input->post('password'),
            'status' => 0,
            'role' => 2,
            'verification_code' => $randomString,
            'image_path' => $image_path,
            'created_date' => date('Y-m-d')
        );
        $status = $this->db->insert('users', $userData);
        //sms Code

        $to_number = (int) 88 . $this->input->post('phone');
        $message = "Congrats !! Your OTP  is $randomString .Please set it for registration complete.";
        $queryString = [
            'ApiKey' => '9WSQ1tffrnrrjNOICNu1NGohCHqHnUS5tZtfKlfUHmE=',
            'ClientId' => 'd0ea432b-1c9c-4d3e-8879-ab8712cba7de',
            'SenderId' => 'batch99',
            'Message' => $message,
            'MobileNumber' => $to_number,
            'Is_Unicode' => 'false',
            'Is_Flash' => 'false'
        ];

        $requestresult = $this->callApi('GET', 'http://188.138.33.132:6005/api/v2/SendSMS', $queryString);
        if ($status):
            $this->session->set_userdata('add', 'Data Submited,Your are one step down from final registration');
        else:
            $this->session->set_userdata('notadd', 'Your registration is failed');
        endif;
        redirect('Auth/registration_final');
    }

    public function registration_final() {
        $data['base_url'] = $this->config->item('base_url');
        $data['page_title'] = "Registration final";

        $this->load->view('web/header', $data);
        $this->load->view('web/registration_veri', $data);
        $this->load->view('web/footer', $data);
    }

    public function callApi($method, $url, $data = false) {
        $curl = curl_init();

        switch ($method) {
            case "POST":
                curl_setopt($curl, CURLOPT_POST, 1);
                if ($data) {
                    curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
                }
                break;
            case "PUT":
                curl_setopt($curl, CURLOPT_PUT, 1);
                break;
            default:
                if ($data) {
                    $url = sprintf("%s?%s", $url, http_build_query($data));
                }
        }

        /** Optional Authentication: */
        curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($curl, CURLOPT_USERPWD, "username:password");

        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

        $result = curl_exec($curl);

        if (!curl_errno($curl)) {
            $result = json_decode($result);
            $result = json_encode($result, JSON_PRETTY_PRINT);
        }

        curl_close($curl);
        return $result;
    }

    function update_profiledata() {
        $id = $this->session->userdata('user_id');
        $target_dir = "assets/img/profile/";
        $target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
        $imgFile = $_FILES['fileToUpload']['name'];


        if (empty($imgFile)) :
            $image_path = 'demo.jpg';
        else:
            if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) :
                $image_path = basename($_FILES["fileToUpload"]["name"]);
            else:
                $data['error'] = "Sorry, there was an error uploading your file";
            endif;
        endif;

        if (empty($imgFile)) :
            $userData = array(
                'name' => $this->input->post('name'),
                'gender' => $this->input->post('gender'),
                'present_loc' => $this->input->post('present_loc'),
                'email' => $this->input->post('email'),
                'passing_year' => $this->input->post('passing_year'),
                'phone' => $this->input->post('phone'),
                'present_address' => $this->input->post('present_address'),
                'fblink' => $this->input->post('fblink'),
                'profession' => $this->input->post('profession'),
                'profession_field' => $this->input->post('profession_field'),
                'specialization' => $this->input->post('specialization'),
                'blood_group' => $this->input->post('blood_group')
            );
        else:
            $userData = array(
                'name' => $this->input->post('name'),
                'gender' => $this->input->post('gender'),
                'present_loc' => $this->input->post('present_loc'),
                'email' => $this->input->post('email'),
                'passing_year' => $this->input->post('passing_year'),
                'phone' => $this->input->post('phone'),
                'present_address' => $this->input->post('present_address'),
                'fblink' => $this->input->post('fblink'),
                'profession' => $this->input->post('profession'),
                'profession_field' => $this->input->post('profession_field'),
                'specialization' => $this->input->post('specialization'),
                'blood_group' => $this->input->post('blood_group'),
                'image_path' => $image_path
            );
        endif;
        $this->db->where('id', $id);
        $status = $this->db->update('users', $userData);

        if ($status):
            //email
            $this->session->set_userdata('add', 'Your profile update is Successfull');
        else:
            $this->session->set_userdata('notadd', 'Your profile update is failed');
        endif;
        redirect('Auth/profile');
    }

    function getdistrict() {
        $division_id = $this->input->post('division');
        if (is_int((int) $division_id)):
            $query = $this->db->query("SELECT * FROM `district_govt` WHERE `divisionid`='$division_id' ");
            if ($query->num_rows() > 0):
                $queryresult = $query->result();
                echo '<option value="">-- Select District --</option>';
                foreach ($queryresult as $data):
                    echo '<option value="' . $data->id . '">' . $data->districtname . '</option>';
                endforeach;
            else:
                echo '<option value = "">no data found</option>';
            endif;
        else:
            echo '<option value = "">no data found</option>';
        endif;
    }

    function logout() {

        if (($this->session->userdata('user_role') != NULL) && ($this->session->userdata('user_name') != NULL )):
// $logdetails = "Logout successfully";
            $this->session->unset_userdata('user_name');
            $this->session->unset_userdata('user_role');
            $this->session->unset_userdata("image_path");
            $this->session->sess_destroy();
            $this->session->set_userdata('add', 'Sucessfully Log Out');
            redirect('Auth');
        else:
            redirect('Auth');
        endif;
    }

    function changepassword() {
        if (in_array($this->session->userdata('user_role'), array(1, 2, 3, 4))):
            $id = $this->session->userdata('user_id');
            $npassword = $this->input->post('npassword2');
            $userdata = array(
                'password' => $npassword
            );
            $this->db->where('id', $id);
            $status = $this->db->update('users', $userdata);
            if ($status):
                //email
                $this->session->set_userdata('add', 'Password changed successfully');
            else:
                $this->session->set_userdata('notadd', 'Password changed  failed');
            endif;
            redirect('Auth/profile');
        else:
            redirect('Auth');
        endif;
    }

}
