<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class About extends CI_Controller {

    public function index() {
        $data['base_url'] = $this->config->item('base_url');
        $data['page_title'] = "About us";


        $this->load->view('web/header', $data);
        $this->load->view('web/aboutus', $data);
        $this->load->view('web/footer', $data);
    }

    public function rules() {
        $data['base_url'] = $this->config->item('base_url');
        $data['page_title'] = "Terms & Condition";


        $this->load->view('web/header', $data);
        $this->load->view('web/group_rules', $data);
        $this->load->view('web/footer', $data);
    }

    public function allevents() {
        $data['base_url'] = $this->config->item('base_url');
        $data['page_title'] = "Event List";
        $data['allevents'] = $this->db->query("SELECT * FROM event  order by id ")->result();

        $this->load->view('web/header', $data);
        $this->load->view('web/allevents', $data);
        $this->load->view('web/footer', $data);
    }

    public function event_details() {
        $data['base_url'] = $this->config->item('base_url');
        $data['page_title'] = "Event Details";
        $eventid = $this->input->get('id');
        $data['event_details'] = $this->db->query("SELECT * FROM event where id='$eventid'")->row();

        $this->load->view('web/header', $data);
        $this->load->view('web/event_view', $data);
        $this->load->view('web/footer', $data);
    }

    public function Support() {
        $data['base_url'] = $this->config->item('base_url');
        $data['page_title'] = "Support";


        $this->load->view('web/header', $data);
        $this->load->view('web/support', $data);
        $this->load->view('web/footer', $data);
    }

    public function contact() {
        $data['base_url'] = $this->config->item('base_url');
        $data['page_title'] = "Contact";


        $this->load->view('web/header', $data);
        $this->load->view('web/contact', $data);
        $this->load->view('web/footer', $data);
    }

    function send_message() {
        $name = $this->input->get('name');
        $phone = $this->input->get('phone');
        $message = $this->input->get('message');

        $messageData = [
            'name' => $name,
            'phone' => $phone,
            'message' => $message,
            'date' => date("Y-m-d")
        ];

        $status = $this->db
                ->insert('messages', $messageData);
    }

    public function Donation() {
        $data['base_url'] = $this->config->item('base_url');
        $data['page_title'] = "Make Donation";


        $this->load->view('web/header', $data);
        $this->load->view('web/donation', $data);
        $this->load->view('web/footer', $data);
    }

}
