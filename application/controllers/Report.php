<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Report extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('Autocomplete_model');
    }

    public function index() {
        $data['base_url'] = $this->config->item('base_url');
        $data['page_title'] = "Alumni Report";
        $data['all_lm'] = $this->db->query("SELECT lm_no FROM enlisted_member  order by lm_no ASC ")->result();
        $data['blood'] = $this->db->query("SELECT * FROM allblood_group order by blood_group ASC")->result();
        $name = $this->input->post('name');
        $lm = $this->input->post('lm_no');
        $passyear = $this->input->post('pass_year');
        $blood_group = $this->input->post('blood_group');
        

        if (!empty($name)):
            $data['allmember'] = $this->db->query("SELECT * FROM enlisted_member   WHERE name LIKE '%$name%'")->result();
        elseif(!empty($lm)):
            $data['allmember'] = $this->db->query("SELECT * FROM enlisted_member   WHERE lm_no='$lm'")->result();
        elseif(!empty($passyear)):
            $data['allmember'] = $this->db->query("SELECT * FROM enlisted_member WHERE passing_year='$passyear'")->result();
        elseif(!empty($blood_group)):
            $data['allmember'] = $this->db->query("SELECT * FROM enlisted_member   WHERE blood_group='$blood_group'")->result();
        else:
            $data['allmember'] = '';
        endif;
       


        $this->load->view('admin/header', $data);
        $this->load->view('admin/sidebar', $data);
        $this->load->view('admin/report', $data);
        $this->load->view('admin/footer', $data);
    }

    function fetch() {
        echo $this->Autocomplete_model->fetch_data($this->uri->segment(3));
    }

}
